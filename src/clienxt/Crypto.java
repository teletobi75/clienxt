/*
 * The MIT License
 *
 * Copyright 2014 fmiboy.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package clienxt;

import static clienxt.ClienxtForm.LogGui;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import static java.lang.Integer.MAX_VALUE;
import static java.lang.System.arraycopy;
import java.security.CodeSource;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import static java.security.Security.getProperty;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.regex.Pattern;
import static java.util.regex.Pattern.CASE_INSENSITIVE;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;


public class Crypto {
		
		static byte[] getPublicKey(String secretPhrase) {
			
			try {
				
				byte[] publicKey = new byte[32];
				Curve25519.keygen(publicKey, null, MessageDigest.getInstance("SHA-256").digest(secretPhrase.getBytes("UTF-8")));
				
				return publicKey;
				
			} catch (Exception e) {
				
				return null;
				
			}
			
		}
		
		static byte[] sign(byte[] message, String secretPhrase) {
			
			try {
				
				byte[] P = new byte[32];
				byte[] s = new byte[32];
				MessageDigest digest = MessageDigest.getInstance("SHA-256");
				Curve25519.keygen(P, s, digest.digest(secretPhrase.getBytes("UTF-8")));
				
				byte[] m = digest.digest(message);
				
				digest.update(m);
				byte[] x = digest.digest(s);
				
				byte[] Y = new byte[32];
				Curve25519.keygen(Y, null, x);
				
				digest.update(m);
				byte[] h = digest.digest(Y);
				
				byte[] v = new byte[32];
				Curve25519.sign(v, h, x, s);
				
				byte[] signature = new byte[64];
				System.arraycopy(v, 0, signature, 0, 32);
				System.arraycopy(h, 0, signature, 32, 32);
				
				return signature;
				
			} catch (Exception e) {
				
				return null;
				
			}
			
		}
		
		static boolean verify(byte[] signature, byte[] message, byte[] publicKey) {
			
			try {
				
				byte[] Y = new byte[32];
				byte[] v = new byte[32];
				System.arraycopy(signature, 0, v, 0, 32);
				byte[] h = new byte[32];
				System.arraycopy(signature, 32, h, 0, 32);
				Curve25519.verify(Y, v, h, publicKey);
				
				MessageDigest digest = MessageDigest.getInstance("SHA-256");
				byte[] m = digest.digest(message);
				digest.update(m);
				byte[] h2 = digest.digest(Y);
				
				return Arrays.equals(h, h2);
				
			} catch (Exception e) {
				
				return false;
				
			}
			
		}
                static public byte[] encryptMessage(String stringToEncrypt, byte[] privateKey, byte[] theirPublicKey) throws UnsupportedEncodingException{
                    byte[] plainText = stringToEncrypt.getBytes("UTF-8");
                    nxt.crypto.XoredData en = nxt.crypto.XoredData.encrypt(plainText, privateKey, theirPublicKey);
                    byte[] nonce = en.getNonce();
                    byte[] data = en.getData();
                    byte[] message = new byte[nonce.length + data.length];
                    System.arraycopy(nonce, 0, message, 0, nonce.length);
                    System.arraycopy(data, 0, message, nonce.length, data.length);
                    return message;
                }
                static public String decryptMessage(byte[] encryptedText, byte[] privateKey, byte[] theirPublicKey) throws UnsupportedEncodingException{
                    byte[] nonce2 = new byte[32];
                    byte[] data2 = new byte[encryptedText.length - nonce2.length];
                    System.arraycopy(encryptedText, 0, nonce2, 0, nonce2.length);
                    System.arraycopy(encryptedText, nonce2.length, data2, 0, data2.length);
                    nxt.crypto.XoredData xored = new nxt.crypto.XoredData(data2, nonce2);
                    byte[] plainText = xored.decrypt(privateKey, theirPublicKey);
                    return new String(plainText, "UTF-8");
                }
                /*static public byte[] encodeMessage(String message, String senderSecret, byte[] recipientKey){
                    try {
                        byte[] senderPrivate = MessageDigest.getInstance("SHA-256").digest(senderSecret.getBytes("UTF-8"));
                        byte[] senderKey = new byte[32];
                        Curve25519.keygen(senderKey, null, senderPrivate);

                        byte[] sharedSecret = new byte[32];
                        Curve25519.curve(sharedSecret, senderPrivate, recipientKey);

                        byte[] plainText = message.getBytes("UTF-8");
                        byte[] cipherText = new byte[plainText.length];
                        byte[] seed = MessageDigest.getInstance("SHA-256").digest(sharedSecret);
                        int n = plainText.length;
                        int index = 0;
                        while( n > 0 ){
                            int len = n > 32 ? 32 : n;
                            for ( int i = 0; i < len; ++ i ){
                                cipherText[index + i] = (byte) (plainText[index + i] ^ seed[i]);
                            }
                            n -= len;
                            index += len;
                            seed = MessageDigest.getInstance("SHA-256").digest(seed);
                        }

                        return cipherText;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return null;
                }
                static public String decodeMessage(byte[] encodeData, String recipientSecret, byte[] senderKey){
                    try {
                        byte[] recipientPrivate = MessageDigest.getInstance("SHA-256")
                                .digest(recipientSecret.getBytes("UTF-8"));
                        byte[] recipientKey = new byte[32];
                        Curve25519.keygen(recipientKey, null, recipientPrivate);

                        byte[] sharedSecret = new byte[32];
                        Curve25519.curve(sharedSecret, recipientPrivate, senderKey);

                        byte[] decodeText = new byte[encodeData.length];
                        byte[] seed = MessageDigest.getInstance("SHA-256").digest(sharedSecret);
                        int n = encodeData.length;
                        int index = 0;
                        while( n > 0 ){
                            int len = n > 32 ? 32 : n;
                            for ( int i = 0; i < len; ++ i ){
                                decodeText[index + i] = (byte) (encodeData[index + i] ^ seed[i]);
                            }
                            n -= len;
                            index += len;
                            seed = MessageDigest.getInstance("SHA-256").digest(seed);
                        }

                        return new String(decodeText);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    return null;
                }*/
                public static byte[] getPrivateKey(String secretPhrase) {
                    try {
                      MessageDigest digest = nxt.crypto.Crypto.sha256();
                      byte[] s = digest.digest(secretPhrase.getBytes("UTF-8"));
                      clienxt.Curve25519.clamp(s);
                      return s;
                    }
                    catch (RuntimeException | UnsupportedEncodingException e) {
                      LogGui("Error getting private key");
                      return null;
                    }
                }
}
	
	/* Ported from C to Java by Dmitry Skiba [sahn0], 23/02/08.
	 * Original: http://cds.xs4all.nl:8081/ecdh/
	 */
	/* Generic 64-bit integer implementation of Curve25519 ECDH
	 * Written by Matthijs van Duin, 200608242056
	 * Public domain.
	 *
	 * Based on work by Daniel J Bernstein, http://cr.yp.to/ecdh.html
	 */
	class Curve25519 {

		/* key size */
		public static final int KEY_SIZE = 32;
		
		/* 0 */
		public static final byte[] ZERO = {
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
		};
		
		/* the prime 2^255-19 */
		public static final byte[] PRIME = {
			(byte)237, (byte)255, (byte)255, (byte)255,
			(byte)255, (byte)255, (byte)255, (byte)255,
			(byte)255, (byte)255, (byte)255, (byte)255,
			(byte)255, (byte)255, (byte)255, (byte)255,
			(byte)255, (byte)255, (byte)255, (byte)255,
			(byte)255, (byte)255, (byte)255, (byte)255,
		    (byte)255, (byte)255, (byte)255, (byte)255,
		    (byte)255, (byte)255, (byte)255, (byte)127
		};
		
		/* group order (a prime near 2^252+2^124) */
		public static final byte[] ORDER = {
			(byte)237, (byte)211, (byte)245, (byte)92,
			(byte)26,  (byte)99,  (byte)18,  (byte)88,
			(byte)214, (byte)156, (byte)247, (byte)162,
			(byte)222, (byte)249, (byte)222, (byte)20,
			(byte)0,   (byte)0,   (byte)0,   (byte)0, 
			(byte)0,   (byte)0,   (byte)0,   (byte)0,
			(byte)0,   (byte)0,   (byte)0,   (byte)0, 
			(byte)0,   (byte)0,   (byte)0,   (byte)16
		};
		
		/********* KEY AGREEMENT *********/
		
		/* Private key clamping
		 *   k [out] your private key for key agreement
		 *   k  [in]  32 random bytes
		 */
		public static final void clamp(byte[] k) {
			k[31] &= 0x7F;
			k[31] |= 0x40;
			k[ 0] &= 0xF8;
		}
		
		/* Key-pair generation
		 *   P  [out] your public key
		 *   s  [out] your private key for signing
		 *   k  [out] your private key for key agreement
		 *   k  [in]  32 random bytes
		 * s may be NULL if you don't care
		 *
		 * WARNING: if s is not NULL, this function has data-dependent timing */
		public static final void keygen(byte[] P, byte[] s, byte[] k) {
			clamp(k);
			core(P, s, k, null);
		}

		/* Key agreement
		 *   Z  [out] shared secret (needs hashing before use)
		 *   k  [in]  your private key for key agreement
		 *   P  [in]  peer's public key
		 */
		public static final void curve(byte[] Z, byte[] k, byte[] P) {
			core(Z, null, k, P);
		}
		
		/********* DIGITAL SIGNATURES *********/

		/* deterministic EC-KCDSA
		 *
		 *    s is the private key for signing
		 *    P is the corresponding public key
		 *    Z is the context data (signer public key or certificate, etc)
		 *
		 * signing:
		 *
		 *    m = hash(Z, message)
		 *    x = hash(m, s)
		 *    keygen25519(Y, NULL, x);
		 *    r = hash(Y);
		 *    h = m XOR r
		 *    sign25519(v, h, x, s);
		 *
		 *    output (v,r) as the signature
		 *
		 * verification:
		 *
		 *    m = hash(Z, message);
		 *    h = m XOR r
		 *    verify25519(Y, v, h, P)
		 *
		 *    confirm  r == hash(Y)
		 *
		 * It would seem to me that it would be simpler to have the signer directly do 
		 * h = hash(m, Y) and send that to the recipient instead of r, who can verify 
		 * the signature by checking h == hash(m, Y).  If there are any problems with 
		 * such a scheme, please let me know.
		 *
		 * Also, EC-KCDSA (like most DS algorithms) picks x random, which is a waste of 
		 * perfectly good entropy, but does allow Y to be calculated in advance of (or 
		 * parallel to) hashing the message.
		 */
		
		/* Signature generation primitive, calculates (x-h)s mod q
		 *   v  [out] signature value
		 *   h  [in]  signature hash (of message, signature pub key, and context data)
		 *   x  [in]  signature private key
		 *   s  [in]  private key for signing
		 * returns true on success, false on failure (use different x or h)
		 */
		public static final boolean sign(byte[] v, byte[] h, byte[] x, byte[] s) {
			/* v = (x - h) s  mod q  */
			byte[] tmp1=new byte[65];
			byte[] tmp2=new byte[33];
			int w;
			int i;
			for (i = 0; i < 32; i++)
				v[i] = 0;
			i = mula_small(v, x, 0, h, 32, -1);
			mula_small(v, v, 0, ORDER, 32, (15-v[31])/16);
			mula32(tmp1, v, s, 32, 1);
			divmod(tmp2, tmp1, 64, ORDER, 32);
			for (w = 0, i = 0; i < 32; i++)
				w |= v[i] = tmp1[i];
			return w != 0;
		}
		
		/* Signature verification primitive, calculates Y = vP + hG
		 *   Y  [out] signature public key
		 *   v  [in]  signature value
		 *   h  [in]  signature hash
		 *   P  [in]  public key
		 */
		public static final void verify(byte[] Y, byte[] v, byte[] h, byte[] P) {
			/* Y = v abs(P) + h G  */
			byte[] d=new byte[32];
		    long10[] 
		        p=new long10[]{new long10(),new long10()},
		    	s=new long10[]{new long10(),new long10()},
			    yx=new long10[]{new long10(),new long10(),new long10()},
			    yz=new long10[]{new long10(),new long10(),new long10()},
			    t1=new long10[]{new long10(),new long10(),new long10()},
			    t2=new long10[]{new long10(),new long10(),new long10()};
			
			int vi = 0, hi = 0, di = 0, nvh=0, i, j, k;

			/* set p[0] to G and p[1] to P  */

			set(p[0], 9);
			unpack(p[1], P);

			/* set s[0] to P+G and s[1] to P-G  */

			/* s[0] = (Py^2 + Gy^2 - 2 Py Gy)/(Px - Gx)^2 - Px - Gx - 486662  */
			/* s[1] = (Py^2 + Gy^2 + 2 Py Gy)/(Px - Gx)^2 - Px - Gx - 486662  */

			x_to_y2(t1[0], t2[0], p[1]);	/* t2[0] = Py^2  */
			sqrt(t1[0], t2[0]);	/* t1[0] = Py or -Py  */
			j = is_negative(t1[0]);		/*      ... check which  */
			t2[0]._0 += 39420360;		/* t2[0] = Py^2 + Gy^2  */
			mul(t2[1], BASE_2Y, t1[0]);/* t2[1] = 2 Py Gy or -2 Py Gy  */
			sub(t1[j], t2[0], t2[1]);	/* t1[0] = Py^2 + Gy^2 - 2 Py Gy  */
			add(t1[1-j], t2[0], t2[1]);/* t1[1] = Py^2 + Gy^2 + 2 Py Gy  */
			cpy(t2[0], p[1]);		/* t2[0] = Px  */
			t2[0]._0 -= 9;			/* t2[0] = Px - Gx  */
			sqr(t2[1], t2[0]);		/* t2[1] = (Px - Gx)^2  */
			recip(t2[0], t2[1], 0);	/* t2[0] = 1/(Px - Gx)^2  */
			mul(s[0], t1[0], t2[0]);	/* s[0] = t1[0]/(Px - Gx)^2  */
			sub(s[0], s[0], p[1]);	/* s[0] = t1[0]/(Px - Gx)^2 - Px  */
			s[0]._0 -= 9 + 486662;		/* s[0] = X(P+G)  */
			mul(s[1], t1[1], t2[0]);	/* s[1] = t1[1]/(Px - Gx)^2  */
			sub(s[1], s[1], p[1]);	/* s[1] = t1[1]/(Px - Gx)^2 - Px  */
			s[1]._0 -= 9 + 486662;		/* s[1] = X(P-G)  */
			mul_small(s[0], s[0], 1);	/* reduce s[0] */
			mul_small(s[1], s[1], 1);	/* reduce s[1] */


			/* prepare the chain  */
			for (i = 0; i < 32; i++) {
				vi = (vi >> 8) ^ (v[i] & 0xFF) ^ ((v[i] & 0xFF) << 1);
				hi = (hi >> 8) ^ (h[i] & 0xFF) ^ ((h[i] & 0xFF) << 1);
				nvh = ~(vi ^ hi);
				di = (nvh & (di & 0x80) >> 7) ^ vi;
				di ^= nvh & (di & 0x01) << 1;
				di ^= nvh & (di & 0x02) << 1;
				di ^= nvh & (di & 0x04) << 1;
				di ^= nvh & (di & 0x08) << 1;
				di ^= nvh & (di & 0x10) << 1;
				di ^= nvh & (di & 0x20) << 1;
				di ^= nvh & (di & 0x40) << 1;
				d[i] = (byte)di;
			}

			di = ((nvh & (di & 0x80) << 1) ^ vi) >> 8;

			/* initialize state */
			set(yx[0], 1);
			cpy(yx[1], p[di]);
			cpy(yx[2], s[0]);
			set(yz[0], 0);
			set(yz[1], 1);
			set(yz[2], 1);

			/* y[0] is (even)P + (even)G
			 * y[1] is (even)P + (odd)G  if current d-bit is 0
			 * y[1] is (odd)P + (even)G  if current d-bit is 1
			 * y[2] is (odd)P + (odd)G
			 */

			vi = 0;
			hi = 0;

			/* and go for it! */
			for (i = 32; i--!=0; ) {
				vi = (vi << 8) | (v[i] & 0xFF);
				hi = (hi << 8) | (h[i] & 0xFF);
				di = (di << 8) | (d[i] & 0xFF);

				for (j = 8; j--!=0; ) {
					mont_prep(t1[0], t2[0], yx[0], yz[0]);
					mont_prep(t1[1], t2[1], yx[1], yz[1]);
					mont_prep(t1[2], t2[2], yx[2], yz[2]);

					k = ((vi ^ vi >> 1) >> j & 1)
					  + ((hi ^ hi >> 1) >> j & 1);
					mont_dbl(yx[2], yz[2], t1[k], t2[k], yx[0], yz[0]);

					k = (di >> j & 2) ^ ((di >> j & 1) << 1);
					mont_add(t1[1], t2[1], t1[k], t2[k], yx[1], yz[1],
							p[di >> j & 1]);

					mont_add(t1[2], t2[2], t1[0], t2[0], yx[2], yz[2],
							s[((vi ^ hi) >> j & 2) >> 1]);
				}
			}

			k = (vi & 1) + (hi & 1);
			recip(t1[0], yz[k], 0);
			mul(t1[1], yx[k], t1[0]);

			pack(t1[1], Y);
		}
		
		/////////////////////////////////////////////////////////////////////////// 
		
		/* sahn0:
		 * Using this class instead of long[10] to avoid bounds checks. */
		private static final class long10 {
			public long10() {}
			public long10(
				long _0, long _1, long _2, long _3, long _4,
				long _5, long _6, long _7, long _8, long _9)
			{
				this._0=_0; this._1=_1; this._2=_2;
				this._3=_3; this._4=_4; this._5=_5;
				this._6=_6; this._7=_7; this._8=_8;
				this._9=_9;
			}
			public long _0,_1,_2,_3,_4,_5,_6,_7,_8,_9;
		}
		
		/********************* radix 2^8 math *********************/
		
		private static final void cpy32(byte[] d, byte[] s) {
			int i;
			for (i = 0; i < 32; i++)
				d[i] = s[i];
		}
		
		/* p[m..n+m-1] = q[m..n+m-1] + z * x */
		/* n is the size of x */
		/* n+m is the size of p and q */
		private static final int mula_small(byte[] p,byte[] q,int m,byte[] x,int n,int z) {
			int v=0;
			for (int i=0;i<n;++i) {
				v+=(q[i+m] & 0xFF)+z*(x[i] & 0xFF);
				p[i+m]=(byte)v;
				v>>=8;
			}
			return v;		
		}
		
		/* p += x * y * z  where z is a small integer
		 * x is size 32, y is size t, p is size 32+t
		 * y is allowed to overlap with p+32 if you don't care about the upper half  */
		private static final int mula32(byte[] p, byte[] x, byte[] y, int t, int z) {
			final int n = 31;
			int w = 0;
			int i = 0;
			for (; i < t; i++) {
				int zy = z * (y[i] & 0xFF);
				w += mula_small(p, p, i, x, n, zy) +
					(p[i+n] & 0xFF) + zy * (x[n] & 0xFF);
				p[i+n] = (byte)w;
				w >>= 8;
			}
			p[i+n] = (byte)(w + (p[i+n] & 0xFF));
			return w >> 8;
		}
		
		/* divide r (size n) by d (size t), returning quotient q and remainder r
		 * quotient is size n-t+1, remainder is size t
		 * requires t > 0 && d[t-1] != 0
		 * requires that r[-1] and d[-1] are valid memory locations
		 * q may overlap with r+t */
		private static final void divmod(byte[] q, byte[] r, int n, byte[] d, int t) {
			int rn = 0;
			int dt = ((d[t-1] & 0xFF) << 8);
			if (t>1) {
				dt |= (d[t-2] & 0xFF);
			}
			while (n-- >= t) {
				int z = (rn << 16) | ((r[n] & 0xFF) << 8);
				if (n>0) {
					z |= (r[n-1] & 0xFF);
				}
				z/=dt;
				rn += mula_small(r,r, n-t+1, d, t, -z);
				q[n-t+1] = (byte)((z + rn) & 0xFF); /* rn is 0 or -1 (underflow) */
				mula_small(r,r, n-t+1, d, t, -rn);
				rn = (r[n] & 0xFF);
				r[n] = 0;
			}
			r[t-1] = (byte)rn;
		}
		
		private static final int numsize(byte[] x,int n) {
			while (n--!=0 && x[n]==0)
				;
			return n+1;
		}
		
		/* Returns x if a contains the gcd, y if b.
		 * Also, the returned buffer contains the inverse of a mod b,
		 * as 32-byte signed.
		 * x and y must have 64 bytes space for temporary use.
		 * requires that a[-1] and b[-1] are valid memory locations  */
		private static final byte[] egcd32(byte[] x,byte[] y,byte[] a,byte[] b) {
			int an, bn = 32, qn, i;
			for (i = 0; i < 32; i++)
				x[i] = y[i] = 0;
			x[0] = 1;
			an = numsize(a, 32);
			if (an==0)
				return y;	/* division by zero */
			byte[] temp=new byte[32];
			while (true) {
				qn = bn - an + 1;
				divmod(temp, b, bn, a, an);
				bn = numsize(b, bn);
				if (bn==0)
					return x;
				mula32(y, x, temp, qn, -1);

				qn = an - bn + 1;
				divmod(temp, a, an, b, bn);
				an = numsize(a, an);
				if (an==0)
					return y;
				mula32(x, y, temp, qn, -1);
			}
		}
		
		/********************* radix 2^25.5 GF(2^255-19) math *********************/	
		
		private static final int P25=33554431;	/* (1 << 25) - 1 */
		private static final int P26=67108863;	/* (1 << 26) - 1 */
		
		/* Convert to internal format from little-endian byte format */
		private static final void unpack(long10 x,byte[] m) {
			x._0 = ((m[0] & 0xFF))         | ((m[1] & 0xFF))<<8 |
					(m[2] & 0xFF)<<16      | ((m[3] & 0xFF)& 3)<<24;
			x._1 = ((m[3] & 0xFF)&~ 3)>>2  | (m[4] & 0xFF)<<6 |
					(m[5] & 0xFF)<<14 | ((m[6] & 0xFF)& 7)<<22;
			x._2 = ((m[6] & 0xFF)&~ 7)>>3  | (m[7] & 0xFF)<<5 |
					(m[8] & 0xFF)<<13 | ((m[9] & 0xFF)&31)<<21;
			x._3 = ((m[9] & 0xFF)&~31)>>5  | (m[10] & 0xFF)<<3 |
					(m[11] & 0xFF)<<11 | ((m[12] & 0xFF)&63)<<19;
			x._4 = ((m[12] & 0xFF)&~63)>>6 | (m[13] & 0xFF)<<2 |
					(m[14] & 0xFF)<<10 |  (m[15] & 0xFF)    <<18;
			x._5 =  (m[16] & 0xFF)         | (m[17] & 0xFF)<<8 |
					(m[18] & 0xFF)<<16 | ((m[19] & 0xFF)& 1)<<24;
			x._6 = ((m[19] & 0xFF)&~ 1)>>1 | (m[20] & 0xFF)<<7 |
					(m[21] & 0xFF)<<15 | ((m[22] & 0xFF)& 7)<<23;
			x._7 = ((m[22] & 0xFF)&~ 7)>>3 | (m[23] & 0xFF)<<5 |
					(m[24] & 0xFF)<<13 | ((m[25] & 0xFF)&15)<<21;
			x._8 = ((m[25] & 0xFF)&~15)>>4 | (m[26] & 0xFF)<<4 |
					(m[27] & 0xFF)<<12 | ((m[28] & 0xFF)&63)<<20;
			x._9 = ((m[28] & 0xFF)&~63)>>6 | (m[29] & 0xFF)<<2 |
					(m[30] & 0xFF)<<10 |  (m[31] & 0xFF)    <<18;
		}
		
		/* Check if reduced-form input >= 2^255-19 */
		private static final boolean is_overflow(long10 x) {
			return (
				((x._0 > P26-19)) &&
				((x._1 & x._3 & x._5 & x._7 & x._9) == P25) &&
				((x._2 & x._4 & x._6 & x._8) == P26)
				) || (x._9 > P25);
		}
		
		/* Convert from internal format to little-endian byte format.  The 
		 * number must be in a reduced form which is output by the following ops:
		 *     unpack, mul, sqr
		 *     set --  if input in range 0 .. P25
		 * If you're unsure if the number is reduced, first multiply it by 1.  */
		private static final void pack(long10 x,byte[] m) {
			int ld = 0, ud = 0;
			long t;
			ld = (is_overflow(x)?1:0) - ((x._9 < 0)?1:0);
			ud = ld * -(P25+1);
			ld *= 19;
			t = ld + x._0 + (x._1 << 26);
			m[ 0] = (byte)t;
			m[ 1] = (byte)(t >> 8);
			m[ 2] = (byte)(t >> 16);
			m[ 3] = (byte)(t >> 24);
			t = (t >> 32) + (x._2 << 19);
			m[ 4] = (byte)t;
			m[ 5] = (byte)(t >> 8);
			m[ 6] = (byte)(t >> 16);
			m[ 7] = (byte)(t >> 24);
			t = (t >> 32) + (x._3 << 13);
			m[ 8] = (byte)t;
			m[ 9] = (byte)(t >> 8);
			m[10] = (byte)(t >> 16);
			m[11] = (byte)(t >> 24);
			t = (t >> 32) + (x._4 <<  6);
			m[12] = (byte)t;
			m[13] = (byte)(t >> 8);
			m[14] = (byte)(t >> 16);
			m[15] = (byte)(t >> 24);
			t = (t >> 32) + x._5 + (x._6 << 25);
			m[16] = (byte)t;
			m[17] = (byte)(t >> 8);
			m[18] = (byte)(t >> 16);
			m[19] = (byte)(t >> 24);
			t = (t >> 32) + (x._7 << 19);
			m[20] = (byte)t;
			m[21] = (byte)(t >> 8);
			m[22] = (byte)(t >> 16);
			m[23] = (byte)(t >> 24);
			t = (t >> 32) + (x._8 << 12);
			m[24] = (byte)t;
			m[25] = (byte)(t >> 8);
			m[26] = (byte)(t >> 16);
			m[27] = (byte)(t >> 24);
			t = (t >> 32) + ((x._9 + ud) << 6);
			m[28] = (byte)t;
			m[29] = (byte)(t >> 8);
			m[30] = (byte)(t >> 16);
			m[31] = (byte)(t >> 24);
		}

		/* Copy a number */
		private static final void cpy(long10 out, long10 in) {
			out._0=in._0;	out._1=in._1;
			out._2=in._2;	out._3=in._3;
			out._4=in._4;	out._5=in._5;
			out._6=in._6;	out._7=in._7;
			out._8=in._8;	out._9=in._9;		
		}

		/* Set a number to value, which must be in range -185861411 .. 185861411 */
		private static final void set(long10 out, int in) {
			out._0=in;	out._1=0;
			out._2=0;	out._3=0;
			out._4=0;	out._5=0;
			out._6=0;	out._7=0;
			out._8=0;	out._9=0;		
		}
		
		/* Add/subtract two numbers.  The inputs must be in reduced form, and the 
		 * output isn't, so to do another addition or subtraction on the output, 
		 * first multiply it by one to reduce it. */
		private static final void add(long10 xy, long10 x, long10 y) {
			xy._0 = x._0 + y._0;	xy._1 = x._1 + y._1;
			xy._2 = x._2 + y._2;	xy._3 = x._3 + y._3;
			xy._4 = x._4 + y._4;	xy._5 = x._5 + y._5;
			xy._6 = x._6 + y._6;	xy._7 = x._7 + y._7;
			xy._8 = x._8 + y._8;	xy._9 = x._9 + y._9;
		}
		private static final void sub(long10 xy, long10 x, long10 y) {
			xy._0 = x._0 - y._0;	xy._1 = x._1 - y._1;
			xy._2 = x._2 - y._2;	xy._3 = x._3 - y._3;
			xy._4 = x._4 - y._4;	xy._5 = x._5 - y._5;
			xy._6 = x._6 - y._6;	xy._7 = x._7 - y._7;
			xy._8 = x._8 - y._8;	xy._9 = x._9 - y._9;
		}
		
		/* Multiply a number by a small integer in range -185861411 .. 185861411.
		 * The output is in reduced form, the input x need not be.  x and xy may point
		 * to the same buffer. */
		private static final long10 mul_small(long10 xy, long10 x, long y) {
			long t;
			t = (x._8*y);
			xy._8 = (t & ((1 << 26) - 1));
			t = (t >> 26) + (x._9*y);
			xy._9 = (t & ((1 << 25) - 1));
			t = 19 * (t >> 25) + (x._0*y);
			xy._0 = (t & ((1 << 26) - 1));
			t = (t >> 26) + (x._1*y);
			xy._1 = (t & ((1 << 25) - 1));
			t = (t >> 25) + (x._2*y);
			xy._2 = (t & ((1 << 26) - 1));
			t = (t >> 26) + (x._3*y);
			xy._3 = (t & ((1 << 25) - 1));
			t = (t >> 25) + (x._4*y);
			xy._4 = (t & ((1 << 26) - 1));
			t = (t >> 26) + (x._5*y);
			xy._5 = (t & ((1 << 25) - 1));
			t = (t >> 25) + (x._6*y);
			xy._6 = (t & ((1 << 26) - 1));
			t = (t >> 26) + (x._7*y);
			xy._7 = (t & ((1 << 25) - 1));
			t = (t >> 25) + xy._8;
			xy._8 = (t & ((1 << 26) - 1));
			xy._9 += (t >> 26);
			return xy;
		}

		/* Multiply two numbers.  The output is in reduced form, the inputs need not 
		 * be. */
		private static final long10 mul(long10 xy, long10 x, long10 y) {
			/* sahn0:
			 * Using local variables to avoid class access.
			 * This seem to improve performance a bit...
			 */
			long
				x_0=x._0,x_1=x._1,x_2=x._2,x_3=x._3,x_4=x._4,
				x_5=x._5,x_6=x._6,x_7=x._7,x_8=x._8,x_9=x._9;
			long
				y_0=y._0,y_1=y._1,y_2=y._2,y_3=y._3,y_4=y._4,
				y_5=y._5,y_6=y._6,y_7=y._7,y_8=y._8,y_9=y._9;
			long t;
			t = (x_0*y_8) + (x_2*y_6) + (x_4*y_4) + (x_6*y_2) +
				(x_8*y_0) + 2 * ((x_1*y_7) + (x_3*y_5) +
						(x_5*y_3) + (x_7*y_1)) + 38 *
				(x_9*y_9);
			xy._8 = (t & ((1 << 26) - 1));
			t = (t >> 26) + (x_0*y_9) + (x_1*y_8) + (x_2*y_7) +
				(x_3*y_6) + (x_4*y_5) + (x_5*y_4) +
				(x_6*y_3) + (x_7*y_2) + (x_8*y_1) +
				(x_9*y_0);
			xy._9 = (t & ((1 << 25) - 1));
			t = (x_0*y_0) + 19 * ((t >> 25) + (x_2*y_8) + (x_4*y_6)
					+ (x_6*y_4) + (x_8*y_2)) + 38 *
				((x_1*y_9) + (x_3*y_7) + (x_5*y_5) +
				 (x_7*y_3) + (x_9*y_1));
			xy._0 = (t & ((1 << 26) - 1));
			t = (t >> 26) + (x_0*y_1) + (x_1*y_0) + 19 * ((x_2*y_9)
					+ (x_3*y_8) + (x_4*y_7) + (x_5*y_6) +
					(x_6*y_5) + (x_7*y_4) + (x_8*y_3) +
					(x_9*y_2));
			xy._1 = (t & ((1 << 25) - 1));
			t = (t >> 25) + (x_0*y_2) + (x_2*y_0) + 19 * ((x_4*y_8)
					+ (x_6*y_6) + (x_8*y_4)) + 2 * (x_1*y_1)
					+ 38 * ((x_3*y_9) + (x_5*y_7) +
							(x_7*y_5) + (x_9*y_3));
			xy._2 = (t & ((1 << 26) - 1));
			t = (t >> 26) + (x_0*y_3) + (x_1*y_2) + (x_2*y_1) +
				(x_3*y_0) + 19 * ((x_4*y_9) + (x_5*y_8) +
						(x_6*y_7) + (x_7*y_6) +
						(x_8*y_5) + (x_9*y_4));
			xy._3 = (t & ((1 << 25) - 1));
			t = (t >> 25) + (x_0*y_4) + (x_2*y_2) + (x_4*y_0) + 19 *
				((x_6*y_8) + (x_8*y_6)) + 2 * ((x_1*y_3) +
									 (x_3*y_1)) + 38 *
				((x_5*y_9) + (x_7*y_7) + (x_9*y_5));
			xy._4 = (t & ((1 << 26) - 1));
			t = (t >> 26) + (x_0*y_5) + (x_1*y_4) + (x_2*y_3) +
				(x_3*y_2) + (x_4*y_1) + (x_5*y_0) + 19 *
				((x_6*y_9) + (x_7*y_8) + (x_8*y_7) +
				 (x_9*y_6));
			xy._5 = (t & ((1 << 25) - 1));
			t = (t >> 25) + (x_0*y_6) + (x_2*y_4) + (x_4*y_2) +
				(x_6*y_0) + 19 * (x_8*y_8) + 2 * ((x_1*y_5) +
						(x_3*y_3) + (x_5*y_1)) + 38 *
				((x_7*y_9) + (x_9*y_7));
			xy._6 = (t & ((1 << 26) - 1));
			t = (t >> 26) + (x_0*y_7) + (x_1*y_6) + (x_2*y_5) +
				(x_3*y_4) + (x_4*y_3) + (x_5*y_2) +
				(x_6*y_1) + (x_7*y_0) + 19 * ((x_8*y_9) +
						(x_9*y_8));
			xy._7 = (t & ((1 << 25) - 1));
			t = (t >> 25) + xy._8;
			xy._8 = (t & ((1 << 26) - 1));
			xy._9 += (t >> 26);
			return xy;
		}

		/* Square a number.  Optimization of  mul25519(x2, x, x)  */
		private static final long10 sqr(long10 x2, long10 x) {
			long
				x_0=x._0,x_1=x._1,x_2=x._2,x_3=x._3,x_4=x._4,
				x_5=x._5,x_6=x._6,x_7=x._7,x_8=x._8,x_9=x._9;
			long t;
			t = (x_4*x_4) + 2 * ((x_0*x_8) + (x_2*x_6)) + 38 *
				(x_9*x_9) + 4 * ((x_1*x_7) + (x_3*x_5));
			x2._8 = (t & ((1 << 26) - 1));
			t = (t >> 26) + 2 * ((x_0*x_9) + (x_1*x_8) + (x_2*x_7) +
					(x_3*x_6) + (x_4*x_5));
			x2._9 = (t & ((1 << 25) - 1));
			t = 19 * (t >> 25) + (x_0*x_0) + 38 * ((x_2*x_8) +
					(x_4*x_6) + (x_5*x_5)) + 76 * ((x_1*x_9)
					+ (x_3*x_7));
			x2._0 = (t & ((1 << 26) - 1));
			t = (t >> 26) + 2 * (x_0*x_1) + 38 * ((x_2*x_9) +
					(x_3*x_8) + (x_4*x_7) + (x_5*x_6));
			x2._1 = (t & ((1 << 25) - 1));
			t = (t >> 25) + 19 * (x_6*x_6) + 2 * ((x_0*x_2) +
					(x_1*x_1)) + 38 * (x_4*x_8) + 76 *
					((x_3*x_9) + (x_5*x_7));
			x2._2 = (t & ((1 << 26) - 1));
			t = (t >> 26) + 2 * ((x_0*x_3) + (x_1*x_2)) + 38 *
				((x_4*x_9) + (x_5*x_8) + (x_6*x_7));
			x2._3 = (t & ((1 << 25) - 1));
			t = (t >> 25) + (x_2*x_2) + 2 * (x_0*x_4) + 38 *
				((x_6*x_8) + (x_7*x_7)) + 4 * (x_1*x_3) + 76 *
				(x_5*x_9);
			x2._4 = (t & ((1 << 26) - 1));
			t = (t >> 26) + 2 * ((x_0*x_5) + (x_1*x_4) + (x_2*x_3))
				+ 38 * ((x_6*x_9) + (x_7*x_8));
			x2._5 = (t & ((1 << 25) - 1));
			t = (t >> 25) + 19 * (x_8*x_8) + 2 * ((x_0*x_6) +
					(x_2*x_4) + (x_3*x_3)) + 4 * (x_1*x_5) +
					76 * (x_7*x_9);
			x2._6 = (t & ((1 << 26) - 1));
			t = (t >> 26) + 2 * ((x_0*x_7) + (x_1*x_6) + (x_2*x_5) +
					(x_3*x_4)) + 38 * (x_8*x_9);
			x2._7 = (t & ((1 << 25) - 1));
			t = (t >> 25) + x2._8;
			x2._8 = (t & ((1 << 26) - 1));
			x2._9 += (t >> 26);
			return x2;
		}
		
		/* Calculates a reciprocal.  The output is in reduced form, the inputs need not 
		 * be.  Simply calculates  y = x^(p-2)  so it's not too fast. */
		/* When sqrtassist is true, it instead calculates y = x^((p-5)/8) */
		private static final void recip(long10 y, long10 x, int sqrtassist) {
			long10
			    t0=new long10(),
				t1=new long10(),
				t2=new long10(),
				t3=new long10(),
				t4=new long10();
			int i;
			/* the chain for x^(2^255-21) is straight from djb's implementation */
			sqr(t1, x);	/*  2 == 2 * 1	*/
			sqr(t2, t1);	/*  4 == 2 * 2	*/
			sqr(t0, t2);	/*  8 == 2 * 4	*/
			mul(t2, t0, x);	/*  9 == 8 + 1	*/
			mul(t0, t2, t1);	/* 11 == 9 + 2	*/
			sqr(t1, t0);	/* 22 == 2 * 11	*/
			mul(t3, t1, t2);	/* 31 == 22 + 9
						== 2^5   - 2^0	*/
			sqr(t1, t3);	/* 2^6   - 2^1	*/
			sqr(t2, t1);	/* 2^7   - 2^2	*/
			sqr(t1, t2);	/* 2^8   - 2^3	*/
			sqr(t2, t1);	/* 2^9   - 2^4	*/
			sqr(t1, t2);	/* 2^10  - 2^5	*/
			mul(t2, t1, t3);	/* 2^10  - 2^0	*/
			sqr(t1, t2);	/* 2^11  - 2^1	*/
			sqr(t3, t1);	/* 2^12  - 2^2	*/
			for (i = 1; i < 5; i++) {
				sqr(t1, t3);
				sqr(t3, t1);
			} /* t3 */		/* 2^20  - 2^10	*/
			mul(t1, t3, t2);	/* 2^20  - 2^0	*/
			sqr(t3, t1);	/* 2^21  - 2^1	*/
			sqr(t4, t3);	/* 2^22  - 2^2	*/
			for (i = 1; i < 10; i++) {
				sqr(t3, t4);
				sqr(t4, t3);
			} /* t4 */		/* 2^40  - 2^20	*/
			mul(t3, t4, t1);	/* 2^40  - 2^0	*/
			for (i = 0; i < 5; i++) {
				sqr(t1, t3);
				sqr(t3, t1);
			} /* t3 */		/* 2^50  - 2^10	*/
			mul(t1, t3, t2);	/* 2^50  - 2^0	*/
			sqr(t2, t1);	/* 2^51  - 2^1	*/
			sqr(t3, t2);	/* 2^52  - 2^2	*/
			for (i = 1; i < 25; i++) {
				sqr(t2, t3);
				sqr(t3, t2);
			} /* t3 */		/* 2^100 - 2^50 */
			mul(t2, t3, t1);	/* 2^100 - 2^0	*/
			sqr(t3, t2);	/* 2^101 - 2^1	*/
			sqr(t4, t3);	/* 2^102 - 2^2	*/
			for (i = 1; i < 50; i++) {
				sqr(t3, t4);
				sqr(t4, t3);
			} /* t4 */		/* 2^200 - 2^100 */
			mul(t3, t4, t2);	/* 2^200 - 2^0	*/
			for (i = 0; i < 25; i++) {
				sqr(t4, t3);
				sqr(t3, t4);
			} /* t3 */		/* 2^250 - 2^50	*/
			mul(t2, t3, t1);	/* 2^250 - 2^0	*/
			sqr(t1, t2);	/* 2^251 - 2^1	*/
			sqr(t2, t1);	/* 2^252 - 2^2	*/
			if (sqrtassist!=0) {
				mul(y, x, t2);	/* 2^252 - 3 */
			} else {
				sqr(t1, t2);	/* 2^253 - 2^3	*/
				sqr(t2, t1);	/* 2^254 - 2^4	*/
				sqr(t1, t2);	/* 2^255 - 2^5	*/
				mul(y, t1, t0);	/* 2^255 - 21	*/
			}
		}
		
		/* checks if x is "negative", requires reduced input */
		private static final int is_negative(long10 x) {
			return (int)(((is_overflow(x) || (x._9 < 0))?1:0) ^ (x._0 & 1));
		}
		
		/* a square root */
		private static final void sqrt(long10 x, long10 u) {
			long10 v=new long10(), t1=new long10(), t2=new long10();
			add(t1, u, u);	/* t1 = 2u		*/
			recip(v, t1, 1);	/* v = (2u)^((p-5)/8)	*/
			sqr(x, v);		/* x = v^2		*/
			mul(t2, t1, x);	/* t2 = 2uv^2		*/
			t2._0--;		/* t2 = 2uv^2-1		*/
			mul(t1, v, t2);	/* t1 = v(2uv^2-1)	*/
			mul(x, u, t1);	/* x = uv(2uv^2-1)	*/
		}
		
		/********************* Elliptic curve *********************/
		
		/* y^2 = x^3 + 486662 x^2 + x  over GF(2^255-19) */
		
		/* t1 = ax + az
		 * t2 = ax - az  */
		private static final void mont_prep(long10 t1, long10 t2, long10 ax, long10 az) {
			add(t1, ax, az);
			sub(t2, ax, az);
		}
		
		/* A = P + Q   where
		 *  X(A) = ax/az
		 *  X(P) = (t1+t2)/(t1-t2)
		 *  X(Q) = (t3+t4)/(t3-t4)
		 *  X(P-Q) = dx
		 * clobbers t1 and t2, preserves t3 and t4  */
		private static final void mont_add(long10 t1, long10 t2, long10 t3, long10 t4,long10 ax, long10 az, long10 dx) {
			mul(ax, t2, t3);
			mul(az, t1, t4);
			add(t1, ax, az);
			sub(t2, ax, az);
			sqr(ax, t1);
			sqr(t1, t2);
			mul(az, t1, dx);
		}
		
		/* B = 2 * Q   where
		 *  X(B) = bx/bz
		 *  X(Q) = (t3+t4)/(t3-t4)
		 * clobbers t1 and t2, preserves t3 and t4  */
		private static final void mont_dbl(long10 t1, long10 t2, long10 t3, long10 t4,long10 bx, long10 bz) {
			sqr(t1, t3);
			sqr(t2, t4);
			mul(bx, t1, t2);
			sub(t2, t1, t2);
			mul_small(bz, t2, 121665);
			add(t1, t1, bz);
			mul(bz, t1, t2);
		}
		
		/* Y^2 = X^3 + 486662 X^2 + X
		 * t is a temporary  */
		private static final void x_to_y2(long10 t, long10 y2, long10 x) {
			sqr(t, x);
			mul_small(y2, x, 486662);
			add(t, t, y2);
			t._0++;
			mul(y2, t, x);
		}
		
		/* P = kG   and  s = sign(P)/k  */
		private static final void core(byte[] Px, byte[] s, byte[] k, byte[] Gx) {
			long10 
			    dx=new long10(),
			    t1=new long10(),
			    t2=new long10(),
			    t3=new long10(),
			    t4=new long10();
		    long10[] 
		        x=new long10[]{new long10(),new long10()},
		    	z=new long10[]{new long10(),new long10()};
			int i, j;

			/* unpack the base */
			if (Gx!=null)
				unpack(dx, Gx);
			else
				set(dx, 9);

			/* 0G = point-at-infinity */
			set(x[0], 1);
			set(z[0], 0);

			/* 1G = G */
			cpy(x[1], dx);
			set(z[1], 1);

			for (i = 32; i--!=0; ) {
				if (i==0) {
					i=0;
				}
				for (j = 8; j--!=0; ) {
					/* swap arguments depending on bit */
					int bit1 = (k[i] & 0xFF) >> j & 1;
					int bit0 = ~(k[i] & 0xFF) >> j & 1;
					long10 ax = x[bit0];
					long10 az = z[bit0];
					long10 bx = x[bit1];
					long10 bz = z[bit1];

					/* a' = a + b	*/
					/* b' = 2 b	*/
					mont_prep(t1, t2, ax, az);
					mont_prep(t3, t4, bx, bz);
					mont_add(t1, t2, t3, t4, ax, az, dx);
					mont_dbl(t1, t2, t3, t4, bx, bz);
				}
			}

			recip(t1, z[0], 0);
			mul(dx, x[0], t1);
			pack(dx, Px);

			/* calculate s such that s abs(P) = G  .. assumes G is std base point */
			if (s!=null) {
				x_to_y2(t2, t1, dx);	/* t1 = Py^2  */
				recip(t3, z[1], 0);	/* where Q=P+G ... */
				mul(t2, x[1], t3);	/* t2 = Qx  */
				add(t2, t2, dx);	/* t2 = Qx + Px  */
				t2._0 += 9 + 486662;	/* t2 = Qx + Px + Gx + 486662  */
				dx._0 -= 9;		/* dx = Px - Gx  */
				sqr(t3, dx);	/* t3 = (Px - Gx)^2  */
				mul(dx, t2, t3);	/* dx = t2 (Px - Gx)^2  */
				sub(dx, dx, t1);	/* dx = t2 (Px - Gx)^2 - Py^2  */
				dx._0 -= 39420360;	/* dx = t2 (Px - Gx)^2 - Py^2 - Gy^2  */
				mul(t1, dx, BASE_R2Y);	/* t1 = -Py  */
				if (is_negative(t1)!=0)	/* sign is 1, so just copy  */
					cpy32(s, k);
				else			/* sign is -1, so negate  */
					mula_small(s, ORDER_TIMES_8, 0, k, 32, -1);

				/* reduce s mod q
				 * (is this needed?  do it just in case, it's fast anyway) */
				//divmod((dstptr) t1, s, 32, order25519, 32);

				/* take reciprocal of s mod q */
				byte[] temp1=new byte[32];
				byte[] temp2=new byte[64];
				byte[] temp3=new byte[64];
				cpy32(temp1, ORDER);
				cpy32(s, egcd32(temp2, temp3, s, temp1));
				if ((s[31] & 0x80)!=0)
					mula_small(s, s, 0, ORDER, 32, 1);
			}
		}

		/* smallest multiple of the order that's >= 2^255 */
		private static final byte[] ORDER_TIMES_8 = {
			(byte)104, (byte)159, (byte)174, (byte)231,
			(byte)210, (byte)24,  (byte)147, (byte)192,
			(byte)178, (byte)230, (byte)188, (byte)23,
			(byte)245, (byte)206, (byte)247, (byte)166,
			(byte)0,   (byte)0,   (byte)0,   (byte)0,  
			(byte)0,   (byte)0,   (byte)0,   (byte)0,
			(byte)0,   (byte)0,   (byte)0,   (byte)0,  
			(byte)0,   (byte)0,   (byte)0,   (byte)128
		};
		
		/* constants 2Gy and 1/(2Gy) */
		private static final long10 BASE_2Y = new long10(
			39999547, 18689728, 59995525, 1648697, 57546132,
			24010086, 19059592, 5425144, 63499247, 16420658
		);
		private static final long10 BASE_R2Y = new long10(
			5744, 8160848, 4790893, 13779497, 35730846,
			12541209, 49101323, 30047407, 40071253, 6226132
		);
	}

/**
* High-performance base64 codec based on the algorithm used in Mikael Grev's MiG Base64.
* This implementation is designed to handle base64 without line splitting and with
* optional padding. Alternative character tables may be supplied to the {@code encode}
* and {@code decode} methods to implement modified base64 schemes.
*
* Decoding assumes correct input, the caller is responsible for ensuring that the input
* contains no invalid characters.
*
* @author Will Glozer
*/
class Base64 {
    private static final char[] encode = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".toCharArray();
    private static final int[] decode = new int[128];
    private static final char pad = '=';

    static {
        Arrays.fill(decode, -1);
        for (int i = 0; i < encode.length; i++) {
            decode[encode[i]] = i;
        }
        decode[pad] = 0;
    }

    /**
* Decode base64 chars to bytes.
*
* @param chars Chars to encode.
*
* @return Decoded bytes.
*/
    public static byte[] decode(char[] chars) {
        return decode(chars, decode, pad);
    }

    /**
* Encode bytes to base64 chars, with padding.
*
* @param bytes Bytes to encode.
*
* @return Encoded chars.
*/
    public static char[] encode(byte[] bytes) {
        return encode(bytes, encode, pad);
    }

    /**
* Encode bytes to base64 chars, with optional padding.
*
* @param bytes Bytes to encode.
* @param padded Add padding to output.
*
* @return Encoded chars.
*/
    public static char[] encode(byte[] bytes, boolean padded) {
        return encode(bytes, encode, padded ? pad : 0);
    }

    /**
* Decode base64 chars to bytes using the supplied decode table and padding
* character.
*
* @param src Base64 encoded data.
* @param table Decode table.
* @param pad Padding character.
*
* @return Decoded bytes.
*/
    public static byte[] decode(char[] src, int[] table, char pad) {
        int len = src.length;

        if (len == 0) return new byte[0];

        int padCount = (src[len - 1] == pad ? (src[len - 2] == pad ? 2 : 1) : 0);
        int bytes = (len * 6 >> 3) - padCount;
        int blocks = (bytes / 3) * 3;

        byte[] dst = new byte[bytes];
        int si = 0, di = 0;

        while (di < blocks) {
            int n = table[src[si++]] << 18 | table[src[si++]] << 12 | table[src[si++]] << 6 | table[src[si++]];
            dst[di++] = (byte) (n >> 16);
            dst[di++] = (byte) (n >> 8);
            dst[di++] = (byte) n;
        }

        if (di < bytes) {
            int n = 0;
            switch (len - si) {
                case 4: n |= table[src[si+3]];
                case 3: n |= table[src[si+2]] << 6;
                case 2: n |= table[src[si+1]] << 12;
                case 1: n |= table[src[si]] << 18;
            }
            for (int r = 16; di < bytes; r -= 8) {
                dst[di++] = (byte) (n >> r);
            }
        }

        return dst;
    }

    /**
* Encode bytes to base64 chars using the supplied encode table and with
* optional padding.
*
* @param src Bytes to encode.
* @param table Encoding table.
* @param pad Padding character, or 0 for no padding.
*
* @return Encoded chars.
*/
    public static char[] encode(byte[] src, char[] table, char pad) {
        int len = src.length;

        if (len == 0) return new char[0];

        int blocks = (len / 3) * 3;
        int chars = ((len - 1) / 3 + 1) << 2;
        int tail = len - blocks;
        if (pad == 0 && tail > 0) chars -= 3 - tail;

        char[] dst = new char[chars];
        int si = 0, di = 0;

        while (si < blocks) {
            int n = (src[si++] & 0xff) << 16 | (src[si++] & 0xff) << 8 | (src[si++] & 0xff);
            dst[di++] = table[(n >>> 18) & 0x3f];
            dst[di++] = table[(n >>> 12) & 0x3f];
            dst[di++] = table[(n >>> 6) & 0x3f];
            dst[di++] = table[n & 0x3f];
        }

        if (tail > 0) {
            int n = (src[si] & 0xff) << 10;
            if (tail == 2) n |= (src[++si] & 0xff) << 2;

            dst[di++] = table[(n >>> 12) & 0x3f];
            dst[di++] = table[(n >>> 6) & 0x3f];
            if (tail == 2) dst[di++] = table[n & 0x3f];

            if (pad != 0) {
                if (tail == 1) dst[di++] = pad;
                dst[di] = pad;
            }
        }

        return dst;
    }
}
/**
* An implementation of the Password-Based Key Derivation Function as specified
* in RFC 2898.
*
* @author Will Glozer
*/
class PBKDF {
    /**
* Implementation of PBKDF2 (RFC2898).
*
* @param alg HMAC algorithm to use.
* @param P Password.
* @param S Salt.
* @param c Iteration count.
* @param dkLen Intended length, in octets, of the derived key.
*
* @return The derived key.
*
* @throws GeneralSecurityException
*/
    public static byte[] pbkdf2(String alg, byte[] P, byte[] S, int c, int dkLen) throws GeneralSecurityException {
        Mac mac = Mac.getInstance(alg);
        mac.init(new SecretKeySpec(P, alg));
        byte[] DK = new byte[dkLen];
        pbkdf2(mac, S, c, DK, dkLen);
        return DK;
    }

    /**
* Implementation of PBKDF2 (RFC2898).
*
* @param mac Pre-initialized {@link Mac} instance to use.
* @param S Salt.
* @param c Iteration count.
* @param DK Byte array that derived key will be placed in.
* @param dkLen Intended length, in octets, of the derived key.
*
* @throws GeneralSecurityException
*/
    public static void pbkdf2(Mac mac, byte[] S, int c, byte[] DK, int dkLen) throws GeneralSecurityException {
        int hLen = mac.getMacLength();

        if (dkLen > (Math.pow(2, 32) - 1) * hLen) {
            throw new GeneralSecurityException("Requested key length too long");
        }

        byte[] U = new byte[hLen];
        byte[] T = new byte[hLen];
        byte[] block1 = new byte[S.length + 4];

        int l = (int) Math.ceil((double) dkLen / hLen);
        int r = dkLen - (l - 1) * hLen;

        arraycopy(S, 0, block1, 0, S.length);

        for (int i = 1; i <= l; i++) {
            block1[S.length + 0] = (byte) (i >> 24 & 0xff);
            block1[S.length + 1] = (byte) (i >> 16 & 0xff);
            block1[S.length + 2] = (byte) (i >> 8 & 0xff);
            block1[S.length + 3] = (byte) (i >> 0 & 0xff);

            mac.update(block1);
            mac.doFinal(U, 0);
            arraycopy(U, 0, T, 0, hLen);

            for (int j = 1; j < c; j++) {
                mac.update(U);
                mac.doFinal(U, 0);

                for (int k = 0; k < hLen; k++) {
                    T[k] ^= U[k];
                }
            }

            arraycopy(T, 0, DK, (i - 1) * hLen, (i == l ? r : hLen));
        }
    }
}
/**
* Exception thrown when the current platform cannot be detected.
*
* @author Will Glozer
*/
class UnsupportedPlatformException extends RuntimeException {
    public UnsupportedPlatformException(String s) {
        super(s);
    }
}
/**
* A native library loader that simply invokes {@link System#loadLibrary}. The shared
* library path and filename are platform specific.
*
* @author Will Glozer
*/
class SysLibraryLoader implements LibraryLoader {
    /**
* Load a shared library.
*
* @param name Name of the library to load.
* @param verify Ignored, no verification is done.
*
* @return true if the library was successfully loaded.
*/
    public boolean load(String name, boolean verify) {
        boolean loaded;

        try {
            System.loadLibrary(name);
            loaded = true;
        } catch (Throwable e) {
            loaded = false;
        }

        return loaded;
    }
}
/**
* A platform is a unique combination of CPU architecture and operating system. This class
* attempts to determine the platform it is executing on by examining and normalizing the
* <code>os.arch</code> and <code>os.name</code> system properties.
*
* @author Will Glozer
*/
class Platform {
    public enum Arch {
        x86 ("x86|i386"),
        x86_64("x86_64|amd64");

        Pattern pattern;

        Arch(String pattern) {
            this.pattern = Pattern.compile("\\A" + pattern + "\\Z", CASE_INSENSITIVE);
        }
    }

    public enum OS {
        darwin ("darwin|mac os x"),
        freebsd("freebsd"),
        linux ("linux");

        Pattern pattern;

        OS(String pattern) {
            this.pattern = Pattern.compile("\\A" + pattern + "\\Z", CASE_INSENSITIVE);
        }
    }

    public final Arch arch;
    public final OS os;

    private Platform(Arch arch, OS os) {
        this.arch = arch;
        this.os = os;
    }

    /**
* Attempt to detect the current platform.
*
* @return The current platform.
*
* @throws UnsupportedPlatformException if the platform cannot be detected.
*/
    public static Platform detect() throws UnsupportedPlatformException {
        String osArch = getProperty("os.arch");
        String osName = getProperty("os.name");

        for (Arch arch : Arch.values()) {
            if (arch.pattern.matcher(osArch).matches()) {
                for (OS os : OS.values()) {
                    if (os.pattern.matcher(osName).matches()) {
                        return new Platform(arch, os);
                    }
                }
            }
        }

        String msg = String.format("Unsupported platform %s %s", osArch, osName);
        throw new UnsupportedPlatformException(msg);
    }
}
/**
* A native library loader that refuses to load libraries.
*
* @author Will Glozer
*/
class NilLibraryLoader implements LibraryLoader {
    /**
* Don't load a shared library.
*
* @param name Name of the library to load.
* @param verify Ignored, no verification is done.
*
* @return false.
*/
    public boolean load(String name, boolean verify) {
        return false;
    }
}
/**
* A native library loader that will extract and load a shared library contained in a jar.
* This loader will attempt to detect the {@link Platform platform} (CPU architecture and OS)
* it is running on and load the appropriate shared library.
*
* Given a library path and name this loader looks for a native library with path
* [libraryPath]/[arch]/[os]/lib[name].[ext]
*
* @author Will Glozer
*/
class JarLibraryLoader implements LibraryLoader {
    private final CodeSource codeSource;
    private final String libraryPath;

    /**
* Initialize a new instance that looks for shared libraries located in the same jar
* as this class and with a path starting with {@code lib}.
*/
    public JarLibraryLoader() {
        this(JarLibraryLoader.class.getProtectionDomain().getCodeSource(), "lib");
    }

    /**
* Initialize a new instance that looks for shared libraries located in the specified
* directory of the supplied code source.
*
* @param codeSource Code source containing shared libraries.
* @param libraryPath Path prefix of shared libraries.
*/
    public JarLibraryLoader(CodeSource codeSource, String libraryPath) {
        this.codeSource = codeSource;
        this.libraryPath = libraryPath;
    }

    /**
* Load a shared library, and optionally verify the jar signatures.
*
* @param name Name of the library to load.
* @param verify Verify the jar file if signed.
*
* @return true if the library was successfully loaded.
*/
    public boolean load(String name, boolean verify) {
        boolean loaded = false;

        try {
            Platform platform = Platform.detect();
            JarFile jar = new JarFile(codeSource.getLocation().getPath(), verify);
            try {
                for (String path : libCandidates(platform, name)) {
                    JarEntry entry = jar.getJarEntry(path);
                    if (entry == null) continue;

                    File lib = extract(name, jar.getInputStream(entry));
                    System.load(lib.getAbsolutePath());
                    lib.delete();

                    loaded = true;
                    break;
                }
            } finally {
                jar.close();
            }
        } catch (Throwable e) {
            loaded = false;
        }

        return loaded;
    }

    /**
* Extract a jar entry to a temp file.
*
* @param name Name prefix for temp file.
* @param is Jar entry input stream.
*
* @return A temporary file.
*
* @throws IOException when an IO error occurs.
*/
    private static File extract(String name, InputStream is) throws IOException {
        byte[] buf = new byte[4096];
        int len;

        File lib = File.createTempFile(name, "lib");
        FileOutputStream os = new FileOutputStream(lib);

        try {
            while ((len = is.read(buf)) > 0) {
                os.write(buf, 0, len);
            }
        } catch (IOException e) {
            lib.delete();
            throw e;
        } finally {
            os.close();
            is.close();
        }

        return lib;
    }

    /**
* Generate a list of candidate libraries for the supplied library name and suitable
* for the current platform.
*
* @param platform Current platform.
* @param name Library name.
*
* @return List of potential library names.
*/
    private List<String> libCandidates(Platform platform, String name) {
        List<String> candidates = new ArrayList<String>();
        StringBuilder sb = new StringBuilder();

        sb.append(libraryPath).append("/");
        sb.append(platform.arch).append("/");
        sb.append(platform.os).append("/");
        sb.append("lib").append(name);

        switch (platform.os) {
            case darwin:
                candidates.add(sb + ".dylib");
                candidates.add(sb + ".jnilib");
                break;
            case linux:
            case freebsd:
                candidates.add(sb + ".so");
                break;
        }

        return candidates;
    }
}
/**
* {@code LibraryLoaders} will create the appropriate {@link LibraryLoader} for
* the VM it is running on.
*
* The system property {@code com.lambdaworks.jni.loader} may be used to override
* loader auto-detection, or to disable loading native libraries entirely via use
* of the nil loader.
*
* @author Will Glozer
*/
class LibraryLoaders {
    /**
* Create a new {@link LibraryLoader} for the current VM.
*
* @return the loader.
*/
    public static LibraryLoader loader() {
        String type = System.getProperty("com.lambdaworks.jni.loader");

        if (type != null) {
            if (type.equals("sys")) return new SysLibraryLoader();
            if (type.equals("nil")) return new NilLibraryLoader();
            if (type.equals("jar")) return new JarLibraryLoader();
            throw new IllegalStateException("Illegal value for com.lambdaworks.jni.loader: " + type);
        }

        String vmSpec = System.getProperty("java.vm.specification.name");
        return vmSpec.startsWith("Java") ? new JarLibraryLoader() : new SysLibraryLoader();
    }
}
/**
* A {@code LibraryLoader} attempts to load the appropriate native library
* for the current platform.
*
* @author Will Glozer
*/
interface LibraryLoader {
    /**
* Load a native library, and optionally verify any signatures.
*
* @param name Name of the library to load.
* @param verify Verify signatures if signed.
*
* @return true if the library was successfully loaded.
*/
    boolean load(String name, boolean verify);
}
/**
* An implementation of the <a href="http://www.tarsnap.com/scrypt/scrypt.pdf"/>scrypt</a>
* key derivation function. This class will attempt to load a native library
* containing the optimized C implementation from
* <a href="http://www.tarsnap.com/scrypt.html">http://www.tarsnap.com/scrypt.html<a> and
* fall back to the pure Java version if that fails.
*
* @author Will Glozer
*/
class SCrypt {
    private static boolean native_library_loaded;

    {
        LibraryLoader loader = LibraryLoaders.loader();
        native_library_loaded = loader.load("scrypt", true);
    }

    /**
* Implementation of the <a href="http://www.tarsnap.com/scrypt/scrypt.pdf"/>scrypt KDF</a>.
* Calls the native implementation {@link #scryptN} when the native library was successfully
* loaded, otherwise calls {@link #scryptJ}.
*
* @param passwd Password.
* @param salt Salt.
* @param N CPU cost parameter.
* @param r Memory cost parameter.
* @param p Parallelization parameter.
* @param dkLen Intended length of the derived key.
*
* @return The derived key.
*
* @throws GeneralSecurityException when HMAC_SHA256 is not available.
*/
    public static byte[] scrypt(byte[] passwd, byte[] salt, int N, int r, int p, int dkLen) throws GeneralSecurityException {
        return native_library_loaded ? scryptN(passwd, salt, N, r, p, dkLen) : scryptJ(passwd, salt, N, r, p, dkLen);
    }

    /**
* Native C implementation of the <a href="http://www.tarsnap.com/scrypt/scrypt.pdf"/>scrypt KDF</a> using
* the code from <a href="http://www.tarsnap.com/scrypt.html">http://www.tarsnap.com/scrypt.html<a>.
*
* @param passwd Password.
* @param salt Salt.
* @param N CPU cost parameter.
* @param r Memory cost parameter.
* @param p Parallelization parameter.
* @param dkLen Intended length of the derived key.
*
* @return The derived key.
*/
    public static native byte[] scryptN(byte[] passwd, byte[] salt, int N, int r, int p, int dkLen);

    /**
* Pure Java implementation of the <a href="http://www.tarsnap.com/scrypt/scrypt.pdf"/>scrypt KDF</a>.
*
* @param passwd Password.
* @param salt Salt.
* @param N CPU cost parameter.
* @param r Memory cost parameter.
* @param p Parallelization parameter.
* @param dkLen Intended length of the derived key.
*
* @return The derived key.
*
* @throws GeneralSecurityException when HMAC_SHA256 is not available.
*/
    public static byte[] scryptJ(byte[] passwd, byte[] salt, int N, int r, int p, int dkLen) throws GeneralSecurityException {
        if (N < 2 || (N & (N - 1)) != 0) throw new IllegalArgumentException("N must be a power of 2 greater than 1");

        if (N > MAX_VALUE / 128 / r) throw new IllegalArgumentException("Parameter N is too large");
        if (r > MAX_VALUE / 128 / p) throw new IllegalArgumentException("Parameter r is too large");

        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(new SecretKeySpec(passwd, "HmacSHA256"));

        byte[] DK = new byte[dkLen];

        byte[] B = new byte[128 * r * p];
        byte[] XY = new byte[256 * r];
        byte[] V = new byte[128 * r * N];
        int i;

        PBKDF.pbkdf2(mac, salt, 1, B, p * 128 * r);

        for (i = 0; i < p; i++) {
            smix(B, i * 128 * r, r, N, V, XY);
        }

        PBKDF.pbkdf2(mac, B, 1, DK, dkLen);

        return DK;
    }

    public static void smix(byte[] B, int Bi, int r, int N, byte[] V, byte[] XY) {
        int Xi = 0;
        int Yi = 128 * r;
        int i;

        arraycopy(B, Bi, XY, Xi, 128 * r);

        for (i = 0; i < N; i++) {
            arraycopy(XY, Xi, V, i * (128 * r), 128 * r);
            blockmix_salsa8(XY, Xi, Yi, r);
        }

        for (i = 0; i < N; i++) {
            int j = integerify(XY, Xi, r) & (N - 1);
            blockxor(V, j * (128 * r), XY, Xi, 128 * r);
            blockmix_salsa8(XY, Xi, Yi, r);
        }

        arraycopy(XY, Xi, B, Bi, 128 * r);
    }

    public static void blockmix_salsa8(byte[] BY, int Bi, int Yi, int r) {
        byte[] X = new byte[64];
        int i;

        arraycopy(BY, Bi + (2 * r - 1) * 64, X, 0, 64);

        for (i = 0; i < 2 * r; i++) {
            blockxor(BY, i * 64, X, 0, 64);
            salsa20_8(X);
            arraycopy(X, 0, BY, Yi + (i * 64), 64);
        }

        for (i = 0; i < r; i++) {
            arraycopy(BY, Yi + (i * 2) * 64, BY, Bi + (i * 64), 64);
        }

        for (i = 0; i < r; i++) {
            arraycopy(BY, Yi + (i * 2 + 1) * 64, BY, Bi + (i + r) * 64, 64);
        }
    }

    public static int R(int a, int b) {
        return (a << b) | (a >>> (32 - b));
    }

    public static void salsa20_8(byte[] B) {
        int[] B32 = new int[16];
        int[] x = new int[16];
        int i;

        for (i = 0; i < 16; i++) {
            B32[i] = (B[i * 4 + 0] & 0xff) << 0;
            B32[i] |= (B[i * 4 + 1] & 0xff) << 8;
            B32[i] |= (B[i * 4 + 2] & 0xff) << 16;
            B32[i] |= (B[i * 4 + 3] & 0xff) << 24;
        }

        arraycopy(B32, 0, x, 0, 16);

        for (i = 8; i > 0; i -= 2) {
            x[ 4] ^= R(x[ 0]+x[12], 7); x[ 8] ^= R(x[ 4]+x[ 0], 9);
            x[12] ^= R(x[ 8]+x[ 4],13); x[ 0] ^= R(x[12]+x[ 8],18);
            x[ 9] ^= R(x[ 5]+x[ 1], 7); x[13] ^= R(x[ 9]+x[ 5], 9);
            x[ 1] ^= R(x[13]+x[ 9],13); x[ 5] ^= R(x[ 1]+x[13],18);
            x[14] ^= R(x[10]+x[ 6], 7); x[ 2] ^= R(x[14]+x[10], 9);
            x[ 6] ^= R(x[ 2]+x[14],13); x[10] ^= R(x[ 6]+x[ 2],18);
            x[ 3] ^= R(x[15]+x[11], 7); x[ 7] ^= R(x[ 3]+x[15], 9);
            x[11] ^= R(x[ 7]+x[ 3],13); x[15] ^= R(x[11]+x[ 7],18);
            x[ 1] ^= R(x[ 0]+x[ 3], 7); x[ 2] ^= R(x[ 1]+x[ 0], 9);
            x[ 3] ^= R(x[ 2]+x[ 1],13); x[ 0] ^= R(x[ 3]+x[ 2],18);
            x[ 6] ^= R(x[ 5]+x[ 4], 7); x[ 7] ^= R(x[ 6]+x[ 5], 9);
            x[ 4] ^= R(x[ 7]+x[ 6],13); x[ 5] ^= R(x[ 4]+x[ 7],18);
            x[11] ^= R(x[10]+x[ 9], 7); x[ 8] ^= R(x[11]+x[10], 9);
            x[ 9] ^= R(x[ 8]+x[11],13); x[10] ^= R(x[ 9]+x[ 8],18);
            x[12] ^= R(x[15]+x[14], 7); x[13] ^= R(x[12]+x[15], 9);
            x[14] ^= R(x[13]+x[12],13); x[15] ^= R(x[14]+x[13],18);
        }

        for (i = 0; i < 16; ++i) B32[i] = x[i] + B32[i];

        for (i = 0; i < 16; i++) {
            B[i * 4 + 0] = (byte) (B32[i] >> 0 & 0xff);
            B[i * 4 + 1] = (byte) (B32[i] >> 8 & 0xff);
            B[i * 4 + 2] = (byte) (B32[i] >> 16 & 0xff);
            B[i * 4 + 3] = (byte) (B32[i] >> 24 & 0xff);
        }
    }

    public static void blockxor(byte[] S, int Si, byte[] D, int Di, int len) {
        for (int i = 0; i < len; i++) {
            D[Di + i] ^= S[Si + i];
        }
    }

    public static int integerify(byte[] B, int Bi, int r) {
        int n;

        Bi += (2 * r - 1) * 64;

        n = (B[Bi + 0] & 0xff) << 0;
        n |= (B[Bi + 1] & 0xff) << 8;
        n |= (B[Bi + 2] & 0xff) << 16;
        n |= (B[Bi + 3] & 0xff) << 24;

        return n;
    }
}
/**
* Simple {@link SCrypt} interface for hashing passwords using the
* <a href="http://www.tarsnap.com/scrypt.html">scrypt</a> key derivation function
* and comparing a plain text password to a hashed one. The hashed output is an
* extended implementation of the Modular Crypt Format that also includes the scrypt
* algorithm parameters.
*
* Format: <code>$s0$PARAMS$SALT$KEY</code>.
*
* <dl>
* <dd>PARAMS</dd><dt>32-bit hex integer containing log2(N) (16 bits), r (8 bits), and p (8 bits)</dt>
* <dd>SALT</dd><dt>base64-encoded salt</dt>
* <dd>KEY</dd><dt>base64-encoded derived key</dt>
* </dl>
*
* <code>s0</code> identifies version 0 of the scrypt format, using a 128-bit salt and 256-bit derived key.
*
* @author Will Glozer
*/
class SCryptUtil {
    /**
* Hash the supplied plaintext password and generate output in the format described
* in {@link SCryptUtil}.
*
* @param passwd Password.
* @param N CPU cost parameter.
* @param r Memory cost parameter.
* @param p Parallelization parameter.
*
* @return The hashed password.
*/
    public static String scrypt(String passwd, int N, int r, int p) {
        try {
            byte[] salt = new byte[4];
            SecureRandom.getInstance("SHA1PRNG").nextBytes(salt);

            byte[] derived = SCrypt.scrypt(passwd.getBytes("UTF-8"), salt, N, r, p, 16);

            String params = Long.toString(log2(N) << 16L | r << 8 | p, 16);

            StringBuilder sb = new StringBuilder((salt.length + derived.length) * 2);
            sb.append("$s0$").append(params).append('$');
            sb.append(Base64.encode(salt)).append('$');
            sb.append(Base64.encode(derived));

            return sb.toString();
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException("JVM doesn't support UTF-8?");
        } catch (GeneralSecurityException e) {
            throw new IllegalStateException("JVM doesn't support SHA1PRNG or HMAC_SHA256?");
        }
    }

    /**
* Compare the supplied plaintext password to a hashed password.
*
* @param passwd Plaintext password.
* @param hashed scrypt hashed password.
*
* @return true if passwd matches hashed value.
*/
    public static boolean check(String passwd, String hashed) {
        try {
            String[] parts = hashed.split("\\$");

            if (parts.length != 5 || !parts[1].equals("s0")) {
                throw new IllegalArgumentException("Invalid hashed value");
            }

            long params = Long.parseLong(parts[2], 16);
            byte[] salt = Base64.decode(parts[3].toCharArray());
            byte[] derived0 = Base64.decode(parts[4].toCharArray());

            int N = (int) Math.pow(2, params >> 16 & 0xffff);
            int r = (int) params >> 8 & 0xff;
            int p = (int) params & 0xff;

            byte[] derived1 = SCrypt.scrypt(passwd.getBytes("UTF-8"), salt, N, r, p, 16);

            if (derived0.length != derived1.length) return false;

            int result = 0;
            for (int i = 0; i < derived0.length; i++) {
                result |= derived0[i] ^ derived1[i];
            }
            return result == 0;
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException("JVM doesn't support UTF-8?");
        } catch (GeneralSecurityException e) {
            throw new IllegalStateException("JVM doesn't support SHA1PRNG or HMAC_SHA256?");
        }
    }

    private static int log2(int n) {
        int log = 0;
        if ((n & 0xffff0000 ) != 0) { n >>>= 16; log = 16; }
        if (n >= 256) { n >>>= 8; log += 8; }
        if (n >= 16 ) { n >>>= 4; log += 4; }
        if (n >= 4 ) { n >>>= 2; log += 2; }
        return log + (n >>> 1);
    }
}
class PBKDF2 {
    // The higher the number of iterations the more 
    // expensive computing the hash is for us
    // and also for a brute force attack.
    private static final int iterations = 10*1024;
    private static final int saltLen = 32;
    private static final int desiredKeyLen = 256;

    /** Computes a salted PBKDF2 hash of given plaintext password
        suitable for storing in a database. 
        Empty passwords are not supported. */
    public static String getSaltedHash(String password) throws Exception {
        byte[] salt = SecureRandom.getInstance("SHA1PRNG").generateSeed(saltLen);
        // store the salt with the password
        return Base64.encode(salt) + "$" + hash(password, salt);
    }

    /** Checks whether given plaintext password corresponds 
        to a stored salted hash of the password. */
    public static boolean check(String password, String stored) throws Exception{
        String[] saltAndPass = stored.split("\\$");
        if (saltAndPass.length != 2)
            return false;
        String hashOfInput = hash(password, Base64.decode(saltAndPass[0].toCharArray()));
        return hashOfInput.equals(saltAndPass[1]);
    }

    // using PBKDF2 from Sun, an alternative is https://github.com/wg/scrypt
    // cf. http://www.unlimitednovelty.com/2012/03/dont-use-bcrypt.html
    private static String hash(String password, byte[] salt) throws Exception {
        if (password == null || password.length() == 0)
            throw new IllegalArgumentException("Empty passwords are not supported.");
        SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        SecretKey key = f.generateSecret(new PBEKeySpec(
            password.toCharArray(), salt, iterations, desiredKeyLen)
        );
        return Base64.encode(key.getEncoded()).toString();
    }
}
class AES {

    public String pad2(String n) {
        if (n.length() < 2) {
            return "0" + n;
        } else {
            return n;
        }
    }

    public String hex(byte[] bytes) {
        String r = "";
        for (int i = 0; i < bytes.length; i++) {
            r = r + pad2(Integer.toHexString(bytes[i] + 128));
        }
        return r;
    }

    public String safePassword(String unsafe) {
        String safe = unsafe;
        if (safe.length() > 16) {
            safe = safe.substring(0, 16);
        }
        int nn = safe.length();
        for (int i = nn - 1; i < 15; i++) {
            safe = safe + "*";
        }
        return safe;
    }

    public String encrypt(String value, String password) throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
        //value = new sun.misc.BASE64Encoder().encode(value.getBytes("UTF-8"));
        SecretKey key = new SecretKeySpec(safePassword(password).getBytes("UTF-8"), "AES");
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding", "SunJCE");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] encrypted = cipher.doFinal(value.getBytes("UTF-8"));
        return nxt.util.Convert.toHexString(encrypted);
    }

    public int parseInt2(String s) {
        return (new java.math.BigInteger(s, 16)).intValue();
    }

    public byte[] fromHex(String enc) {
        byte[] r = new byte[enc.length() / 2];
        for (int i = 0; i < r.length; i++) {
            int n = parseInt2(enc.substring(i * 2, i * 2 + 2)) - 128;
            r[i] = (byte) n;
        }
        return r;
    }

    public String decrypt(String value, String password) throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, IOException {
        byte[] encypted = nxt.util.Convert.parseHexString(value);
        //encypted = new sun.misc.BASE64Decoder().decodeBuffer(new String(encypted));
        SecretKey key = new SecretKeySpec(safePassword(password).getBytes("UTF-8"), "AES");
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding", "SunJCE");
        cipher.init(Cipher.DECRYPT_MODE, key);
        byte[] decrypted = cipher.doFinal(encypted);
        return new String(decrypted, "UTF-8");
    }
}  