/*
 * The MIT License
 *
 * Copyright 2014 fmiboy.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package clienxt;

/**
 *
 * @author fmiboy
 */
import java.util.*;
// This class downloads a file from a URL.
class AccountGUI extends Observable implements Runnable {
// Max size of download buffer.
private static final int MAX_BUFFER_SIZE = 1024;
// These are the status names.
public static final String STATUSES[] = {"Forging",
"Paused", "Watching", "Error"};
// These are the status codes.
public static final int FORGING = 0;
public static final int PAUSED = 1;
public static final int WATCHING = 2;
public static final int ERROR = 3;
private String balance;
private String label;
private String accountN;
private int deadline; 
private int timepassed; 
private int status; 
public AccountGUI(String label, String accnum, String balance, int deadline ) {
    //this.url = url;
    this.deadline = deadline;
    this.label = label;
    this.accountN = accnum;
    this.balance = balance;
    timepassed = 0;
    status = FORGING;
    forge();
    
}
public String getLabel(){
    return label;
}
public String setLabel(String label0){
    this.label = label0;
    return label0;
}
public String getAccountN(){
    return accountN;
}
public String setAccountN(String accnum){
    this.accountN = accnum;
    return accnum;
}
/*public String getUrl() {
    return url.toString();
}
public String setUrl(String url0) {
    this.url = url0;
    return url0;
}*/
public int getDeadline() {
    return deadline;
}
public int setDeadline(int deadl) {
    this.deadline = deadl;
    if (deadline>0){
        status = FORGING;
        stateChanged();
    }
    //stateChanged();
    return deadl;
}
public String getBalance(){
    return balance;
}
public String setBalance(String balance){
    this.balance = balance;
    return balance;
}
public float getProgress() {
    return ((float) timepassed / deadline) * 100;
}

public int getStatus() {
    return status;
}

public void pause() {
    status = PAUSED;
    stateChanged();
}

public void resume() {
    status = FORGING;
    stateChanged();
    forge();
}
private void error() {
    status = ERROR;
    stateChanged();
}
// Start or resume downloading.
public void forge() {
    Thread thread = new Thread(this);
    thread.start();
}
// Get file name portion of URL.
/*private String getFileName(String url) {
    String fileName = url;
    return fileName;
}*/
public void run() {
    try {
        while (status == FORGING) {
            Thread.sleep(1000);
            deadline = deadline - 1;
            timepassed += 1;
            if (deadline<0)
                status = WATCHING;
            stateChanged();
        }
        if (status == FORGING) {
            status = WATCHING;
            stateChanged();
        }
    } catch (Exception e) {
        error();
    } 
}

// Notify observers that this download's status has changed.
private void stateChanged() {
    setChanged();
    notifyObservers();
}
}