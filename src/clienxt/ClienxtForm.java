/*
 * The MIT License
 *
 * Copyright 2014 fmiboy.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copiepas or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package clienxt;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import java.awt.AWTException;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.Rectangle;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.InetSocketAddress;
import java.net.ProxySelector;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Permission;
import java.security.PermissionCollection;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.imageio.ImageIO;
import javax.jnlp.*;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.DefaultRowSorter;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.text.DefaultEditorKit;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


public class ClienxtForm extends JFrame implements Observer {
    public ResourceBundle bundle;
    private Locale locale;
    private String lang = null;
    private AES aes = new AES();
    TrayIcon trayIcon;
    SystemTray tray;
    private String masterKey;
    
    public ClienxtForm(){
        this.globalURL = "http://"+prefs.get(PREF_server, null)+"/";
        
        //testing system tray

        if(SystemTray.isSupported()){
            tray=SystemTray.getSystemTray();
            Image image=Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/nxticon.png"));
            ActionListener exitListener=new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    //System.out.println("Exiting....");
                    System.exit(0);
                }
            };
            PopupMenu popup=new PopupMenu();
            MenuItem defaultItem=new MenuItem(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("MAIN WINDOW"));
            defaultItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    setVisible(true);
                    setExtendedState(JFrame.NORMAL);
                }
            });
            popup.add(defaultItem);
            defaultItem=new MenuItem(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("EXIT PROGRAM"));
            defaultItem.addActionListener(exitListener);
            popup.add(defaultItem);
            
            trayIcon=new TrayIcon(image, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("NXT ECOSYSTEM"), popup);
            trayIcon.setImageAutoSize(true);
        }
        addWindowStateListener(new WindowStateListener() {
            public void windowStateChanged(WindowEvent e) {
                if(e.getNewState()==ICONIFIED){
                    try {
                        tray.add(trayIcon);
                        setVisible(false);
                        //System.out.println("added to SystemTray");
                    } catch (AWTException ex) {
                        System.out.println(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("UNABLE TO ADD TO TRAY"));
                    }
                }
        if(e.getNewState()==7){
                    try{
            tray.add(trayIcon);
            setVisible(false);
            }catch(AWTException ex){
                System.out.println(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("UNABLE TO ADD TO SYSTEM TRAY"));
            }
        }
        if(e.getNewState()==MAXIMIZED_BOTH){
                    tray.remove(trayIcon);
                    setVisible(true);
                }
                if(e.getNewState()==NORMAL){
                    tray.remove(trayIcon);
                    setVisible(true);
                }
            }
        });
        //System tray test ends
        
        lang = prefs.get(PREF_language, "en"); //get saved user setting
        if ("en".equals(lang)){
            locale = new Locale("en", "US");
            setTitle(java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("NXT: ECOSYSTEM"));
        }
        if ("ru".equals(lang)){
            locale = new Locale("ru", "RU");
            setTitle(java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("NXT: ECOSYSTEM"));
        }
        if ("de".equals(lang)){
            locale = new Locale("de", "DE");
            setTitle(java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("NXT: ECOSYSTEM"));
        }
        if ("pt".equals(lang)){
            locale = new Locale("pt", "PT");
            setTitle(java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("NXT: ECOSYSTEM"));
        }
        if ("es".equals(lang)){
            locale = new Locale("es", "ES");
            setTitle(java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("NXT: ECOSYSTEM"));
        }
        Locale.setDefault(locale);
        bundle = java.util.ResourceBundle.getBundle("clienxt/Bundle", locale); // NOI18N
        
        initComponents();
        init();
    }

    private void init(){
        
        setSize(850,700);
        setLocationRelativeTo(null);
        //pack();
        setVisible(true);
        if ("en".equals(lang)){
            englishMenuItem.setSelected(true);
        }
        if ("ru".equals(lang)){
            russianMenuItem.setSelected(true);
        }
        if ("de".equals(lang)){
            germanMenuItem.setSelected(true);
        }
        if ("es".equals(lang)){
            spanishMenuItem.setSelected(true);
        }
        if ("pt".equals(lang)){
            portugueseMenuItem.setSelected(true);
        }
        
        if (isWindows()){
            LIBFolder = OUTPUTUSRFOLDER+"\\lib\\";
            NXTFolder = LIBFolder+"nxt\\";
        }
        if (isLinux()){
            LIBFolder = OUTPUTUSRFOLDER+"/lib/";
            NXTFolder = LIBFolder+"nxt/";
        }
        if (isMac()){
            LIBFolder = OUTPUTUSRFOLDER+"/lib/";
            NXTFolder = LIBFolder+"nxt/";
            InputMap im = (InputMap) UIManager.get("TextField.focusInputMap");
            im.put(KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.META_DOWN_MASK), DefaultEditorKit.copyAction);
            im.put(KeyStroke.getKeyStroke(KeyEvent.VK_V, KeyEvent.META_DOWN_MASK), DefaultEditorKit.pasteAction);
            im.put(KeyStroke.getKeyStroke(KeyEvent.VK_X, KeyEvent.META_DOWN_MASK), DefaultEditorKit.cutAction);
        }        
        //String overviewtext = "O";
        //overviewMenu.setMnemonic(KeyStroke.getKeyStroke(overviewtext).getKeyCode());
        overviewMenu.setMnemonic(KeyEvent.VK_O);
        //Setting the accelerator:
        overviewMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.ALT_MASK));
        consoleMenu.setMnemonic(KeyEvent.VK_C);
        //Setting the accelerator:
        consoleMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.ALT_MASK));
        sendMenu.setMnemonic(KeyEvent.VK_S);
        //Setting the accelerator:
        sendMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.ALT_MASK));
        receiveMenu.setMnemonic(KeyEvent.VK_U);
        //Setting the accelerator:
        receiveMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, ActionEvent.ALT_MASK));
        transactionsMenu.setMnemonic(KeyEvent.VK_T);
        //Setting the accelerator:
        transactionsMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, ActionEvent.ALT_MASK));
        aliasMenu.setMnemonic(KeyEvent.VK_A);
        //Setting the accelerator:
        aliasMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, ActionEvent.ALT_MASK));
        assetExMenu.setMnemonic(KeyEvent.VK_E);
        //Setting the accelerator:
        assetExMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.ALT_MASK));
        votingMenu.setMnemonic(KeyEvent.VK_V);
        //Setting the accelerator:
        votingMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, ActionEvent.ALT_MASK));
        AccountsTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                tableSelectionChanged();
            }
        });
        ConsolePanel.setVisible(true);
        consoleButton.setSelected(true);
        assetexchangeButton.setSelected(false);
        
        consoleOutput.setEditable(false);
        /*MouseListener popupListener = new PopupListener(contextMenu);
        consoleOutput.addMouseListener(popupListener);*/
        
        MouseListener popupListenerTable = new PopupListener(TablePopupMenu);
        transactionsTable.addMouseListener(popupListenerTable);
        messagesTable.addMouseListener(popupListenerTable);
        
        MouseListener popupListenerAsset = new PopupListener(contextMenuAssetTable);
        allAssetsTable.addMouseListener(popupListenerAsset);

        MouseListener popupListenerAssetOders = new PopupListener(contextMenuAssetOrders);
        myOrdersTable.addMouseListener(popupListenerAssetOders);

        MouseListener popupListenerPoll = new PopupListener(castVoteContextMenu);
        allPollsTable.addMouseListener(popupListenerPoll);
        
        TransactionsPanel.setVisible(false);
        SendPanel.setVisible(false);
        ReceivePanel.setVisible(false);
        MessagesPanel.setVisible(false);
        BlocksPanel.setVisible(false);
        AliasesPanel.setVisible(false);
        OverviewPanel.setVisible(false);
        AssetExPanel.setVisible(false);
        VotingPanel.setVisible(false);
        
        //redirect all console outputs to textarea
        MessageConsole mc = new MessageConsole(consoleOutput);
        mc.redirectOut();
        mc.redirectErr(Color.RED, null);
        
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/nxticon.png")));
        //Accouts table settings
        tableModelA = new AccountTableModel();
        AccountsTable.setModel(tableModelA);
        //Transactions table settings
        transactionsTable.setAutoCreateRowSorter(true);
        transactionsTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        sorter = new TableRowSorter<TableModel>(transactionsTable.getModel());
        transactionsTable.setRowSorter(sorter);
        
        //Asset table settings
        allAssetsTable.setAutoCreateRowSorter(true);
        allAssetsTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        sorter = new TableRowSorter<TableModel>(allAssetsTable.getModel());
        allAssetsTable.setRowSorter(sorter);
        
        final Color fgSel = allAssetsTable.getSelectionForeground();
        final Color bgSel = allAssetsTable.getSelectionBackground();
        allAssetsTable.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {
            private boolean selected;
            @Override
            public void paint(Graphics g) {
                super.paint(g);

                String filter = searchAssetField.getText().trim();
                if (filter.length() == 0) {
                    return;
                }
                String text = getText();
                int index = text.indexOf(filter);
                if (index == -1) {
                    return;
                }
                String preMatch = text.substring(0, index);
                String match = text.substring(preMatch.length(), preMatch.length() + filter.length());
                int pmw = g.getFontMetrics().stringWidth(preMatch);
                int w = g.getFontMetrics().stringWidth(match);

                g.setColor(selected ? fgSel : Color.yellow);
                g.fillRect(pmw, 0, w, getHeight());
                g.setColor(selected ? bgSel : getForeground());
                Rectangle r = g.getFontMetrics().getStringBounds(match, g).getBounds();
                g.drawString(match, pmw + 1, -r.y);
            }

            @Override
            public Component getTableCellRendererComponent(JTable arg0, Object arg1, boolean selected, boolean arg3, int arg4, int arg5) {
                this.selected = selected;
                return super.getTableCellRendererComponent(arg0, arg1, selected, arg3, arg4, arg5);
            }

        });
        
        //Default settings values
        defaultAccount.removeAllItems();
        /*
        Vector acclist = readAccounts("1");
        for (int i=0; i< acclist.size(); i++) {
           defaultAccount.addItem(acclist.get(i).toString());
        }*/
        defaultFee.setText(String.valueOf(prefs.getInt(PREF_fee, 1))); // "1"
        defaultDeadline.setText(String.valueOf(prefs.getInt(PREF_deadline, 1440))); // "a string"
        //defaultServer.setText(String.valueOf(prefs.get(PREF_server, null))); // "1"
        defaultAccount.setSelectedItem(prefs.get(PREF_account, ""));
        if (prefs.get(PREF_network, "0").equals("1")){
            networkCheckBox.setSelected(true);
            timeToForgeLabel.setText("TESTNET");
        }
        else{
            networkCheckBox.setSelected(false);
            timeToForgeLabel.setText("MAINNET");
            getAssetsButton.setEnabled(false);
            issueAssetButton.setEnabled(false);
            transferAssetButton.setEnabled(false);
            ordersAssetButton.setEnabled(false);
            getPollsButton.setEnabled(false);
            createPollsButton.setEnabled(false);
        }
               
        //Connections (proxy)
        String buttonIndex = prefs.get(PREF_proxyButton, "0");
        if (buttonIndex.equals("0"))
            proxyNoRbox.setSelected(true);
        if (buttonIndex.equals("1"))
            proxyAutoRBox.setSelected(true);
        if (buttonIndex.equals("2")){
            proxyManRBox.setSelected(true);
            proxyHostField.setText(prefs.get(PREF_proxyManualHost, "0.0.0.0"));
            proxyPortField.setText(prefs.get(PREF_proxyManualPort, "8080"));
        }
        
        //disable some parts until Nxt starts
        partialComponents(false);
        
        //starting server
        String serverURL = prefs.get(PREF_server, "localhost:7678");
        //defaultServer.setText(serverURL);
        //if (serverURL.equals("holms.cloudapp.net:6874")){
        //    consoleOutput.setText(".\n");
        //}else {
        
        startServer();
        //}
        
        //init Db
        Db.init();
        if (prefs.get(PREF_masterKey, null) == null)
            deleteDbRecords();
        
        //Updating Peers/Blocks
        /*int delay = 60000; //milliseconds
        ActionListener taskPerformer = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent evt) {
            try {
                getBlocks();
                getPeers();
            } catch (IOException ex) {
                consoleOutput.append("\n["+nowtime()+"] "+ex+java.util.ResourceBundle.getBundle("clienxt/Bundle").getString(" SOMETHING IS WRONG WITH EITHER YOUR INTERNET CONNECTION OR SERVER CONNECTION, BLOCKS AND PEERS CANNOT BE UPDATED!)"));
                //Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        };
        new Timer(delay, taskPerformer).start();*/
        
    }
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        UnlockDialog = new javax.swing.JDialog();
        jLabel1 = new javax.swing.JLabel();
        accountPassField = new javax.swing.JPasswordField();
        encryptMyAccountCheckBox = new javax.swing.JCheckBox();
        labelofAccount = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        preferencesFrame = new javax.swing.JFrame();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        defaultFee = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        defaultDeadline = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        defaultAccount = new javax.swing.JComboBox();
        jLabel28 = new javax.swing.JLabel();
        networkCheckBox = new javax.swing.JCheckBox();
        jPanel8 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        masterPasswordField = new javax.swing.JPasswordField();
        jPanel10 = new javax.swing.JPanel();
        changeMasterPasswordField = new javax.swing.JPasswordField();
        newMasterPasswordField = new javax.swing.JPasswordField();
        confirmMasterPasswordField = new javax.swing.JPasswordField();
        jLabel66 = new javax.swing.JLabel();
        jLabel67 = new javax.swing.JLabel();
        jLabel68 = new javax.swing.JLabel();
        jLabel64 = new javax.swing.JLabel();
        jPanel12 = new javax.swing.JPanel();
        proxyHostField = new javax.swing.JTextField();
        jLabel65 = new javax.swing.JLabel();
        jLabel69 = new javax.swing.JLabel();
        proxyNoRbox = new javax.swing.JRadioButton();
        proxyAutoRBox = new javax.swing.JRadioButton();
        proxyManRBox = new javax.swing.JRadioButton();
        proxyPortField = new javax.swing.JTextField();
        proxyUserField = new javax.swing.JTextField();
        proxyPasswordField = new javax.swing.JPasswordField();
        jLabel50 = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();
        updatePrefsButton = new javax.swing.JButton();
        aboutClieNXTdialog = new javax.swing.JDialog();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel63 = new javax.swing.JLabel();
        jLabel71 = new javax.swing.JLabel();
        ToolbarButtonGroup = new javax.swing.ButtonGroup();
        SentDialog = new javax.swing.JDialog();
        jScrollPane1 = new javax.swing.JScrollPane();
        accountCheckTable = new javax.swing.JTable();
        OKbutton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        menuLanguageButtonGroup = new javax.swing.ButtonGroup();
        contextMenu = new javax.swing.JPopupMenu();
        restartCMenu = new javax.swing.JMenuItem();
        stopCMenu = new javax.swing.JMenuItem();
        assetOrdersDialog = new javax.swing.JDialog();
        jScrollPane4 = new javax.swing.JScrollPane();
        askOrdersAssetTable = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        bidOrdersAssetTable = new javax.swing.JTable();
        jLabel34 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        issueAssetDialog = new javax.swing.JDialog();
        jPanel11 = new javax.swing.JPanel();
        assetNameField = new javax.swing.JTextField();
        jLabel37 = new javax.swing.JLabel();
        jScrollPane11 = new javax.swing.JScrollPane();
        assetDescriptionField = new javax.swing.JTextArea();
        jLabel38 = new javax.swing.JLabel();
        assetQtyField = new javax.swing.JTextField();
        jLabel39 = new javax.swing.JLabel();
        assetPassphraseField = new javax.swing.JPasswordField();
        jLabel40 = new javax.swing.JLabel();
        issueAssetButtonIn = new javax.swing.JButton();
        transferAssetDialog = new javax.swing.JDialog();
        transferAssetPanel = new javax.swing.JPanel();
        recipientAssetField = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        nameAssetComboBox = new javax.swing.JComboBox();
        jLabel20 = new javax.swing.JLabel();
        quantityAssetField = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        passphraseAssetField = new javax.swing.JPasswordField();
        jLabel31 = new javax.swing.JLabel();
        transferAssetButtonIn = new javax.swing.JButton();
        assetAccountsComboBox = new javax.swing.JComboBox();
        jLabel36 = new javax.swing.JLabel();
        contextMenuAssetTable = new javax.swing.JPopupMenu();
        viewAskBidCMenu = new javax.swing.JMenuItem();
        placeAskBidCMenu = new javax.swing.JMenuItem();
        viewTradesCMenu = new javax.swing.JMenuItem();
        updateTableCMenu = new javax.swing.JMenuItem();
        myOrdersDialog = new javax.swing.JDialog();
        assetAccountListComboBox = new javax.swing.JComboBox();
        jScrollPane6 = new javax.swing.JScrollPane();
        myOrdersTable = new javax.swing.JTable();
        placeOrderDialog = new javax.swing.JDialog();
        orderTypeComboBox = new javax.swing.JComboBox();
        jLabel35 = new javax.swing.JLabel();
        assetQuantityField = new javax.swing.JTextField();
        jLabel42 = new javax.swing.JLabel();
        assetPriceField = new javax.swing.JTextField();
        jLabel43 = new javax.swing.JLabel();
        assetPassField = new javax.swing.JPasswordField();
        jLabel44 = new javax.swing.JLabel();
        assetOrderSubmitButton = new javax.swing.JButton();
        assetIdOrderLabel = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        jLabel48 = new javax.swing.JLabel();
        contextMenuAssetOrders = new javax.swing.JPopupMenu();
        cancelOrderCMenu = new javax.swing.JMenuItem();
        myOrderCancelDialog = new javax.swing.JDialog();
        idOrderLabel = new javax.swing.JLabel();
        typeOrderLabel = new javax.swing.JLabel();
        cancelPassField = new javax.swing.JPasswordField();
        jLabel46 = new javax.swing.JLabel();
        confirmCancelAssetButton = new javax.swing.JButton();
        assetTradesDialog = new javax.swing.JDialog();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane12 = new javax.swing.JScrollPane();
        assetTradesTable = new javax.swing.JTable();
        generateTokenDialog = new javax.swing.JDialog();
        websiteField = new javax.swing.JTextField();
        jLabel52 = new javax.swing.JLabel();
        tokenPassField = new javax.swing.JPasswordField();
        jLabel53 = new javax.swing.JLabel();
        jScrollPane15 = new javax.swing.JScrollPane();
        tokenTextPane = new javax.swing.JTextPane();
        tokenAccountIdLabel = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        createPollDialog = new javax.swing.JDialog();
        creatPollButton = new javax.swing.JButton();
        voteNameField = new javax.swing.JTextField();
        jScrollPane17 = new javax.swing.JScrollPane();
        voteDescriptionField = new javax.swing.JTextArea();
        voteMinField = new javax.swing.JTextField();
        voteMaxField = new javax.swing.JTextField();
        jLabel54 = new javax.swing.JLabel();
        jLabel55 = new javax.swing.JLabel();
        jLabel56 = new javax.swing.JLabel();
        jLabel57 = new javax.swing.JLabel();
        jLabel58 = new javax.swing.JLabel();
        jScrollPane18 = new javax.swing.JScrollPane();
        voteOptionsField = new javax.swing.JTextArea();
        jLabel59 = new javax.swing.JLabel();
        jLabel60 = new javax.swing.JLabel();
        voteOptionsAreBinaryComboBox = new javax.swing.JComboBox();
        voteSecretPassField = new javax.swing.JPasswordField();
        jLabel61 = new javax.swing.JLabel();
        castVoteDialog = new javax.swing.JDialog();
        castVoteIdLabel = new javax.swing.JLabel();
        castVotePassField = new javax.swing.JPasswordField();
        castVoteSubmitButton = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        castVoteOptionComboBox = new javax.swing.JComboBox();
        castVoteContextMenu = new javax.swing.JPopupMenu();
        castVoteMenuItem = new javax.swing.JMenuItem();
        forgeDialog = new javax.swing.JDialog();
        forgePassField = new javax.swing.JPasswordField();
        connectionOptionsCheckBox = new javax.swing.ButtonGroup();
        masterPasswordDialog = new javax.swing.JDialog();
        masterPassowordField = new javax.swing.JPasswordField();
        watchAccountDialog = new javax.swing.JDialog();
        watchAccountNumberField = new javax.swing.JTextField();
        jLabel70 = new javax.swing.JLabel();
        jLabel62 = new javax.swing.JLabel();
        labelWatchAccountField = new javax.swing.JTextField();
        TablePopupMenu = new javax.swing.JPopupMenu();
        copySenderAddressCMenu = new javax.swing.JMenuItem();
        copyRecipientAddressCMenu = new javax.swing.JMenuItem();
        sendNxtToSenderAddressCMenu = new javax.swing.JMenuItem();
        sendNxtToRecipientAddressCMenu = new javax.swing.JMenuItem();
        sendMsgToSenderAddressCMenu = new javax.swing.JMenuItem();
        sendMsgToRecipientAddressCMenu = new javax.swing.JMenuItem();
        MainToolbar = new javax.swing.JToolBar();
        overviewButton = new javax.swing.JToggleButton();
        consoleButton = new javax.swing.JToggleButton();
        unlockButton = new javax.swing.JToggleButton();
        sendButton = new javax.swing.JToggleButton();
        messagesButton = new javax.swing.JToggleButton();
        transactionsButton = new javax.swing.JToggleButton();
        aliasButton = new javax.swing.JToggleButton();
        assetexchangeButton = new javax.swing.JToggleButton();
        votingButton = new javax.swing.JToggleButton();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        MainPanel = new javax.swing.JPanel();
        ConsolePanel = new javax.swing.JPanel();
        consoleScrollPane = new javax.swing.JScrollPane();
        consoleOutput = new javax.swing.JTextArea();
        AssetExPanel = new javax.swing.JPanel();
        allAssetsScrollPane = new javax.swing.JScrollPane();
        allAssetsTable = new javax.swing.JTable();
        ordersAssetButton = new javax.swing.JButton();
        issueAssetButton = new javax.swing.JButton();
        transferAssetButton = new javax.swing.JButton();
        searchAssetField = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        getAssetsButton = new javax.swing.JButton();
        SendPanel = new javax.swing.JPanel();
        receiverAddressField = new javax.swing.JTextField();
        receiverLabel = new javax.swing.JLabel();
        amountNxtField = new javax.swing.JTextField();
        amountLabel = new javax.swing.JLabel();
        passphraseField = new javax.swing.JPasswordField();
        sendNxtButton = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        senderAddressComboBox = new javax.swing.JComboBox();
        senderLabel = new javax.swing.JLabel();
        attachMessageCheckBox = new javax.swing.JCheckBox();
        jScrollPane3 = new javax.swing.JScrollPane();
        inputMessageField = new javax.swing.JTextArea();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        TransactionsPanel = new javax.swing.JPanel();
        TransactionsScrollPane = new javax.swing.JScrollPane();
        transactionsTable = new javax.swing.JTable();
        TXaccountListCombobox = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        ReceivePanel = new javax.swing.JPanel();
        accountsSeparator = new javax.swing.JSeparator();
        AccountsTableScrollPane = new javax.swing.JScrollPane();
        AccountsTable = new javax.swing.JTable();
        AddAccountButton = new javax.swing.JButton();
        RemoveAccountButton = new javax.swing.JButton();
        updateBalancesButton = new javax.swing.JButton();
        generateTokenButton = new javax.swing.JButton();
        qrCodeButton = new javax.swing.JButton();
        forgeButton = new javax.swing.JButton();
        filler2 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        MessagesPanel = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        messagesTable = new javax.swing.JTable();
        messageSenderComboBox = new javax.swing.JComboBox();
        messageRecipientField = new javax.swing.JTextField();
        jScrollPane8 = new javax.swing.JScrollPane();
        messageContentField = new javax.swing.JTextArea();
        messageSecretPhraseField = new javax.swing.JPasswordField();
        messageSendButton = new javax.swing.JButton();
        jLabel72 = new javax.swing.JLabel();
        jLabel73 = new javax.swing.JLabel();
        jLabel74 = new javax.swing.JLabel();
        jLabel75 = new javax.swing.JLabel();
        jButton11 = new javax.swing.JButton();
        BlocksPanel = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        BlocksTable = new javax.swing.JTable();
        AliasesPanel = new javax.swing.JPanel();
        jScrollPane10 = new javax.swing.JScrollPane();
        aliasTable = new javax.swing.JTable();
        aliasField = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        uriField = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        assignAliasButton = new javax.swing.JButton();
        ALaccountListComboBox = new javax.swing.JComboBox();
        jLabel8 = new javax.swing.JLabel();
        aliasAccountPassphrase = new javax.swing.JPasswordField();
        jLabel9 = new javax.swing.JLabel();
        outputL = new javax.swing.JLabel();
        aliasSearchField = new javax.swing.JTextField();
        jLabel33 = new javax.swing.JLabel();
        OverviewPanel = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        totalBalanceLabelNXT = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        totalBalanceLabelBTC = new javax.swing.JLabel();
        totalBalanceLabelUSD = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jScrollPane13 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel49 = new javax.swing.JLabel();
        jScrollPane14 = new javax.swing.JScrollPane();
        overviewAssetTable = new javax.swing.JTable();
        jPanel5 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        NxtBtcLabel = new javax.swing.JLabel();
        NxtUsdLabel = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        VotingPanel = new javax.swing.JPanel();
        jScrollPane16 = new javax.swing.JScrollPane();
        allPollsTable = new javax.swing.JTable();
        getPollsButton = new javax.swing.JButton();
        createPollsButton = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        statusBarPanel = new javax.swing.JPanel();
        statusbarLabel = new javax.swing.JLabel();
        jProgressBar1 = new javax.swing.JProgressBar();
        timeToForgeLabel = new javax.swing.JLabel();
        latestBlockLabel = new javax.swing.JLabel();
        peersLabel = new javax.swing.JLabel();
        MenuBar = new javax.swing.JMenuBar();
        FileMenu = new javax.swing.JMenu();
        overviewMenu = new javax.swing.JMenuItem();
        consoleMenu = new javax.swing.JMenuItem();
        sendMenu = new javax.swing.JMenuItem();
        receiveMenu = new javax.swing.JMenuItem();
        transactionsMenu = new javax.swing.JMenuItem();
        aliasMenu = new javax.swing.JMenuItem();
        assetExMenu = new javax.swing.JMenuItem();
        votingMenu = new javax.swing.JMenuItem();
        exitMenuItem = new javax.swing.JMenuItem();
        settingsMenu = new javax.swing.JMenu();
        languagesMenuItem = new javax.swing.JMenu();
        englishMenuItem = new javax.swing.JCheckBoxMenuItem();
        russianMenuItem = new javax.swing.JCheckBoxMenuItem();
        germanMenuItem = new javax.swing.JCheckBoxMenuItem();
        portugueseMenuItem = new javax.swing.JCheckBoxMenuItem();
        spanishMenuItem = new javax.swing.JCheckBoxMenuItem();
        othersMenuItemItem = new javax.swing.JMenuItem();
        preferencesMenuItem = new javax.swing.JMenuItem();
        aboutMenu = new javax.swing.JMenu();
        aboutNxtMenuItem = new javax.swing.JMenuItem();
        checkNxtUpdateMenu = new javax.swing.JMenuItem();
        aboutClienxtMenuItem = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();

        UnlockDialog.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        UnlockDialog.setIconImages(null);
        UnlockDialog.setModalityType(java.awt.Dialog.ModalityType.APPLICATION_MODAL);

        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("src/clienxt/Bundle"); // NOI18N
        jLabel1.setText(bundle.getString("SECRETPHRASE")); // NOI18N

        accountPassField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                accountPassFieldActionPerformed(evt);
            }
        });

        encryptMyAccountCheckBox.setText(bundle.getString("ENCRYPT SECRETPHRASE IN DATABASE")); // NOI18N

        jLabel4.setText(bundle.getString("LABEL")); // NOI18N

        javax.swing.GroupLayout UnlockDialogLayout = new javax.swing.GroupLayout(UnlockDialog.getContentPane());
        UnlockDialog.getContentPane().setLayout(UnlockDialogLayout);
        UnlockDialogLayout.setHorizontalGroup(
            UnlockDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(UnlockDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(UnlockDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(UnlockDialogLayout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(jLabel1))
                    .addComponent(jLabel4))
                .addGap(18, 18, 18)
                .addGroup(UnlockDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(labelofAccount)
                    .addComponent(encryptMyAccountCheckBox)
                    .addComponent(accountPassField, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(30, Short.MAX_VALUE))
        );
        UnlockDialogLayout.setVerticalGroup(
            UnlockDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(UnlockDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(UnlockDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(labelofAccount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(encryptMyAccountCheckBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(UnlockDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(accountPassField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(20, Short.MAX_VALUE))
        );

        preferencesFrame.setIconImages(null);
        preferencesFrame.setModalExclusionType(java.awt.Dialog.ModalExclusionType.APPLICATION_EXCLUDE);

        jLabel26.setText(bundle.getString("FEE")); // NOI18N

        defaultDeadline.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                defaultDeadlineActionPerformed(evt);
            }
        });

        jLabel27.setText(bundle.getString("DEADLINE")); // NOI18N

        jLabel28.setText(bundle.getString("ACCOUNT")); // NOI18N

        networkCheckBox.setText(bundle.getString("ISTESTNET (SELECT IF YOU WANT TESTNET BY DEFAULT, UNSELECT IF YOU WANT MAINNET BY DEFAULT)")); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(defaultDeadline, javax.swing.GroupLayout.DEFAULT_SIZE, 511, Short.MAX_VALUE)
                            .addComponent(defaultFee))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel26)
                            .addComponent(jLabel27))
                        .addGap(70, 70, 70))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(defaultAccount, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(20, 20, 20)
                        .addComponent(jLabel28)
                        .addContainerGap())
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(networkCheckBox)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(defaultFee, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel26))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(defaultDeadline, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel27))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel28)
                    .addComponent(defaultAccount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(networkCheckBox)
                .addContainerGap(112, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Default values", jPanel2);

        jPanel9.setBorder(javax.swing.BorderFactory.createTitledBorder("Set Master Password"));

        masterPasswordField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                masterPasswordFieldActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(masterPasswordField)
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(masterPasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel10.setBorder(javax.swing.BorderFactory.createTitledBorder("Change Master Password"));

        changeMasterPasswordField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                changeMasterPasswordFieldActionPerformed(evt);
            }
        });

        jLabel66.setText(bundle.getString("CURRENT")); // NOI18N

        jLabel67.setText(bundle.getString("NEW")); // NOI18N

        jLabel68.setText(bundle.getString("CONFIRM")); // NOI18N

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel67)
                    .addComponent(jLabel68)
                    .addComponent(jLabel66))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(changeMasterPasswordField, javax.swing.GroupLayout.DEFAULT_SIZE, 567, Short.MAX_VALUE)
                    .addComponent(newMasterPasswordField)
                    .addComponent(confirmMasterPasswordField, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(changeMasterPasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel66))
                .addGap(18, 18, 18)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(newMasterPasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel67))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(confirmMasterPasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel68))
                .addContainerGap())
        );

        jLabel64.setText(bundle.getString("DON'T FORGET MASTER PASSWORD!")); // NOI18N

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel9, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(jLabel64)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel64)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Master password", jPanel8);

        jLabel65.setText(bundle.getString("HOST")); // NOI18N

        jLabel69.setText(bundle.getString("PORT")); // NOI18N

        connectionOptionsCheckBox.add(proxyNoRbox);
        proxyNoRbox.setSelected(true);
        proxyNoRbox.setText(bundle.getString("NO PROXY")); // NOI18N

        connectionOptionsCheckBox.add(proxyAutoRBox);
        proxyAutoRBox.setText(bundle.getString("AUTO DETECT PROXY")); // NOI18N

        connectionOptionsCheckBox.add(proxyManRBox);
        proxyManRBox.setText(bundle.getString("MANUALLY SET PROXY")); // NOI18N
        proxyManRBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                proxyManRBoxItemStateChanged(evt);
            }
        });

        proxyPortField.setText("0");

        jLabel50.setText(bundle.getString("USERNAME")); // NOI18N

        jLabel51.setText(bundle.getString("PASSWORD")); // NOI18N

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(proxyNoRbox))
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(proxyAutoRBox))
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(proxyManRBox))
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel65)
                            .addComponent(jLabel50)
                            .addComponent(jLabel51))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(proxyPasswordField)
                            .addComponent(proxyUserField)
                            .addComponent(proxyHostField, javax.swing.GroupLayout.DEFAULT_SIZE, 205, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel69)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(proxyPortField, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(282, Short.MAX_VALUE))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(proxyNoRbox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(proxyAutoRBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(proxyManRBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(proxyHostField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel65)
                    .addComponent(jLabel69)
                    .addComponent(proxyPortField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(proxyUserField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel50))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(proxyPasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel51))
                .addContainerGap(95, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Connections", jPanel12);

        updatePrefsButton.setText(bundle.getString("UPDATE")); // NOI18N
        updatePrefsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updatePrefsButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout preferencesFrameLayout = new javax.swing.GroupLayout(preferencesFrame.getContentPane());
        preferencesFrame.getContentPane().setLayout(preferencesFrameLayout);
        preferencesFrameLayout.setHorizontalGroup(
            preferencesFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(preferencesFrameLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(preferencesFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(preferencesFrameLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(updatePrefsButton))
                    .addComponent(jTabbedPane1))
                .addContainerGap())
        );
        preferencesFrameLayout.setVerticalGroup(
            preferencesFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(preferencesFrameLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 288, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(updatePrefsButton)
                .addContainerGap())
        );

        aboutClieNXTdialog.setIconImages(null);

        jLabel17.setText(bundle.getString("CLIENXT GUI FOR NXT ECOSYSTEM")); // NOI18N

        jLabel18.setText(bundle.getString("DEVELOPER:")); // NOI18N

        jLabel29.setText(bundle.getString("TRANSLATORS:")); // NOI18N

        jLabel63.setText("<html><b>Spanish</b> - VanBreuk (8118526282532613576 ),<br>\n<b>Portuguese</b> - garcias (),<br>\n<b>German</b> - teletobi (11810049982317883239),<br>\n<b>Russian</b>, <b>English</b>"); // NOI18N

        jLabel71.setText("<html><b>fmiboy</b>"); // NOI18N

        javax.swing.GroupLayout aboutClieNXTdialogLayout = new javax.swing.GroupLayout(aboutClieNXTdialog.getContentPane());
        aboutClieNXTdialog.getContentPane().setLayout(aboutClieNXTdialogLayout);
        aboutClieNXTdialogLayout.setHorizontalGroup(
            aboutClieNXTdialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(aboutClieNXTdialogLayout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(aboutClieNXTdialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(aboutClieNXTdialogLayout.createSequentialGroup()
                        .addComponent(jLabel29)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel63, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel17)
                    .addGroup(aboutClieNXTdialogLayout.createSequentialGroup()
                        .addComponent(jLabel18)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel71, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        aboutClieNXTdialogLayout.setVerticalGroup(
            aboutClieNXTdialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(aboutClieNXTdialogLayout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(jLabel17)
                .addGap(18, 18, 18)
                .addGroup(aboutClieNXTdialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(jLabel71, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(aboutClieNXTdialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel29)
                    .addComponent(jLabel63, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(72, Short.MAX_VALUE))
        );

        SentDialog.setIconImages(null);

        accountCheckTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null}
            },
            new String [] {
                "Account", "Balance", "PubKey", "Note"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                true, false, true, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        accountCheckTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_LAST_COLUMN);
        jScrollPane1.setViewportView(accountCheckTable);

        OKbutton.setText(bundle.getString("OK")); // NOI18N
        OKbutton.setPreferredSize(new java.awt.Dimension(65, 23));
        OKbutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OKbuttonActionPerformed(evt);
            }
        });

        cancelButton.setText(bundle.getString("CANCEL")); // NOI18N
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout SentDialogLayout = new javax.swing.GroupLayout(SentDialog.getContentPane());
        SentDialog.getContentPane().setLayout(SentDialogLayout);
        SentDialogLayout.setHorizontalGroup(
            SentDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SentDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(SentDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(SentDialogLayout.createSequentialGroup()
                        .addComponent(OKbutton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 213, Short.MAX_VALUE)
                        .addComponent(cancelButton)))
                .addContainerGap())
        );
        SentDialogLayout.setVerticalGroup(
            SentDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, SentDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(SentDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(OKbutton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cancelButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        restartCMenu.setText(bundle.getString("RE-START NXT SERVER")); // NOI18N
        restartCMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                restartCMenuActionPerformed(evt);
            }
        });
        contextMenu.add(restartCMenu);

        stopCMenu.setText(bundle.getString("STOP NXT SERVER")); // NOI18N
        stopCMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stopCMenuActionPerformed(evt);
            }
        });
        contextMenu.add(stopCMenu);

        assetOrdersDialog.setIconImages(null);
        assetOrdersDialog.setResizable(false);

        askOrdersAssetTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null}
            },
            new String [] {
                "Account", "Asset id", "Quantity", "Price"
            }
        ));
        jScrollPane4.setViewportView(askOrdersAssetTable);

        bidOrdersAssetTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null}
            },
            new String [] {
                "Account", "Asset id", "Quantity", "Price"
            }
        ));
        jScrollPane2.setViewportView(bidOrdersAssetTable);

        jLabel34.setText("Ask orders");

        jLabel41.setText("Buy orders");

        javax.swing.GroupLayout assetOrdersDialogLayout = new javax.swing.GroupLayout(assetOrdersDialog.getContentPane());
        assetOrdersDialog.getContentPane().setLayout(assetOrdersDialogLayout);
        assetOrdersDialogLayout.setHorizontalGroup(
            assetOrdersDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(assetOrdersDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(assetOrdersDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 693, Short.MAX_VALUE)
                    .addComponent(jScrollPane2)
                    .addComponent(jLabel34)
                    .addComponent(jLabel41))
                .addContainerGap())
        );
        assetOrdersDialogLayout.setVerticalGroup(
            assetOrdersDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, assetOrdersDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel34)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel41)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        issueAssetDialog.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        issueAssetDialog.setIconImages(null);
        issueAssetDialog.setResizable(false);

        jPanel11.setBorder(javax.swing.BorderFactory.createTitledBorder("Issue Asset"));

        jLabel37.setText("Name");

        assetDescriptionField.setColumns(20);
        assetDescriptionField.setRows(5);
        jScrollPane11.setViewportView(assetDescriptionField);

        jLabel38.setText("Description");
        jLabel38.setToolTipText("");

        jLabel39.setText("Quantity");

        jLabel40.setText("secretphrase");

        issueAssetButtonIn.setText("Submit");
        issueAssetButtonIn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                issueAssetButtonInActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(assetPassphraseField)
                    .addComponent(assetQtyField)
                    .addComponent(jScrollPane11, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 292, Short.MAX_VALUE)
                    .addComponent(assetNameField, javax.swing.GroupLayout.Alignment.LEADING))
                .addGap(18, 18, 18)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel40)
                    .addComponent(jLabel37)
                    .addComponent(jLabel38)
                    .addComponent(jLabel39))
                .addContainerGap(85, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(issueAssetButtonIn)
                .addContainerGap())
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(assetNameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel37))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane11, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel38))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(assetQtyField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel39))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(assetPassphraseField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel40))
                .addGap(18, 18, 18)
                .addComponent(issueAssetButtonIn)
                .addContainerGap())
        );

        javax.swing.GroupLayout issueAssetDialogLayout = new javax.swing.GroupLayout(issueAssetDialog.getContentPane());
        issueAssetDialog.getContentPane().setLayout(issueAssetDialogLayout);
        issueAssetDialogLayout.setHorizontalGroup(
            issueAssetDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(issueAssetDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        issueAssetDialogLayout.setVerticalGroup(
            issueAssetDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(issueAssetDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        transferAssetDialog.setIconImages(null);

        transferAssetPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Transfer Asset"));

        jLabel12.setText("Address of recipient");

        nameAssetComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Asset id", " " }));
        nameAssetComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nameAssetComboBoxActionPerformed(evt);
            }
        });

        jLabel20.setText(bundle.getString("ASSET ID")); // NOI18N

        jLabel30.setText(bundle.getString("QUANTITY")); // NOI18N

        jLabel31.setText("secretPhrase");

        transferAssetButtonIn.setText("Submit");
        transferAssetButtonIn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                transferAssetButtonInActionPerformed(evt);
            }
        });

        assetAccountsComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "accounts" }));
        assetAccountsComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                assetAccountsComboBoxActionPerformed(evt);
            }
        });

        jLabel36.setText("null");

        javax.swing.GroupLayout transferAssetPanelLayout = new javax.swing.GroupLayout(transferAssetPanel);
        transferAssetPanel.setLayout(transferAssetPanelLayout);
        transferAssetPanelLayout.setHorizontalGroup(
            transferAssetPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(transferAssetPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(transferAssetPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(assetAccountsComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(recipientAssetField, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(passphraseAssetField, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(quantityAssetField, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(nameAssetComboBox, javax.swing.GroupLayout.Alignment.LEADING, 0, 291, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(transferAssetPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel31)
                    .addComponent(jLabel36)
                    .addComponent(jLabel30)
                    .addComponent(jLabel12)
                    .addComponent(jLabel20))
                .addContainerGap(74, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, transferAssetPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(transferAssetButtonIn)
                .addContainerGap())
        );
        transferAssetPanelLayout.setVerticalGroup(
            transferAssetPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(transferAssetPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(transferAssetPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(assetAccountsComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel36))
                .addGap(14, 14, 14)
                .addGroup(transferAssetPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(recipientAssetField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(transferAssetPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nameAssetComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel20))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(transferAssetPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(quantityAssetField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel30))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(transferAssetPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(passphraseAssetField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel31))
                .addGap(18, 18, Short.MAX_VALUE)
                .addComponent(transferAssetButtonIn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout transferAssetDialogLayout = new javax.swing.GroupLayout(transferAssetDialog.getContentPane());
        transferAssetDialog.getContentPane().setLayout(transferAssetDialogLayout);
        transferAssetDialogLayout.setHorizontalGroup(
            transferAssetDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(transferAssetDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(transferAssetPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        transferAssetDialogLayout.setVerticalGroup(
            transferAssetDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(transferAssetDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(transferAssetPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        viewAskBidCMenu.setText("View ask/bid orders");
        viewAskBidCMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewAskBidCMenuActionPerformed(evt);
            }
        });
        contextMenuAssetTable.add(viewAskBidCMenu);

        placeAskBidCMenu.setText("Place ask/bid order");
        placeAskBidCMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                placeAskBidCMenuActionPerformed(evt);
            }
        });
        contextMenuAssetTable.add(placeAskBidCMenu);

        viewTradesCMenu.setText("View trades");
        viewTradesCMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewTradesCMenuActionPerformed(evt);
            }
        });
        contextMenuAssetTable.add(viewTradesCMenu);

        updateTableCMenu.setText("Update table");
        updateTableCMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateTableCMenuActionPerformed(evt);
            }
        });
        contextMenuAssetTable.add(updateTableCMenu);

        myOrdersDialog.setIconImages(null);

        assetAccountListComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { " " }));
        assetAccountListComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                assetAccountListComboBoxActionPerformed(evt);
            }
        });

        myOrdersTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null}
            },
            new String [] {
                "Type", "Order id", "Asset name", "Asset description", "Asset quantity", "Offered price"
            }
        ));
        jScrollPane6.setViewportView(myOrdersTable);

        javax.swing.GroupLayout myOrdersDialogLayout = new javax.swing.GroupLayout(myOrdersDialog.getContentPane());
        myOrdersDialog.getContentPane().setLayout(myOrdersDialogLayout);
        myOrdersDialogLayout.setHorizontalGroup(
            myOrdersDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(myOrdersDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(myOrdersDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(myOrdersDialogLayout.createSequentialGroup()
                        .addComponent(assetAccountListComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 601, Short.MAX_VALUE))
                .addContainerGap())
        );
        myOrdersDialogLayout.setVerticalGroup(
            myOrdersDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(myOrdersDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(assetAccountListComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane6)
                .addContainerGap())
        );

        placeOrderDialog.setIconImages(null);
        placeOrderDialog.setResizable(false);

        orderTypeComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Ask", "Bid" }));

        jLabel35.setText("Type");

        jLabel42.setText("Quantity");

        jLabel43.setText(bundle.getString("PRICE")); // NOI18N

        jLabel44.setText("secretPhrase");

        assetOrderSubmitButton.setText("Submit");
        assetOrderSubmitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                assetOrderSubmitButtonActionPerformed(evt);
            }
        });

        jLabel45.setText("Asset id:");

        jLabel47.setText("Price in NXT cent, enter 145 for 1.45 NXT");

        jLabel48.setText("null");

        javax.swing.GroupLayout placeOrderDialogLayout = new javax.swing.GroupLayout(placeOrderDialog.getContentPane());
        placeOrderDialog.getContentPane().setLayout(placeOrderDialogLayout);
        placeOrderDialogLayout.setHorizontalGroup(
            placeOrderDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(placeOrderDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(placeOrderDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(placeOrderDialogLayout.createSequentialGroup()
                        .addComponent(jLabel45)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(assetIdOrderLabel))
                    .addComponent(jLabel47)
                    .addComponent(jLabel48)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, placeOrderDialogLayout.createSequentialGroup()
                        .addGroup(placeOrderDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(placeOrderDialogLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(assetOrderSubmitButton))
                            .addComponent(assetPassField)
                            .addComponent(assetPriceField, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(assetQuantityField)
                            .addComponent(orderTypeComboBox, 0, 166, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(placeOrderDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel35)
                            .addComponent(jLabel42)
                            .addComponent(jLabel43)
                            .addComponent(jLabel44))))
                .addContainerGap())
        );
        placeOrderDialogLayout.setVerticalGroup(
            placeOrderDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(placeOrderDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(placeOrderDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(assetIdOrderLabel)
                    .addComponent(jLabel45))
                .addGap(18, 18, 18)
                .addGroup(placeOrderDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(orderTypeComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel35))
                .addGap(18, 18, 18)
                .addGroup(placeOrderDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(assetQuantityField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel42))
                .addGap(18, 18, 18)
                .addGroup(placeOrderDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(assetPriceField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel43))
                .addGap(18, 18, 18)
                .addGroup(placeOrderDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(assetPassField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel44))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(assetOrderSubmitButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel47)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel48)
                .addContainerGap())
        );

        cancelOrderCMenu.setText("Cancel order");
        cancelOrderCMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelOrderCMenuActionPerformed(evt);
            }
        });
        contextMenuAssetOrders.add(cancelOrderCMenu);

        myOrderCancelDialog.setResizable(false);

        jLabel46.setText("secetPhrase");

        confirmCancelAssetButton.setText("Submit");
        confirmCancelAssetButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirmCancelAssetButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout myOrderCancelDialogLayout = new javax.swing.GroupLayout(myOrderCancelDialog.getContentPane());
        myOrderCancelDialog.getContentPane().setLayout(myOrderCancelDialogLayout);
        myOrderCancelDialogLayout.setHorizontalGroup(
            myOrderCancelDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(myOrderCancelDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(myOrderCancelDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(myOrderCancelDialogLayout.createSequentialGroup()
                        .addComponent(typeOrderLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(idOrderLabel))
                    .addComponent(cancelPassField, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(myOrderCancelDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel46)
                    .addComponent(confirmCancelAssetButton))
                .addContainerGap())
        );
        myOrderCancelDialogLayout.setVerticalGroup(
            myOrderCancelDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(myOrderCancelDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(myOrderCancelDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(typeOrderLabel)
                    .addComponent(idOrderLabel))
                .addGap(18, 18, 18)
                .addGroup(myOrderCancelDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cancelPassField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel46))
                .addGap(18, 18, 18)
                .addComponent(confirmCancelAssetButton)
                .addContainerGap())
        );

        jPanel6.setLayout(new java.awt.BorderLayout());

        assetTradesTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Time", "Quantity", "Price", "AskOrderId", "BidOrderId"
            }
        ));
        jScrollPane12.setViewportView(assetTradesTable);

        jPanel6.add(jScrollPane12, java.awt.BorderLayout.CENTER);

        javax.swing.GroupLayout assetTradesDialogLayout = new javax.swing.GroupLayout(assetTradesDialog.getContentPane());
        assetTradesDialog.getContentPane().setLayout(assetTradesDialogLayout);
        assetTradesDialogLayout.setHorizontalGroup(
            assetTradesDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, assetTradesDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, 655, Short.MAX_VALUE)
                .addContainerGap())
        );
        assetTradesDialogLayout.setVerticalGroup(
            assetTradesDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(assetTradesDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, 474, Short.MAX_VALUE)
                .addContainerGap())
        );

        jLabel52.setText("Website");

        tokenPassField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tokenPassFieldActionPerformed(evt);
            }
        });

        jLabel53.setText("secretPhrase");

        tokenTextPane.setEditable(false);
        jScrollPane15.setViewportView(tokenTextPane);

        jButton2.setText("Generate");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Decode");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout generateTokenDialogLayout = new javax.swing.GroupLayout(generateTokenDialog.getContentPane());
        generateTokenDialog.getContentPane().setLayout(generateTokenDialogLayout);
        generateTokenDialogLayout.setHorizontalGroup(
            generateTokenDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(generateTokenDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(generateTokenDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(generateTokenDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jScrollPane15, javax.swing.GroupLayout.DEFAULT_SIZE, 183, Short.MAX_VALUE)
                        .addComponent(tokenPassField)
                        .addComponent(websiteField))
                    .addComponent(tokenAccountIdLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(generateTokenDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel52)
                    .addComponent(jLabel53)
                    .addComponent(jButton2)
                    .addComponent(jButton3))
                .addGap(21, 21, 21))
        );
        generateTokenDialogLayout.setVerticalGroup(
            generateTokenDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(generateTokenDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(generateTokenDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(websiteField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel52))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(generateTokenDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tokenPassField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel53))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(generateTokenDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane15, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(generateTokenDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(tokenAccountIdLabel)
                    .addComponent(jButton3))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        creatPollButton.setText("Submit");
        creatPollButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                creatPollButtonActionPerformed(evt);
            }
        });

        voteDescriptionField.setColumns(20);
        voteDescriptionField.setRows(5);
        jScrollPane17.setViewportView(voteDescriptionField);

        voteMaxField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                voteMaxFieldActionPerformed(evt);
            }
        });

        jLabel54.setText(bundle.getString("NAME")); // NOI18N

        jLabel55.setText(bundle.getString("DESCRIPTION")); // NOI18N

        jLabel56.setText("Min");

        jLabel57.setText("Max");

        jLabel58.setText("Binary");

        voteOptionsField.setColumns(20);
        voteOptionsField.setRows(5);
        jScrollPane18.setViewportView(voteOptionsField);

        jLabel59.setText("Options");

        jLabel60.setText("each new line one option");

        voteOptionsAreBinaryComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "true", "false" }));

        jLabel61.setText("secretPhrase");

        javax.swing.GroupLayout createPollDialogLayout = new javax.swing.GroupLayout(createPollDialog.getContentPane());
        createPollDialog.getContentPane().setLayout(createPollDialogLayout);
        createPollDialogLayout.setHorizontalGroup(
            createPollDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(createPollDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(createPollDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, createPollDialogLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(creatPollButton))
                    .addGroup(createPollDialogLayout.createSequentialGroup()
                        .addGroup(createPollDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(voteSecretPassField, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(voteOptionsAreBinaryComboBox, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(voteMaxField, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(voteMinField, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(voteNameField, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane17, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane18, javax.swing.GroupLayout.Alignment.LEADING))
                        .addGap(18, 18, 18)
                        .addGroup(createPollDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel54)
                            .addComponent(jLabel55)
                            .addComponent(jLabel56)
                            .addComponent(jLabel57)
                            .addComponent(jLabel59)
                            .addComponent(jLabel58)
                            .addComponent(jLabel61)
                            .addComponent(jLabel60))
                        .addGap(0, 67, Short.MAX_VALUE)))
                .addContainerGap())
        );
        createPollDialogLayout.setVerticalGroup(
            createPollDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, createPollDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(createPollDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(createPollDialogLayout.createSequentialGroup()
                        .addGroup(createPollDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(voteNameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel54))
                        .addGap(18, 18, 18)
                        .addGroup(createPollDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel55))
                        .addGap(18, 18, 18)
                        .addGroup(createPollDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(createPollDialogLayout.createSequentialGroup()
                                .addComponent(jLabel59)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel60))
                            .addGroup(createPollDialogLayout.createSequentialGroup()
                                .addComponent(jScrollPane18, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(createPollDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(voteMinField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel56))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(createPollDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(voteMaxField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel57))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(voteOptionsAreBinaryComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel58))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(createPollDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(voteSecretPassField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel61))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(creatPollButton)
                .addContainerGap())
        );

        castVoteSubmitButton.setText("Submit");
        castVoteSubmitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                castVoteSubmitButtonActionPerformed(evt);
            }
        });

        jLabel5.setText("secretPhrase");

        jLabel6.setText("Options");

        javax.swing.GroupLayout castVoteDialogLayout = new javax.swing.GroupLayout(castVoteDialog.getContentPane());
        castVoteDialog.getContentPane().setLayout(castVoteDialogLayout);
        castVoteDialogLayout.setHorizontalGroup(
            castVoteDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(castVoteDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(castVoteDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, castVoteDialogLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(castVoteSubmitButton))
                    .addGroup(castVoteDialogLayout.createSequentialGroup()
                        .addGroup(castVoteDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(castVoteIdLabel)
                            .addGroup(castVoteDialogLayout.createSequentialGroup()
                                .addComponent(castVoteOptionComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel6))
                            .addGroup(castVoteDialogLayout.createSequentialGroup()
                                .addComponent(castVotePassField, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel5)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        castVoteDialogLayout.setVerticalGroup(
            castVoteDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(castVoteDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(castVoteIdLabel)
                .addGroup(castVoteDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(castVoteDialogLayout.createSequentialGroup()
                        .addGap(89, 117, Short.MAX_VALUE)
                        .addComponent(castVoteSubmitButton))
                    .addGroup(castVoteDialogLayout.createSequentialGroup()
                        .addGroup(castVoteDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(castVoteOptionComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(castVoteDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(castVotePassField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        castVoteMenuItem.setText("Cast vote");
        castVoteMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                castVoteMenuItemActionPerformed(evt);
            }
        });
        castVoteContextMenu.add(castVoteMenuItem);

        forgePassField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                forgePassFieldActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout forgeDialogLayout = new javax.swing.GroupLayout(forgeDialog.getContentPane());
        forgeDialog.getContentPane().setLayout(forgeDialogLayout);
        forgeDialogLayout.setHorizontalGroup(
            forgeDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(forgeDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(forgePassField, javax.swing.GroupLayout.PREFERRED_SIZE, 268, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        forgeDialogLayout.setVerticalGroup(
            forgeDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(forgeDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(forgePassField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        masterPasswordDialog.setTitle("null");

        masterPassowordField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                masterPassowordFieldActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout masterPasswordDialogLayout = new javax.swing.GroupLayout(masterPasswordDialog.getContentPane());
        masterPasswordDialog.getContentPane().setLayout(masterPasswordDialogLayout);
        masterPasswordDialogLayout.setHorizontalGroup(
            masterPasswordDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(masterPasswordDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(masterPassowordField, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                .addContainerGap())
        );
        masterPasswordDialogLayout.setVerticalGroup(
            masterPasswordDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(masterPasswordDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(masterPassowordField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        watchAccountNumberField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                watchAccountNumberFieldActionPerformed(evt);
            }
        });

        jLabel70.setText("Account number");

        jLabel62.setText("Label");

        javax.swing.GroupLayout watchAccountDialogLayout = new javax.swing.GroupLayout(watchAccountDialog.getContentPane());
        watchAccountDialog.getContentPane().setLayout(watchAccountDialogLayout);
        watchAccountDialogLayout.setHorizontalGroup(
            watchAccountDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(watchAccountDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(watchAccountDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel70)
                    .addComponent(jLabel62))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                .addGroup(watchAccountDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(labelWatchAccountField, javax.swing.GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE)
                    .addComponent(watchAccountNumberField))
                .addContainerGap())
        );
        watchAccountDialogLayout.setVerticalGroup(
            watchAccountDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(watchAccountDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(watchAccountDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel62)
                    .addComponent(labelWatchAccountField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(watchAccountDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(watchAccountNumberField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel70))
                .addContainerGap())
        );

        copySenderAddressCMenu.setText("Copy Sender address");
        copySenderAddressCMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                copySenderAddressCMenuActionPerformed(evt);
            }
        });
        TablePopupMenu.add(copySenderAddressCMenu);

        copyRecipientAddressCMenu.setText("Copy Recipient address");
        copyRecipientAddressCMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                copyRecipientAddressCMenuActionPerformed(evt);
            }
        });
        TablePopupMenu.add(copyRecipientAddressCMenu);

        sendNxtToSenderAddressCMenu.setText("Send NXT to Sender address");
        sendNxtToSenderAddressCMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendNxtToSenderAddressCMenuActionPerformed(evt);
            }
        });
        TablePopupMenu.add(sendNxtToSenderAddressCMenu);

        sendNxtToRecipientAddressCMenu.setText("Send NXT to Recipient address");
        sendNxtToRecipientAddressCMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendNxtToRecipientAddressCMenuActionPerformed(evt);
            }
        });
        TablePopupMenu.add(sendNxtToRecipientAddressCMenu);

        sendMsgToSenderAddressCMenu.setText("Send Message to Sender address");
        sendMsgToSenderAddressCMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendMsgToSenderAddressCMenuActionPerformed(evt);
            }
        });
        TablePopupMenu.add(sendMsgToSenderAddressCMenu);

        sendMsgToRecipientAddressCMenu.setText("Send Message to Recipient address");
        sendMsgToRecipientAddressCMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendMsgToRecipientAddressCMenuActionPerformed(evt);
            }
        });
        TablePopupMenu.add(sendMsgToRecipientAddressCMenu);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        MainToolbar.setFloatable(false);
        MainToolbar.setRollover(true);

        ToolbarButtonGroup.add(overviewButton);
        overviewButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/bhome.png"))); // NOI18N
        overviewButton.setText("Overview");
        overviewButton.setFocusable(false);
        overviewButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        overviewButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        overviewButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                overviewButtonActionPerformed(evt);
            }
        });
        MainToolbar.add(overviewButton);

        ToolbarButtonGroup.add(consoleButton);
        consoleButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/bmonitor.png"))); // NOI18N
        consoleButton.setText("Console");
        consoleButton.setFocusable(false);
        consoleButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        consoleButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        consoleButton.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                consoleButtonStateChanged(evt);
            }
        });
        consoleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                consoleButtonActionPerformed(evt);
            }
        });
        MainToolbar.add(consoleButton);

        ToolbarButtonGroup.add(unlockButton);
        unlockButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/block.png"))); // NOI18N
        unlockButton.setText("Unlock");
        unlockButton.setFocusable(false);
        unlockButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        unlockButton.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/images/bunlock.png"))); // NOI18N
        unlockButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        unlockButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                unlockButtonActionPerformed(evt);
            }
        });
        MainToolbar.add(unlockButton);

        ToolbarButtonGroup.add(sendButton);
        sendButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/bnext.png"))); // NOI18N
        sendButton.setText("Send");
        sendButton.setToolTipText("Send Nxt");
        sendButton.setFocusable(false);
        sendButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        sendButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        sendButton.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                sendButtonStateChanged(evt);
            }
        });
        sendButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendButtonActionPerformed(evt);
            }
        });
        MainToolbar.add(sendButton);

        ToolbarButtonGroup.add(messagesButton);
        messagesButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/bmail.png"))); // NOI18N
        messagesButton.setText("Messages");
        messagesButton.setToolTipText("Arbitrary messages");
        messagesButton.setFocusable(false);
        messagesButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        messagesButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        messagesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                messagesButtonActionPerformed(evt);
            }
        });
        MainToolbar.add(messagesButton);

        ToolbarButtonGroup.add(transactionsButton);
        transactionsButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/database.png"))); // NOI18N
        transactionsButton.setText("Transactions");
        transactionsButton.setFocusable(false);
        transactionsButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        transactionsButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        transactionsButton.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                transactionsButtonStateChanged(evt);
            }
        });
        transactionsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                transactionsButtonActionPerformed(evt);
            }
        });
        MainToolbar.add(transactionsButton);

        ToolbarButtonGroup.add(aliasButton);
        aliasButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/buser.png"))); // NOI18N
        aliasButton.setText("Alias");
        aliasButton.setToolTipText("");
        aliasButton.setFocusable(false);
        aliasButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        aliasButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        aliasButton.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                aliasButtonStateChanged(evt);
            }
        });
        aliasButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aliasButtonActionPerformed(evt);
            }
        });
        MainToolbar.add(aliasButton);

        ToolbarButtonGroup.add(assetexchangeButton);
        assetexchangeButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/bprogress.png"))); // NOI18N
        assetexchangeButton.setText("Asset Exchange");
        assetexchangeButton.setFocusable(false);
        assetexchangeButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        assetexchangeButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        assetexchangeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                assetexchangeButtonActionPerformed(evt);
            }
        });
        MainToolbar.add(assetexchangeButton);

        ToolbarButtonGroup.add(votingButton);
        votingButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/bcheck_mark.png"))); // NOI18N
        votingButton.setText("Voting System");
        votingButton.setFocusable(false);
        votingButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        votingButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        votingButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                votingButtonActionPerformed(evt);
            }
        });
        MainToolbar.add(votingButton);
        MainToolbar.add(filler1);

        MainPanel.setPreferredSize(new java.awt.Dimension(700, 725));
        MainPanel.setLayout(new javax.swing.OverlayLayout(MainPanel));

        consoleOutput.setEditable(false);
        consoleOutput.setColumns(20);
        consoleOutput.setLineWrap(true);
        consoleOutput.setRows(5);
        consoleOutput.setInheritsPopupMenu(true);
        consoleScrollPane.setViewportView(consoleOutput);

        javax.swing.GroupLayout ConsolePanelLayout = new javax.swing.GroupLayout(ConsolePanel);
        ConsolePanel.setLayout(ConsolePanelLayout);
        ConsolePanelLayout.setHorizontalGroup(
            ConsolePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ConsolePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(consoleScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 719, Short.MAX_VALUE)
                .addContainerGap())
        );
        ConsolePanelLayout.setVerticalGroup(
            ConsolePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ConsolePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(consoleScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 453, Short.MAX_VALUE)
                .addContainerGap())
        );

        MainPanel.add(ConsolePanel);

        allAssetsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null}
            },
            new String [] {
                "Account", "Name", "Description", "Quantity", "AssetId"
            }
        ));
        allAssetsScrollPane.setViewportView(allAssetsTable);

        ordersAssetButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/bapprove_notes.png"))); // NOI18N
        ordersAssetButton.setText("Orders");
        ordersAssetButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ordersAssetButtonActionPerformed(evt);
            }
        });

        issueAssetButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/badd_notes.png"))); // NOI18N
        issueAssetButton.setText("Issue asset");
        issueAssetButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                issueAssetButtonActionPerformed(evt);
            }
        });

        transferAssetButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/bedit_notes.png"))); // NOI18N
        transferAssetButton.setText("Transfer asset");
        transferAssetButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                transferAssetButtonActionPerformed(evt);
            }
        });

        searchAssetField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchAssetFieldActionPerformed(evt);
            }
        });
        searchAssetField.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                searchAssetFieldInputMethodTextChanged(evt);
            }
        });

        jLabel32.setText("search asset");

        getAssetsButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/bnotes.png"))); // NOI18N
        getAssetsButton.setText("Get assets");
        getAssetsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                getAssetsButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout AssetExPanelLayout = new javax.swing.GroupLayout(AssetExPanel);
        AssetExPanel.setLayout(AssetExPanelLayout);
        AssetExPanelLayout.setHorizontalGroup(
            AssetExPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AssetExPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(AssetExPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(AssetExPanelLayout.createSequentialGroup()
                        .addComponent(getAssetsButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(issueAssetButton)
                        .addGap(10, 10, 10)
                        .addComponent(transferAssetButton)
                        .addGap(10, 10, 10)
                        .addComponent(ordersAssetButton)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel32)
                        .addGap(10, 10, 10)
                        .addComponent(searchAssetField, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(allAssetsScrollPane))
                .addContainerGap())
        );
        AssetExPanelLayout.setVerticalGroup(
            AssetExPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AssetExPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(AssetExPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(getAssetsButton)
                    .addComponent(issueAssetButton)
                    .addComponent(transferAssetButton)
                    .addComponent(ordersAssetButton)
                    .addComponent(jLabel32)
                    .addComponent(searchAssetField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(allAssetsScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 414, Short.MAX_VALUE)
                .addContainerGap())
        );

        MainPanel.add(AssetExPanel);

        SendPanel.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                SendPanelPropertyChange(evt);
            }
        });

        receiverLabel.setText("address of recipient");

        amountLabel.setText("amount");

        sendNxtButton.setText("Send");
        sendNxtButton.setMaximumSize(new java.awt.Dimension(99, 23));
        sendNxtButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendNxtButtonActionPerformed(evt);
            }
        });

        jLabel11.setText("masterPassword/secretPhrase");

        senderAddressComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { " " }));
        senderAddressComboBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                senderAddressComboBoxMouseClicked(evt);
            }
        });
        senderAddressComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                senderAddressComboBoxActionPerformed(evt);
            }
        });

        senderLabel.setText("address of sender");

        attachMessageCheckBox.setText("attach message");
        attachMessageCheckBox.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                attachMessageCheckBoxStateChanged(evt);
            }
        });
        attachMessageCheckBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                attachMessageCheckBoxItemStateChanged(evt);
            }
        });
        attachMessageCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                attachMessageCheckBoxActionPerformed(evt);
            }
        });

        inputMessageField.setColumns(20);
        inputMessageField.setRows(5);
        inputMessageField.setToolTipText("message content");
        jScrollPane3.setViewportView(inputMessageField);

        jLabel2.setForeground(new java.awt.Color(102, 102, 255));
        jLabel2.setText("*     Bulk send separator \":\", end with \":\", example 1234567890:22345678901:109876543321:");
        jLabel2.setAutoscrolls(true);

        jLabel3.setForeground(new java.awt.Color(102, 102, 255));
        jLabel3.setText("**   Amount = 0 only for messages, not 0 means Nxt transfer, bulk separator with \":\", respectively");

        jLabel10.setForeground(new java.awt.Color(102, 102, 255));
        jLabel10.setText("*** Bulk send Messages, also behaves same way, \":\" separator for each receiver and end with \":\"");

        javax.swing.GroupLayout SendPanelLayout = new javax.swing.GroupLayout(SendPanel);
        SendPanel.setLayout(SendPanelLayout);
        SendPanelLayout.setHorizontalGroup(
            SendPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SendPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(SendPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(SendPanelLayout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(SendPanelLayout.createSequentialGroup()
                        .addGroup(SendPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(SendPanelLayout.createSequentialGroup()
                                .addComponent(sendNxtButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(attachMessageCheckBox))
                            .addComponent(jLabel3)
                            .addComponent(jLabel2)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 407, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(SendPanelLayout.createSequentialGroup()
                                .addGroup(SendPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(SendPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(amountNxtField, javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(receiverAddressField, javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(senderAddressComboBox, javax.swing.GroupLayout.Alignment.LEADING, 0, 240, Short.MAX_VALUE))
                                    .addComponent(passphraseField, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(47, 47, 47)
                                .addGroup(SendPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel11)
                                    .addComponent(receiverLabel)
                                    .addComponent(senderLabel)
                                    .addComponent(amountLabel))))
                        .addContainerGap(254, Short.MAX_VALUE))))
        );
        SendPanelLayout.setVerticalGroup(
            SendPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SendPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(SendPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(senderAddressComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(senderLabel))
                .addGap(18, 18, 18)
                .addGroup(SendPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(receiverAddressField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(receiverLabel))
                .addGap(18, 18, 18)
                .addGroup(SendPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(amountNxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(amountLabel))
                .addGap(18, 18, 18)
                .addGroup(SendPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(passphraseField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11))
                .addGap(18, 18, Short.MAX_VALUE)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(SendPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sendNxtButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(attachMessageCheckBox))
                .addGap(18, 18, Short.MAX_VALUE)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel10)
                .addContainerGap())
        );

        MainPanel.add(SendPanel);

        TransactionsPanel.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                TransactionsPanelPropertyChange(evt);
            }
        });

        transactionsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Sender", "Recipient", "Amount", "Fee", "Confirms", "Time", "Message", "Asset"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        transactionsTable.setCellSelectionEnabled(true);
        TransactionsScrollPane.setViewportView(transactionsTable);

        TXaccountListCombobox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TXaccountListComboboxActionPerformed(evt);
            }
        });

        jLabel7.setText("Select account");

        javax.swing.GroupLayout TransactionsPanelLayout = new javax.swing.GroupLayout(TransactionsPanel);
        TransactionsPanel.setLayout(TransactionsPanelLayout);
        TransactionsPanelLayout.setHorizontalGroup(
            TransactionsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(TransactionsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(TransactionsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(TransactionsScrollPane)
                    .addGroup(TransactionsPanelLayout.createSequentialGroup()
                        .addComponent(TXaccountListCombobox, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel7)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        TransactionsPanelLayout.setVerticalGroup(
            TransactionsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, TransactionsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(TransactionsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(TXaccountListCombobox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(TransactionsScrollPane)
                .addContainerGap())
        );

        MainPanel.add(TransactionsPanel);

        AccountsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null}
            },
            new String [] {
                "Label", "Account_Number", "Balance", "Status"
            }
        ));
        AccountsTableScrollPane.setViewportView(AccountsTable);

        AddAccountButton.setText("Add");
        AddAccountButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AddAccountButtonActionPerformed(evt);
            }
        });

        RemoveAccountButton.setText("Remove");
        RemoveAccountButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RemoveAccountButtonActionPerformed(evt);
            }
        });

        updateBalancesButton.setText("Update");
        updateBalancesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateBalancesButtonActionPerformed(evt);
            }
        });

        generateTokenButton.setText("Token");
        generateTokenButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generateTokenButtonActionPerformed(evt);
            }
        });

        qrCodeButton.setText("QR-code");
        qrCodeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                qrCodeButtonActionPerformed(evt);
            }
        });

        forgeButton.setText("Forge");
        forgeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                forgeButtonActionPerformed(evt);
            }
        });

        jButton8.setText("SecretPhrase");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        jButton9.setText(bundle.getString("GENERATE")); // NOI18N
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });

        jButton10.setText("Watch");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout ReceivePanelLayout = new javax.swing.GroupLayout(ReceivePanel);
        ReceivePanel.setLayout(ReceivePanelLayout);
        ReceivePanelLayout.setHorizontalGroup(
            ReceivePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ReceivePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(ReceivePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ReceivePanelLayout.createSequentialGroup()
                        .addComponent(AddAccountButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(RemoveAccountButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(updateBalancesButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(forgeButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 48, Short.MAX_VALUE)
                        .addComponent(qrCodeButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(generateTokenButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(filler2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(accountsSeparator)
                    .addComponent(AccountsTableScrollPane))
                .addContainerGap())
        );
        ReceivePanelLayout.setVerticalGroup(
            ReceivePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ReceivePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(AccountsTableScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 401, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(accountsSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 11, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(ReceivePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ReceivePanelLayout.createSequentialGroup()
                        .addGroup(ReceivePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ReceivePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(generateTokenButton)
                                .addComponent(qrCodeButton))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ReceivePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(AddAccountButton)
                                .addComponent(updateBalancesButton)
                                .addComponent(RemoveAccountButton)
                                .addComponent(forgeButton)
                                .addComponent(jButton8)
                                .addComponent(jButton9)
                                .addComponent(jButton10)))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ReceivePanelLayout.createSequentialGroup()
                        .addComponent(filler2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(22, 22, 22))))
        );

        MainPanel.add(ReceivePanel);

        messagesTable.setAutoCreateRowSorter(true);
        messagesTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null}
            },
            new String [] {
                "Sender", "Recipient", "Message", "Time"
            }
        ));
        messagesTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                messagesTableMouseClicked(evt);
            }
        });
        jScrollPane5.setViewportView(messagesTable);

        messageSenderComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { " " }));
        messageSenderComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                messageSenderComboBoxActionPerformed(evt);
            }
        });

        messageContentField.setColumns(20);
        messageContentField.setRows(5);
        jScrollPane8.setViewportView(messageContentField);

        messageSendButton.setText("Send");
        messageSendButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                messageSendButtonActionPerformed(evt);
            }
        });

        jLabel72.setText("Select account");

        jLabel73.setText("Recipient");

        jLabel74.setText("SecretPhrase");

        jLabel75.setText("Message content");

        jButton11.setText("Decrypt");
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout MessagesPanelLayout = new javax.swing.GroupLayout(MessagesPanel);
        MessagesPanel.setLayout(MessagesPanelLayout);
        MessagesPanelLayout.setHorizontalGroup(
            MessagesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MessagesPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(MessagesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 719, Short.MAX_VALUE)
                    .addGroup(MessagesPanelLayout.createSequentialGroup()
                        .addGroup(MessagesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel72)
                            .addComponent(jLabel73)
                            .addComponent(jLabel74))
                        .addGap(18, 18, 18)
                        .addGroup(MessagesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(MessagesPanelLayout.createSequentialGroup()
                                .addComponent(messageSendButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButton11))
                            .addGroup(MessagesPanelLayout.createSequentialGroup()
                                .addGroup(MessagesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(messageSecretPhraseField, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(messageRecipientField, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(MessagesPanelLayout.createSequentialGroup()
                                        .addComponent(messageSenderComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel75)))
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 340, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        MessagesPanelLayout.setVerticalGroup(
            MessagesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, MessagesPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(MessagesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(MessagesPanelLayout.createSequentialGroup()
                        .addGroup(MessagesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(messageSenderComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel72)
                            .addComponent(jLabel75))
                        .addGap(18, 18, 18)
                        .addGroup(MessagesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(messageRecipientField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel73))
                        .addGap(18, 18, 18)
                        .addGroup(MessagesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(messageSecretPhraseField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel74)))
                    .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(MessagesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(messageSendButton)
                    .addComponent(jButton11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 14, Short.MAX_VALUE)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 309, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        MainPanel.add(MessagesPanel);

        BlocksTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "LastBlock", "Height", "Generator", "Time", "NumberOfTransactions", "Total Amount", "Total Fee", "Transactions"
            }
        ));
        jScrollPane7.setViewportView(BlocksTable);

        javax.swing.GroupLayout BlocksPanelLayout = new javax.swing.GroupLayout(BlocksPanel);
        BlocksPanel.setLayout(BlocksPanelLayout);
        BlocksPanelLayout.setHorizontalGroup(
            BlocksPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BlocksPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 719, Short.MAX_VALUE)
                .addContainerGap())
        );
        BlocksPanelLayout.setVerticalGroup(
            BlocksPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BlocksPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 453, Short.MAX_VALUE)
                .addContainerGap())
        );

        MainPanel.add(BlocksPanel);

        AliasesPanel.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                AliasesPanelPropertyChange(evt);
            }
        });

        aliasTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null}
            },
            new String [] {
                "Alias", "URI"
            }
        ));
        aliasTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                aliasTableMouseClicked(evt);
            }
        });
        jScrollPane10.setViewportView(aliasTable);

        jLabel16.setText("Alias");

        jLabel19.setText("URI");

        assignAliasButton.setText("Assign");
        assignAliasButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                assignAliasButtonActionPerformed(evt);
            }
        });

        ALaccountListComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ALaccountListComboBoxActionPerformed(evt);
            }
        });

        jLabel8.setText("Select account");

        jLabel9.setText("SecretPhrase");

        aliasSearchField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aliasSearchFieldActionPerformed(evt);
            }
        });

        jLabel33.setText("search alias");

        javax.swing.GroupLayout AliasesPanelLayout = new javax.swing.GroupLayout(AliasesPanel);
        AliasesPanel.setLayout(AliasesPanelLayout);
        AliasesPanelLayout.setHorizontalGroup(
            AliasesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AliasesPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(AliasesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane10)
                    .addGroup(AliasesPanelLayout.createSequentialGroup()
                        .addGroup(AliasesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(AliasesPanelLayout.createSequentialGroup()
                                .addComponent(uriField, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel19))
                            .addGroup(AliasesPanelLayout.createSequentialGroup()
                                .addComponent(aliasField, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel16))
                            .addGroup(AliasesPanelLayout.createSequentialGroup()
                                .addGroup(AliasesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(assignAliasButton, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, AliasesPanelLayout.createSequentialGroup()
                                        .addComponent(aliasAccountPassphrase, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel9)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(outputL, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 374, Short.MAX_VALUE))
                    .addGroup(AliasesPanelLayout.createSequentialGroup()
                        .addComponent(ALaccountListComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(aliasSearchField, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel33)))
                .addContainerGap())
        );
        AliasesPanelLayout.setVerticalGroup(
            AliasesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AliasesPanelLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(AliasesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ALaccountListComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(aliasSearchField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel33))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, 233, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(AliasesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(aliasField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel16, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(18, 18, 18)
                .addGroup(AliasesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(uriField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel19))
                .addGap(18, 18, 18)
                .addGroup(AliasesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(aliasAccountPassphrase, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addGap(18, 18, 18)
                .addGroup(AliasesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(assignAliasButton)
                    .addComponent(outputL, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        MainPanel.add(AliasesPanel);

        jLabel13.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        jLabel13.setText("Wallet");

        jLabel14.setText("Total balance");

        jLabel23.setText("in BTC");

        jLabel24.setText("in USD");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane13.setViewportView(jTable1);

        jLabel49.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        jLabel49.setText("Assets");

        jScrollPane14.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        overviewAssetTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "account label", "asset name", "quantity"
            }
        ));
        jScrollPane14.setViewportView(overviewAssetTable);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel49)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel13)
                                    .addComponent(jLabel23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel24, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(totalBalanceLabelNXT, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(totalBalanceLabelBTC, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(totalBalanceLabelUSD, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addComponent(jScrollPane14))
                        .addContainerGap())))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel13)
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(totalBalanceLabelNXT))
                .addGap(28, 28, 28)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel23)
                    .addComponent(totalBalanceLabelBTC))
                .addGap(37, 37, 37)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(totalBalanceLabelUSD)
                    .addComponent(jLabel24))
                .addGap(18, 18, 18)
                .addComponent(jLabel49)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane14, javax.swing.GroupLayout.PREFERRED_SIZE, 368, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jLabel15.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        jLabel15.setText("Market");

        jLabel21.setText("NXT/BTC");

        jLabel22.setText("NXT/USD");

        jLabel25.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N
        jLabel25.setText("[data from bitstamp and bter]");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel15)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel21)
                            .addComponent(jLabel22))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(NxtUsdLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(NxtBtcLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabel25))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel15)
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(NxtBtcLabel)
                    .addComponent(jLabel21))
                .addGap(32, 32, 32)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel22)
                    .addComponent(NxtUsdLabel))
                .addGap(18, 18, Short.MAX_VALUE)
                .addComponent(jLabel25)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout OverviewPanelLayout = new javax.swing.GroupLayout(OverviewPanel);
        OverviewPanel.setLayout(OverviewPanelLayout);
        OverviewPanelLayout.setHorizontalGroup(
            OverviewPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OverviewPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        OverviewPanelLayout.setVerticalGroup(
            OverviewPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OverviewPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        MainPanel.add(OverviewPanel);

        allPollsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null}
            },
            new String [] {
                "Name", "Description", "Options", "Votes", "PollId"
            }
        ));
        jScrollPane16.setViewportView(allPollsTable);

        getPollsButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/bnotes.png"))); // NOI18N
        getPollsButton.setText("Get polls");
        getPollsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                getPollsButtonActionPerformed(evt);
            }
        });

        createPollsButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/badd_notes.png"))); // NOI18N
        createPollsButton.setText("Create poll");
        createPollsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createPollsButtonActionPerformed(evt);
            }
        });

        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/bapprove_notes.png"))); // NOI18N
        jButton6.setText("My votes");
        jButton6.setEnabled(false);

        jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/bedit_notes.png"))); // NOI18N
        jButton7.setText("...");
        jButton7.setEnabled(false);

        javax.swing.GroupLayout VotingPanelLayout = new javax.swing.GroupLayout(VotingPanel);
        VotingPanel.setLayout(VotingPanelLayout);
        VotingPanelLayout.setHorizontalGroup(
            VotingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(VotingPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(VotingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane16)
                    .addGroup(VotingPanelLayout.createSequentialGroup()
                        .addComponent(getPollsButton)
                        .addGap(18, 18, 18)
                        .addComponent(createPollsButton)
                        .addGap(18, 18, 18)
                        .addComponent(jButton6)
                        .addGap(18, 18, 18)
                        .addComponent(jButton7)
                        .addGap(0, 273, Short.MAX_VALUE)))
                .addContainerGap())
        );
        VotingPanelLayout.setVerticalGroup(
            VotingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, VotingPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(VotingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(getPollsButton)
                    .addComponent(createPollsButton)
                    .addComponent(jButton6)
                    .addComponent(jButton7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane16, javax.swing.GroupLayout.DEFAULT_SIZE, 409, Short.MAX_VALUE)
                .addContainerGap())
        );

        MainPanel.add(VotingPanel);

        statusBarPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        statusBarPanel.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        statusBarPanel.setLayout(new javax.swing.BoxLayout(statusBarPanel, javax.swing.BoxLayout.LINE_AXIS));
        statusBarPanel.add(statusbarLabel);

        jProgressBar1.setStringPainted(true);
        statusBarPanel.add(jProgressBar1);
        statusBarPanel.add(timeToForgeLabel);

        latestBlockLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/bnetwork.png"))); // NOI18N
        statusBarPanel.add(latestBlockLabel);

        peersLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/bchart.png"))); // NOI18N
        statusBarPanel.add(peersLabel);

        FileMenu.setText("File");

        overviewMenu.setText(bundle.getString("OVERVIEW")); // NOI18N
        overviewMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                overviewMenuActionPerformed(evt);
            }
        });
        FileMenu.add(overviewMenu);

        consoleMenu.setText(bundle.getString("CONSOLE")); // NOI18N
        consoleMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                consoleMenuActionPerformed(evt);
            }
        });
        FileMenu.add(consoleMenu);

        sendMenu.setText("Send");
        sendMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendMenuActionPerformed(evt);
            }
        });
        FileMenu.add(sendMenu);

        receiveMenu.setText(bundle.getString("UNLOCK")); // NOI18N
        receiveMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                receiveMenuActionPerformed(evt);
            }
        });
        FileMenu.add(receiveMenu);

        transactionsMenu.setText(bundle.getString("TRANSACTIONS")); // NOI18N
        transactionsMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                transactionsMenuActionPerformed(evt);
            }
        });
        FileMenu.add(transactionsMenu);

        aliasMenu.setText(bundle.getString("ALIAS")); // NOI18N
        aliasMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aliasMenuActionPerformed(evt);
            }
        });
        FileMenu.add(aliasMenu);

        assetExMenu.setText(bundle.getString("ASSETS")); // NOI18N
        assetExMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                assetExMenuActionPerformed(evt);
            }
        });
        FileMenu.add(assetExMenu);

        votingMenu.setText(bundle.getString("VOTING")); // NOI18N
        votingMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                votingMenuActionPerformed(evt);
            }
        });
        FileMenu.add(votingMenu);

        exitMenuItem.setText(bundle.getString("EXIT")); // NOI18N
        exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitMenuItemActionPerformed(evt);
            }
        });
        FileMenu.add(exitMenuItem);

        MenuBar.add(FileMenu);

        settingsMenu.setText(bundle.getString("SETTINGS")); // NOI18N

        languagesMenuItem.setText(bundle.getString("LANGUAGES")); // NOI18N

        menuLanguageButtonGroup.add(englishMenuItem);
        englishMenuItem.setSelected(true);
        englishMenuItem.setText(bundle.getString("ENGLISH")); // NOI18N
        englishMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/flag_usa.png"))); // NOI18N
        englishMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                englishMenuItemActionPerformed(evt);
            }
        });
        languagesMenuItem.add(englishMenuItem);

        menuLanguageButtonGroup.add(russianMenuItem);
        russianMenuItem.setText(bundle.getString("RUSSIAN")); // NOI18N
        russianMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/flag_russia.png"))); // NOI18N
        russianMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                russianMenuItemActionPerformed(evt);
            }
        });
        languagesMenuItem.add(russianMenuItem);

        menuLanguageButtonGroup.add(germanMenuItem);
        germanMenuItem.setText(bundle.getString("GERMAN")); // NOI18N
        germanMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/flag_germany.png"))); // NOI18N
        germanMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                germanMenuItemActionPerformed(evt);
            }
        });
        languagesMenuItem.add(germanMenuItem);

        menuLanguageButtonGroup.add(portugueseMenuItem);
        portugueseMenuItem.setText(bundle.getString("PORTUGUESE")); // NOI18N
        portugueseMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/flag_portugal.png"))); // NOI18N
        portugueseMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                portugueseMenuItemActionPerformed(evt);
            }
        });
        languagesMenuItem.add(portugueseMenuItem);

        menuLanguageButtonGroup.add(spanishMenuItem);
        spanishMenuItem.setText(bundle.getString("SPANISH")); // NOI18N
        spanishMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/flag_spain.png"))); // NOI18N
        spanishMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                spanishMenuItemActionPerformed(evt);
            }
        });
        languagesMenuItem.add(spanishMenuItem);

        othersMenuItemItem.setText(bundle.getString("OTHERS")); // NOI18N
        othersMenuItemItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                othersMenuItemItemActionPerformed(evt);
            }
        });
        languagesMenuItem.add(othersMenuItemItem);

        settingsMenu.add(languagesMenuItem);

        preferencesMenuItem.setText(bundle.getString("PREFERENCES")); // NOI18N
        preferencesMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                preferencesMenuItemActionPerformed(evt);
            }
        });
        settingsMenu.add(preferencesMenuItem);

        MenuBar.add(settingsMenu);

        aboutMenu.setText(bundle.getString("ABOUT")); // NOI18N

        aboutNxtMenuItem.setText(bundle.getString("ABOUT NXT")); // NOI18N
        aboutMenu.add(aboutNxtMenuItem);

        checkNxtUpdateMenu.setText(bundle.getString("CHECK NXT UPDATES")); // NOI18N
        checkNxtUpdateMenu.setEnabled(false);
        checkNxtUpdateMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkNxtUpdateMenuActionPerformed(evt);
            }
        });
        aboutMenu.add(checkNxtUpdateMenu);

        aboutClienxtMenuItem.setText(bundle.getString("ABOUT CLIENXT")); // NOI18N
        aboutClienxtMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aboutClienxtMenuItemActionPerformed(evt);
            }
        });
        aboutMenu.add(aboutClienxtMenuItem);

        jMenuItem2.setText(bundle.getString("CHECK CLIENXT UPDATES")); // NOI18N
        jMenuItem2.setEnabled(false);
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        aboutMenu.add(jMenuItem2);

        MenuBar.add(aboutMenu);

        setJMenuBar(MenuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(MainToolbar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(MainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(statusBarPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(MainToolbar, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(MainPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 475, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(statusBarPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Called when table row selection changes.
    private void tableSelectionChanged() {
        /* Unregister from receiving notifications
        from the last selected download. */
        if (selectedAccount != null)
            selectedAccount.deleteObserver(ClienxtForm.this);
        /* If not in the middle of clearing a download,
        set the selected download and register to
        receive notifications from it. */
        if (!clearing && AccountsTable.getSelectedRow() > -1) {
            selectedAccount = tableModelA.getAccount(AccountsTable.getSelectedRow());
            selectedAccount.addObserver(ClienxtForm.this);
            updateButtons();
        }
    }

    private void sendMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendMenuActionPerformed
            sendButton.setSelected(true);
            TransactionsPanel.setVisible(false);
            ConsolePanel.setVisible(false);
            SendPanel.setVisible(true);
            ReceivePanel.setVisible(false);
            AliasesPanel.setVisible(false);
            MessagesPanel.setVisible(false);
            BlocksPanel.setVisible(false);
            OverviewPanel.setVisible(false);
            AssetExPanel.setVisible(false);
            VotingPanel.setVisible(false);
    }//GEN-LAST:event_sendMenuActionPerformed
    private final String globalURL;
    private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed
        //exit from program
        if (flag == 1){
           nxt.Nxt.shutdown();
           flag = 0;
        }
        Db.shutdown();
        //p.destroy();
        System.exit(0);
    }//GEN-LAST:event_exitMenuItemActionPerformed
    private String passPhrases;
    private Long deadlineF;
    private AccountTableModel tableModelA;
    private AccountGUI selectedAccount;
    private boolean clearing;
    private final List<Object> Generators = new ArrayList<Object>();
    private Map mMap = new HashMap();
    class PasswordUtil {
        /** Minimum password length = 6 */
        public final int MIN_PASSWORD_LENGTH = 16;
        /** Maximum password length = 8 */
        public final int MAX_PASSWORD_LENGTH = 255;

        /** Uppercase characters A-Z */
        public final char[] UPPERS = new char[26];
        /** Lowercase characters a-z */
        public final char[] LOWERS = new char[26];
        /**
         * Printable non-alphanumeric characters, excluding space.
         */
        public final char[] SPECIALS = new char[32];
        public final char[] DIGITS = new char[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        {
            // Static initializer block for populating arrays
            int U = 'A';
            int l = 'a';
            int d = '0';
            for (int i = 0; i < 26; i++) {
                UPPERS[i] = (char) (U + i);
                LOWERS[i] = (char) (l + i);
                if (i < 10) {
                        DIGITS[i] = (char) (d + i);
                }
            }
            int p = 0;
            for (int s = 33; s < 127; s++) {
                char specialChar = (char) 32;

                if (s >= 'a' && s <= 'z')
                        s = 'z' + 1; // jump over 'a' to 'z'
                else if (s >= 'A' && s <= 'Z')
                        s = 'Z' + 1; // jump over 'A' to 'Z'
                else if (s >= '0' && s <= '9')
                        s = '9' + 1; // jump over '0' to '9'

                specialChar = (char) s;
                SPECIALS[p] = specialChar;
                p++;
            }
        }

        public String generatePassword() {
            List<char[]> activeSets = new ArrayList<char[]>(4);
            List<char[]> inactiveSets = new ArrayList<char[]>(4);

            activeSets.add(UPPERS);
            activeSets.add(LOWERS);
            activeSets.add(SPECIALS);
            activeSets.add(DIGITS);

            SecureRandom random = new SecureRandom();

            int passwordLength = 15 + random.nextInt(240);
            StringBuffer password = new StringBuffer(passwordLength + 1);

            for (int p = 0; p <= passwordLength; p++) {
                char[] randomSet = null;
                if (activeSets.size() > 1) {
                    int rSet = random.nextInt(activeSets.size());
                    randomSet = activeSets.get(rSet);
                    inactiveSets.add(randomSet);
                    activeSets.remove(rSet);
                } else {
                    randomSet = activeSets.get(0);
                    inactiveSets.add(randomSet);
                    activeSets.clear();
                    activeSets.addAll(inactiveSets);
                    inactiveSets.clear();
                }
                int rChar = random.nextInt(randomSet.length);
                char randomChar = randomSet[rChar];
                password.append(randomChar);
            }
            return password.toString();
        }
    }
    
    private void overviewMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_overviewMenuActionPerformed
            TransactionsPanel.setVisible(false);
            ConsolePanel.setVisible(false);
            SendPanel.setVisible(false);
            ReceivePanel.setVisible(false);
            AliasesPanel.setVisible(false);
            MessagesPanel.setVisible(false);
            BlocksPanel.setVisible(false);
            OverviewPanel.setVisible(true);
            overviewButton.setSelected(true);
            AssetExPanel.setVisible(false);
        VotingPanel.setVisible(false);
    }//GEN-LAST:event_overviewMenuActionPerformed

    private void othersMenuItemItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_othersMenuItemItemActionPerformed
        String volunteers = java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("VOLUNTEER TRANSLATORS ARE VERY WELCOME TO CONTRIBUTE!");
        JOptionPane.showMessageDialog(null, volunteers);
    }//GEN-LAST:event_othersMenuItemItemActionPerformed
    private String[] getCurrentState(){
        String blockAPI = "nxt?requestType=getState";
        
        String strA = null;
        try {
            strA = urlExec(globalURL,blockAPI);
        } catch (IOException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        String lastBlock = null;
        JSONParser parser = new JSONParser();
        Object obj = null;
        try {
            obj = parser.parse(strA);
        } catch (ParseException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        JSONObject jsonObject = (JSONObject) obj;
        if (jsonObject != null){
            lastBlock = (String) jsonObject.get("lastBlock");
        }
        //nxt.Blockchain nn = null;
        //String lastBlock = nn.getLastBlock().getStringId();
        return new String[] {lastBlock};
    }
    private int selectedIndex;
    DefaultTableModel tableModelBlock;
    private void getBlocks() throws IOException{
        
        Timestamp later = Timestamp.valueOf("2013-11-24 13:00:00");
        Long height = (Long.parseLong("0"));
        Long timestamp = (Long.parseLong("0"));
        Long totalAmount = (Long.parseLong("0"));
        Long totalFee = (Long.parseLong("0"));
        
        String[] data = getCurrentState();
        
        String blocksAPI = "nxt?requestType=getBlock&block=";
        blocksAPI += URLEncoder.encode(data[0], "UTF-8");
        String strB = urlExec(globalURL,blocksAPI);
        
        JSONParser parser2 = new JSONParser();
        Object obj2 = null;
        try {
            obj2 = parser2.parse(strB);
        } catch (ParseException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        JSONObject jsonObject2 = (JSONObject) obj2;
        if (jsonObject2 != null){
            height = (Long) jsonObject2.get("height");
            timestamp = (Long) jsonObject2.get("timestamp");
            totalAmount = (Long) jsonObject2.get("totalAmount");
            totalFee = (Long) jsonObject2.get("totalFee");
            Date sdf = null;
            try {
                sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2013-11-24 13:00:00");
            } catch (java.text.ParseException ex) {
                Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
            }
            later = new Timestamp(timestamp * 1000 + sdf.getTime());
        }
        latestBlockLabel.setToolTipText(java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("BLOCK TIME: ")+later.toString()+java.util.ResourceBundle.getBundle("clienxt/Bundle").getString(" HEIGHT: ")+height.toString()+java.util.ResourceBundle.getBundle("clienxt/Bundle").getString(" TOTALAMOUNT: ")+totalAmount.toString()+java.util.ResourceBundle.getBundle("clienxt/Bundle").getString(" TOTALFEE: ")+totalFee.toString());
    }
    private void getAssetIds() throws IOException{
        
        //check database table if it is empty
        //JCTable assetsT = new JCTable("lib/assets");
        //JCRow[] resultAll = assetsT.select();
        
        //gui table
        Object[][] rowData = {};
        Object[] columnNames = {java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("ACCOUNT"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("NAME"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("DESCRIPTION"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("QUANTITY"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("ASSETID")};

        DefaultTableModel tableModelAssets = new DefaultTableModel(rowData, columnNames);
        allAssetsTable.setModel(tableModelAssets);

        //get from network and save to database
        //get all assetIds
        String assetsAPI = "nxt?requestType=getAssetIds";
        String strA = urlExec(globalURL,assetsAPI);

        JSONParser parser3 = new JSONParser();
        Object obj3 = null;
        try {
            obj3 = parser3.parse(strA);
        } catch (ParseException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        JSONObject jsonObject3 = (JSONObject) obj3;
        JSONArray assets = null;
        int totalDataRead = 0;
        jProgressBar1.setValue(0);
        jProgressBar1.setStringPainted(true);



        if (jsonObject3 != null){
            assets = (JSONArray) jsonObject3.get("assetIds");
            Iterator<String> iterator = assets.iterator();
            int length = assets.size();
            while (iterator.hasNext()) {
                String data[];
                data = getAsset(iterator.next());

                tableModelAssets.addRow(new Object[]{data[0], data[1], data[2], data[3], data[4]});
                allAssetsTable.repaint();
                tableModelAssets.fireTableDataChanged();

                totalDataRead=totalDataRead+1;
                float Percent=(totalDataRead*100)/length;
                jProgressBar1.setValue((int)Percent);
            }
        }
    }
    private String[] getAsset(String assetId){
        //get asset details and add to the table
        String account = null;
        String name = null;
        String description = null;
        Long quantity = null;
        
        String assetAPI = "nxt?requestType=getAsset&asset=";
        try {
            assetAPI += URLEncoder.encode(assetId, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        String strB = null;
        try {
            strB = urlExec(globalURL,assetAPI);
        } catch (IOException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }

        JSONParser parser4 = new JSONParser();
        Object obj4 = null;
        try {
            obj4 = parser4.parse(strB);
        } catch (ParseException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        JSONObject jsonObject4 = (JSONObject) obj4;
        if (jsonObject4 != null){
            account = (String) jsonObject4.get("account");
            name = (String) jsonObject4.get("name");
            description = (String) jsonObject4.get("description");
            quantity = (Long) jsonObject4.get("quantity");
        }

        return new String[] {account, name, description, quantity.toString(), assetId};
    }
    private final List<String> optionsList = new ArrayList<String>();
    private String[] getPoll(String PollId){
        //get asset details and add to the table
        //String account = null;
        String name = null;
        String description = null;
        //Long quantity = null;
        
        String pollAPI = "nxt?requestType=getPoll&poll=";
        try {
            pollAPI += URLEncoder.encode(PollId, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        String strB = null;
        try {
            strB = urlExec(globalURL,pollAPI);
        } catch (IOException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }

        JSONParser parser4 = new JSONParser();
        Object obj4 = null;
        try {
            obj4 = parser4.parse(strB);
        } catch (ParseException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        JSONObject jsonObject4 = (JSONObject) obj4;
        JSONArray options = null;
        JSONArray voters = null;
        String opt = "";
        String vot = "";
        if (jsonObject4 != null){
            name = (String) jsonObject4.get("name");
            description = (String) jsonObject4.get("description");
            options = (JSONArray) jsonObject4.get("options");
            Iterator<String> iterator = options.iterator();
            
            while (iterator.hasNext()) {
                //optionsList.add(iterator.next());
                opt += iterator.next()+":";
            }
            voters = (JSONArray) jsonObject4.get("voters");
            Iterator<String> iterator2 = voters.iterator();
            
            while (iterator2.hasNext()) {
                //optionsList.add(iterator.next());
                vot += iterator2.next()+":";
            }
        }
        return new String[] {name, description, opt, vot, PollId};
    }
    
    
    private final List<String> peerList = new ArrayList<String>();
    DefaultTableModel tableModelPeer;
    private void getPeers() throws IOException{
        Long peerState = Long.parseLong("0");
        Integer couts = 0;
        /*Object[][] rowData = {};
        Object[] columnNames = {"Platform", "Application", "Weight", "Hallmark", "State", "AnnouncedAddress", "DownloadedVolume", "Version", "UploadedVolume"};

        tableModelPeer = new DefaultTableModel(rowData, columnNames);
        peersTable.setModel(tableModelPeer);*/
        
        String peersAPI = "nxt?requestType=getPeers";
        String strA = urlExec(globalURL, peersAPI);
        //System.out.println(strA);
        
        JSONParser parser3 = new JSONParser();
        Object obj3 = null;
        try {
            obj3 = parser3.parse(strA);
        } catch (ParseException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        JSONObject jsonObject3 = (JSONObject) obj3;
        JSONArray peerss = null;
        if (jsonObject3 != null){
             peerss = (JSONArray) jsonObject3.get("peers");
             Iterator<String> iterator = peerss.iterator();

            while (iterator.hasNext()) {
                peerList.add(iterator.next());
            }
        }

        for (int j=0; j<peerList.size(); j++){
                
                String peerAPI = "nxt?requestType=getPeer&peer=";
                peerAPI += URLEncoder.encode(peerList.get(j).toString(), "UTF-8");
                String strB = urlExec(globalURL,peerAPI);

                JSONParser parser4 = new JSONParser();
                Object obj4 = null;
                try {
                    obj4 = parser4.parse(strB);
                } catch (ParseException ex) {
                    Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                }
                JSONObject jsonObject4 = (JSONObject) obj4;
                if (jsonObject4 != null){
                     peerState = (Long) jsonObject4.get("state");
                     couts += peerState.intValue();
                }
           }
        peersLabel.setToolTipText(java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("ACTIVE PEERS: ")+couts.toString());
    }
    
    DefaultTableModel tableModelMsg;
    private static Long TYPE;
    private static Long SUBTYPE;
    private void getTransactionIds(final String accountnumber) throws IOException{
        
        SwingWorker<TableModel, TableModel> getTransactionsWork;
        getTransactionsWork = new SwingWorker<TableModel, TableModel>() {
            DefaultTableModel tableModelt;
            @Override
            protected TableModel doInBackground() throws Exception {
                Object[][] rowData = {};
                Object[] columnNames = {java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("SENDER"), java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("RECIPIENT"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("AMOUNT"), java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("TYPE"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("TIME"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("CONFIRMS"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("FEE"), java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("TRANSACTIONID")};
                tableModelt = new DefaultTableModel(rowData, columnNames);

                //get transactionIds
                String transactionIdsAPI = "nxt?requestType=getAccountTransactionIds&timestamp=0&account=";
                transactionIdsAPI += URLEncoder.encode(accountnumber, "UTF-8");
                String str = urlExec(globalURL,transactionIdsAPI);
                final List<String> transactions = new ArrayList<String>();
                JSONParser parser3 = new JSONParser();
                Object obj3 = null;
                try {
                    obj3 = parser3.parse(str);
                } catch (ParseException ex) {
                    Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                }
                JSONObject jsonObject3 = (JSONObject) obj3;
                JSONArray transactionIdss = null;
                int length1 = 0;
                int totalDataRead1 = 0;
                jProgressBar1.setValue(0);
                jProgressBar1.setStringPainted(true);
                if (jsonObject3 != null){
                    transactionIdss = (JSONArray) jsonObject3.get("transactionIds");
                    if (transactionIdss != null){
                        Iterator<String> iterator = transactionIdss.iterator();
                        length1 = transactionIdss.size();
                        while (iterator.hasNext()) {
                            transactions.add(iterator.next());
                            totalDataRead1=totalDataRead1+1;
                            float Percent=(totalDataRead1*100)/length1;
                            jProgressBar1.setValue((int)Percent);
                        }
                    }
                    //getTransactions(transactions);
                    Timestamp later = Timestamp.valueOf("2013-11-24 13:00:00");
                    try {
                        String ttype="";
                        int length = transactions.size();
                        int totalDataRead = 0;
                        jProgressBar1.setValue(0);
                        jProgressBar1.setStringPainted(true);
                        for (int j=0; j<length; j++){
                            String transactionID = transactions.get(j);
                            String transactionsAPI = "nxt?requestType=getTransaction&transaction=";
                            transactionsAPI += URLEncoder.encode(transactionID, "UTF-8");
                            String str2 = null;
                            try {
                                str2 = urlExec(globalURL, transactionsAPI);
                            } catch (IOException ex) {
                                Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            JSONParser parser = new JSONParser();
                            Object obj = null;
                            try {
                                obj = parser.parse(str2);
                            } catch (ParseException ex) {
                                Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            JSONObject jsonObject = (JSONObject) obj;
                            if (jsonObject != null){
                                String senderId = (String) jsonObject.get("sender");
                                Long fees = (Long) jsonObject.get("fee");
                                Long amount = (Long) jsonObject.get("amount");
                                Long timestamp = (Long) jsonObject.get("timestamp");
                                Long confirmation = (Long) jsonObject.get("confirmations");
                                String receiver = (String) jsonObject.get("recipient");

                                TYPE = (Long) jsonObject.get("type");
                                SUBTYPE = (Long) jsonObject.get("subtype");

                                Date sdf = null;
                                try {
                                    sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2013-11-24 13:00:00");
                                } catch (java.text.ParseException ex) {
                                    Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                later = new Timestamp(timestamp * 1000 + sdf.getTime());

                                if (TYPE == 0){
                                    if (SUBTYPE == 0)
                                        ttype = java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("ORDINARY PAYMENT");
                                    else
                                        ttype = "";
                                }
                                if (TYPE == 1){
                                    if (SUBTYPE == 0)
                                        ttype = java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("ARBITRARY MESSAGE");
                                    else
                                        ttype = java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("ALIAS ASSIGNMENT");
                                }
                                if (TYPE == 2){
                                    switch (SUBTYPE.intValue()){
                                        case 0:                                                 
                                            ttype = java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("ASSET ISSUANCE");
                                            break;
                                        case 1:
                                            ttype = java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("ASSET TRANSFER");
                                            break;
                                        case 2:
                                            ttype = java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("ASK ORDER PLACEMENT");
                                            break;
                                        case 3:
                                            ttype = java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("BID ORDER PLACEMENT");
                                            break;
                                        case 4:
                                            ttype = java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("ASK ORDER CANCELLATION");
                                            break;
                                        case 5:
                                            ttype = java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("BID ORDER CANCELLATION");
                                            break;
                                    }
                                }                          
                                totalDataRead=totalDataRead+1;
                                float Percent=(totalDataRead*100)/length;
                                jProgressBar1.setValue((int)Percent);
                                tableModelt.addRow(new Object[]{senderId, receiver, amount, ttype, later.toString(), confirmation, fees, transactionID});    
                                publish(tableModelt);
                            }
                        }
                    }catch (UnsupportedEncodingException ex) {
                        Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                //------
                return tableModelt;
            }
            @Override
            protected void done() {
                try {
                    TableModel model = get(10, TimeUnit.SECONDS);
                    if (model != null)
                        transactionsTable.setModel(model);
                } catch (InterruptedException el) {
                    LogGui("WARNING! InterruptedException "+el.getMessage());
                } catch (ExecutionException ex) {
                    LogGui("WARNING! ExecutionException "+ex.getMessage());
                } catch (TimeoutException et) {
                    LogGui("WARNING! TimeoutException "+et.getMessage());
                }
            }
            @Override
            protected void process(List<TableModel> chunks) {
                transactionsTable.setModel(chunks.get(chunks.size()-1));
            }
        };
        getTransactionsWork.execute();
    }
    DefaultTableModel tableModelAlias;
    private void getAlias(String accountN) throws IOException{
        
        Object[][] rowData = {};
        Object[] columnNames = {java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("ALIAS"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("URI")};
        tableModelAlias = new DefaultTableModel(rowData, columnNames);
        aliasTable.setModel(tableModelAlias);
        
        String aliasAPI = "nxt?requestType=listAccountAliases&account=";
        aliasAPI += URLEncoder.encode(accountN, "UTF-8");
        String str = urlExec(globalURL,aliasAPI);
        
        JSONParser parser = new JSONParser();
        Object obj = null;
        try {
            obj = parser.parse(str);
        } catch (ParseException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        JSONObject jsonObject = (JSONObject) obj;
        
        if (jsonObject != null){
            JSONArray newjsonObject = (JSONArray) jsonObject.get("aliases");
            if (newjsonObject != null){
                Iterator<String> iterator = newjsonObject.iterator();
                while (iterator.hasNext()) {
                    Object inarr = iterator.next();
                    JSONObject injsonObject = (JSONObject) inarr;
                    String alias = (String) injsonObject.get("alias");
                    String uri = (String) injsonObject.get("uri");
                    tableModelAlias.addRow(new Object[]{alias, uri});
                }
            }
        }
    }

    private void consoleMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_consoleMenuActionPerformed
            TransactionsPanel.setVisible(false);
            ConsolePanel.setVisible(true);
            SendPanel.setVisible(false);
            ReceivePanel.setVisible(false);
            AliasesPanel.setVisible(false);
            MessagesPanel.setVisible(false);
            BlocksPanel.setVisible(false);
            OverviewPanel.setVisible(false);
            consoleButton.setSelected(true);
            AssetExPanel.setVisible(false);
            VotingPanel.setVisible(false);
    }//GEN-LAST:event_consoleMenuActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        if (flag==1){
            nxt.Nxt.shutdown();
            Db.shutdown();
            flag = 0;
        }
    //p.destroy();
    }//GEN-LAST:event_formWindowClosing

    private void receiveMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_receiveMenuActionPerformed
            unlockButton.setSelected(true);
            TransactionsPanel.setVisible(false);
            ConsolePanel.setVisible(false);
            SendPanel.setVisible(false);
            ReceivePanel.setVisible(true);
            AliasesPanel.setVisible(false);
            MessagesPanel.setVisible(false);
            BlocksPanel.setVisible(false);
            OverviewPanel.setVisible(false);
            AssetExPanel.setVisible(false);
            VotingPanel.setVisible(false);
    }//GEN-LAST:event_receiveMenuActionPerformed
    private String sapId;
    private String sapBl;
    private void accountPassFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_accountPassFieldActionPerformed
        passPhrases = accountPassField.getText();
        
        if (consoleOutput.getText().trim().length() == 0){
            JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("PLEASE, START THE SERVER!"));
        }
        else{
            Long balls;
            //JCTable accountsA = new JCTable("lib/accounts");
            tableModelA = (AccountTableModel) AccountsTable.getModel();//new AccountTableModel();
            AccountsTable.setModel(tableModelA);
            try {
                if (accountPassField.getText().length()>0){
                    String secretPhrase = accountPassField.getText();
                    //API call and extract data
                    String unlockAPI = "nxt?requestType=getAccountId&secretPhrase="; 
                    unlockAPI += URLEncoder.encode(secretPhrase, "UTF-8");
                    String str = urlExec(globalURL,unlockAPI);

                    String accountID;
                    JSONParser parser = new JSONParser();
                    Object obj = null;
                    try {
                        obj = parser.parse(str);
                    } catch (ParseException ex) {
                        Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    JSONObject jsonObject = (JSONObject) obj;
                    accountID = (String) jsonObject.get("accountId");

                    //if result exist message account is already listed, otherwise insert new data
                    //JCRow[] result = accountsA.select("Account_Number", "=", accountID);
                    Vector row = readRecord(accountID);
                    if (row.size()>0){
                        JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("ACCOUNT IS ALREADY LISTED!"));        
                    }
                    else {
                        balls = getBalance(accountID);
                        /*JCRow newRow = new JCRow(accountsA);
                        newRow.setString("Label", labelofAccount.getText());
                        newRow.setString("Account_Number", accountID);
                        newRow.setString("Balance", balls.toString());
                        newRow.setString("Flag", "1");
                        accountsA.insert(newRow);
                        */
                        if (encryptMyAccountCheckBox.isSelected()){
                            masterFlag = 3;      
                            sapId = accountID;
                            sapBl = balls.toString();
                            masterPasswordDialog.setSize(310, 75);
                            masterPasswordDialog.setLocationRelativeTo(null);
                            masterPasswordDialog.setTitle(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("ENTER MASTERPASSWORD!"));
                            masterPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
                            masterPasswordDialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
                            masterPasswordDialog.setVisible(true);
                        }else{
                            //String enc = DeEncrypter.getInstance(prefs.get(PREF_masterKey, null).getBytes()).encrypt("secretPhrase is NOT SAVED!");
                            String enc = "";//aes.encrypt("secretPhrase is NOT SAVED!", prefs.get(PREF_masterKey, null));
                            insertRecordIntoDbAccountsTable(labelofAccount.getText(), accountID, balls.toString(), enc, "1");
                        }
                        final nxt.Generator check;
                        check = nxt.Generator.startForging(secretPhrase);
                        Generators.add(check);
                        //mMap.put(tableModelA.getRowCount(), check);
                        int ded;
                        nxt.Generator.addListener(new nxt.util.Listener<nxt.Generator>() {
                            @Override
                            public void notify(nxt.Generator t) {
                                for (int i=0; i<Generators.size(); i++){
                                        if (Generators.get(i) == t){
                                            tableModelA.updateAccount(null,null,null,(int) t.getDeadline(), i);
                                        }
                                }
                            }
                        }, nxt.Generator.Event.GENERATION_DEADLINE);

                        if (check != null){
                            ded = (int)check.getDeadline();
                        }else
                            ded = 0;
                        tableModelA.addAccount(new AccountGUI(labelofAccount.getText(), accountID, balls.toString(), ded));
                    }
                }
            } 
            catch (Exception ex) {
                Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
            }
            accountPassField.setText("");
            //watchAccountNumberField.setText("");
            labelofAccount.setText("");
            UnlockDialog.dispose();
        }
    }//GEN-LAST:event_accountPassFieldActionPerformed

    private void sendNxtButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendNxtButtonActionPerformed
        Object[][] rowDataMsg = {};
        Object[] columnNamesMsg = {java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("ACCOUNT"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("BALANCE"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("PUBKEY"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("NOTE")};
        DefaultTableModel checktablemod = new DefaultTableModel(rowDataMsg,columnNamesMsg);
        accountCheckTable.setModel(checktablemod);
        if (receiverAddressField.getText().endsWith(":")){
            StringTokenizer tokens = new StringTokenizer(receiverAddressField.getText().substring(0, receiverAddressField.getText().length()-1), ":");
            while (tokens.hasMoreTokens()){
                String accountN = tokens.nextToken().toString();
                Long balanceN = null;
                try {
                    balanceN = getBalance(accountN);
                    
                } catch (IOException ex) {
                    Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                }
                String pubkeyN = null;
                try {
                    pubkeyN = getPubKey(accountN);

                } catch (UnsupportedEncodingException ex) {
                    Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                }
                String Note = "";
                if (pubkeyN.equals(""))
                    Note += java.util.ResourceBundle.getBundle("clienxt/Bundle").getString(" WARNING PUBKEY. ");
                else
                    Note += java.util.ResourceBundle.getBundle("clienxt/Bundle").getString(" OK PUBKEY. ");
                if (balanceN < 1)
                    Note += java.util.ResourceBundle.getBundle("clienxt/Bundle").getString(" ZERO BALANCE. ");
                else
                    Note += java.util.ResourceBundle.getBundle("clienxt/Bundle").getString(" OK BALANCE. ");
                    
                checktablemod.addRow(new Object[]{accountN, balanceN.toString(), pubkeyN, Note});                
            }
        }else {
            Long balanceNS = null;
            String accountNS = receiverAddressField.getText();
                try {
                    balanceNS = getBalance(accountNS);
                } catch (IOException ex) {
                    Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                }
                String pubkeyNS = null;
                try {
                    pubkeyNS = getPubKey(accountNS);

                } catch (UnsupportedEncodingException ex) {
                    Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                }
                String NoteS = "";
                if (pubkeyNS == null)
                    NoteS += java.util.ResourceBundle.getBundle("clienxt/Bundle").getString(" WARNING PUBKEY. ");
                else
                    NoteS += java.util.ResourceBundle.getBundle("clienxt/Bundle").getString(" OK PUBKEY. ");
                if (balanceNS < 1)
                    NoteS += java.util.ResourceBundle.getBundle("clienxt/Bundle").getString(" ZERO BALANCE. ");
                else
                    NoteS += java.util.ResourceBundle.getBundle("clienxt/Bundle").getString(" OK BALANCE. ");

                checktablemod.addRow(new Object[]{accountNS, balanceNS.toString(), pubkeyNS, NoteS});                
        }
            SentDialog.setSize(395,220);
            SentDialog.setLocationRelativeTo(null);
            SentDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            SentDialog.setTitle(java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("SENDING..."));
            SentDialog.setVisible(true);
    }//GEN-LAST:event_sendNxtButtonActionPerformed

    private void preferencesMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_preferencesMenuItemActionPerformed
        preferencesFrame.setSize(580, 380);
        preferencesFrame.setLocationRelativeTo(null);
        preferencesFrame.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        preferencesFrame.setVisible(true);
    }//GEN-LAST:event_preferencesMenuItemActionPerformed
    // This will define a node in which the preferences can be stored
    private final Preferences prefs = Preferences.userRoot().node(this.getClass().getName());
    private final String PREF_fee = "fee";
    private final String PREF_deadline = "deadline";
    private final String PREF_account = "account";
    private final String PREF_language = "language";
    private final String PREF_server = "server";
    private final String PREF_version = "version";
    private final String PREF_masterKey = "";
    private final String PREF_proxyButton = "index";
    private final String PREF_proxyManualHost = "host";
    private final String PREF_proxyManualPort = "port";
    private final String PREF_network = "network";
    
    private final char[] HEX_CHARS = "0123456789abcdef".toCharArray();
    private final String hexStringConverter = null;
 
    public String stringToHex(String input) throws UnsupportedEncodingException
    {
        if (input == null) throw new NullPointerException();
        return asHex(input.getBytes());
    }
 
    public String hexToString(String txtInHex)
    {
        byte [] txtInByte = new byte [txtInHex.length() / 2];
        int j = 0;
        for (int i = 0; i < txtInHex.length(); i += 2)
        {
                txtInByte[j++] = Byte.parseByte(txtInHex.substring(i, i + 2), 16);
        }
        return new String(txtInByte);
    }
 
    private String asHex(byte[] buf)
    {
        char[] chars = new char[2 * buf.length];
        for (int i = 0; i < buf.length; ++i)
        {
            chars[2 * i] = HEX_CHARS[(buf[i] & 0xF0) >>> 4];
            chars[2 * i + 1] = HEX_CHARS[buf[i] & 0x0F];
        }
        return new String(chars);
    }
    public String stringToCurve(String senderSP, String recieverAN, String text){
        /*byte[] mysecret = SHA256.getHash(senderSP.getBytes(), true);
        mysecret[0] &= 248;
        mysecret[31] &= 127;
        mysecret[31] |= 64;
        byte[] peerkey = null;
        try {
            peerkey = getPubKey(recieverAN).getBytes();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        byte[] shared_secret = null;
        Curve25519.curve(shared_secret, mysecret, peerkey);
        byte[] seed = SHA256.getHash(shared_secret, true);
        //seed = ~seed & 0xff;
        //seed = text.getBytes()^seed;
        
        return seed.toString();*/
        //nxt.crypto.Crypto.sign("hello world".getBytes(), passPhrases);
        
        
        
        return "";
    }    
    private void aboutClienxtMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aboutClienxtMenuItemActionPerformed
        aboutClieNXTdialog.setSize(440, 285);
        aboutClieNXTdialog.setLocationRelativeTo(null);
        aboutClieNXTdialog.setVisible(true);
        aboutClieNXTdialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        aboutClieNXTdialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    }//GEN-LAST:event_aboutClienxtMenuItemActionPerformed

    private void attachMessageCheckBoxStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_attachMessageCheckBoxStateChanged
        
    }//GEN-LAST:event_attachMessageCheckBoxStateChanged

    private void AddAccountButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AddAccountButtonActionPerformed
        UnlockDialog.setSize(380, 150);
        UnlockDialog.setLocationRelativeTo(null);
        UnlockDialog.setVisible(true);
        UnlockDialog.setTitle(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("ADD ACCOUNT WITH SECRETPHRASE!"));
        UnlockDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        UnlockDialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    }//GEN-LAST:event_AddAccountButtonActionPerformed
 
    private void sendButtonStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_sendButtonStateChanged
    }//GEN-LAST:event_sendButtonStateChanged

    private void consoleButtonStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_consoleButtonStateChanged
    }//GEN-LAST:event_consoleButtonStateChanged

    private void transactionsButtonStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_transactionsButtonStateChanged
    }//GEN-LAST:event_transactionsButtonStateChanged

    private void aliasButtonStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_aliasButtonStateChanged
    }//GEN-LAST:event_aliasButtonStateChanged

    private void sendButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendButtonActionPerformed
        // snd
        SendPanel.setVisible(true);
        VotingPanel.setVisible(false);
        AssetExPanel.setVisible(false);
        jScrollPane3.setEnabled(false);
        inputMessageField.setEnabled(false);
        
        attachMessageCheckBox.setSelected(false);
        ConsolePanel.setVisible(false);
        TransactionsPanel.setVisible(false);
        ReceivePanel.setVisible(false);
        AliasesPanel.setVisible(false);
        MessagesPanel.setVisible(false);
        BlocksPanel.setVisible(false);
        OverviewPanel.setVisible(false);
        if (SendPanel.isVisible()){
            //JCTable accountsA = new JCTable("lib/accounts");
            senderAddressComboBox.removeAllItems();
            //JCRow[] acclist = accountsA.select("Flag","=", "1");
            Vector acc = readAccounts("1");
            for (int i=0; i<acc.size(); i++){
                senderAddressComboBox.addItem(acc.get(i).toString());
            }
        }
    }//GEN-LAST:event_sendButtonActionPerformed

    private void RemoveAccountButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RemoveAccountButtonActionPerformed
        String removeAcc = (String) AccountsTable.getValueAt(AccountsTable.getSelectedRow(), 1).toString();
        //JCTable accounts = new JCTable("lib/accounts");
//        Object[][] rowData = {};
//        Object[] columnNames = {java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("LABEL"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("ACCOUNT_NUMBER"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("BALANCE")};
        //consoleOutput.append(removeAcc);
        Generators.remove(AccountsTable.getSelectedRow());
        //accounts.delete("Account_Number", "=", removeAcc);
        deleteAccountsFromDb(removeAcc);
        clearing = true;
        tableModelA.clearAccount(AccountsTable.getSelectedRow());
        clearing = false;
        selectedAccount = null;

        
    }//GEN-LAST:event_RemoveAccountButtonActionPerformed

    private void transactionsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_transactionsButtonActionPerformed
        //txcombo
        //if (transactionsButton.isSelected()){
            TransactionsPanel.setVisible(true);
            VotingPanel.setVisible(false);
            AssetExPanel.setVisible(false);
            ConsolePanel.setVisible(false);
            SendPanel.setVisible(false);
            ReceivePanel.setVisible(false);
            AliasesPanel.setVisible(false);
            MessagesPanel.setVisible(false);
            BlocksPanel.setVisible(false);
            OverviewPanel.setVisible(false);
            if (TransactionsPanel.isVisible()){
                //JCTable accountsA = new JCTable("lib/accounts");
                TXaccountListCombobox.removeAllItems();
                //JCRow[] acclist = accountsA.select();
                Vector acc = readAccounts(null);
                for (int i=0; i<acc.size(); i++) {
                    TXaccountListCombobox.addItem(acc.get(i).toString());
                }
            }
            
        //}
    }//GEN-LAST:event_transactionsButtonActionPerformed

    private void TXaccountListComboboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TXaccountListComboboxActionPerformed
        if (TransactionsPanel.isVisible()){
            try {
                if (consoleOutput.getText().trim().length() != 0){
                    if (TXaccountListCombobox.getItemCount() > 0){
                        getTransactionIds(TXaccountListCombobox.getSelectedItem().toString());
                    }
                }
            } catch (IOException ex) {
                LogGui("WARNING! IOException "+ex.getMessage());
                //Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_TXaccountListComboboxActionPerformed

    private void attachMessageCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_attachMessageCheckBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_attachMessageCheckBoxActionPerformed

    private void aliasButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aliasButtonActionPerformed
        AliasesPanel.setVisible(true);
        VotingPanel.setVisible(false);
        AssetExPanel.setVisible(false);
        ConsolePanel.setVisible(false);
        SendPanel.setVisible(false);
        TransactionsPanel.setVisible(false);
        ReceivePanel.setVisible(false);
        OverviewPanel.setVisible(false);
        MessagesPanel.setVisible(false);
        BlocksPanel.setVisible(false); 
        if (AliasesPanel.isVisible()){
            ALaccountListComboBox.removeAllItems();
            //JCTable accountsA = new JCTable("lib/accounts");
            //JCRow[] acclist = accountsA.select(); //all accounts
            Vector acc = readAccounts(null);
            for (int i=0; i<acc.size(); i++){
                ALaccountListComboBox.addItem(acc.get(i).toString());
            }
        }
 
    }//GEN-LAST:event_aliasButtonActionPerformed

    private void overviewAssets(String accountId){
        Object[][] rowData = {};
        Object[] columnNames = {java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("ACCOUNT"), java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("ASSET NAME"), java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("QUANTITY")};
        DefaultTableModel tableoverviewModel = new DefaultTableModel(rowData, columnNames);
        overviewAssetTable.setModel(tableoverviewModel);
        
        String currentAssetAPI = "nxt?requestType=getAccount&account=";
        try {
            currentAssetAPI += URLEncoder.encode(accountId, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        String str = null;
        try {
            str = urlExec(globalURL,currentAssetAPI);
        } catch (IOException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        JSONParser parser = new JSONParser();
        Object obj = null;
        try {
            obj = parser.parse(str);
        } catch (ParseException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        JSONObject jsonObject = (JSONObject) obj;
        quantityAssetList.clear();
        if (jsonObject != null){
            JSONArray newjsonObject = (JSONArray) jsonObject.get("assetBalances");
            if (newjsonObject != null){
                Iterator<String> iterator = newjsonObject.iterator();
                while (iterator.hasNext()) {
                    Object inarr = iterator.next();
                    JSONObject injsonObject = (JSONObject) inarr;
                    Long balance = (Long) injsonObject.get("balance");
                    String asset = (String) injsonObject.get("asset");
                    quantityAssetList.add(balance.toString());
                    nameAssetComboBox.addItem(asset);
                    tableoverviewModel.addRow(new Object[]{accountId, asset, balance});
                }
            }else
                nameAssetComboBox.addItem(java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("NO ASSETS AVAILABLE!"));
        }
    }
    
    
    private void overviewButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_overviewButtonActionPerformed
        VotingPanel.setVisible(false);
        AssetExPanel.setVisible(false);        
        AliasesPanel.setVisible(false);
        ConsolePanel.setVisible(false);
        SendPanel.setVisible(false);
        TransactionsPanel.setVisible(false);
        ReceivePanel.setVisible(false);
        OverviewPanel.setVisible(true);
        MessagesPanel.setVisible(false);
        BlocksPanel.setVisible(false);
        
        SwingWorker<TableModel, TableModel> worker = new SwingWorker<TableModel, TableModel>() {

            @Override
            protected TableModel doInBackground() throws Exception {
                Object[][] rowData = {};
                Object[] columnNames = {java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("ACCOUNT"), java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("ASSET NAME"), java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("QUANTITY")};
                DefaultTableModel tableoverviewModel = new DefaultTableModel(rowData, columnNames);
                //JCTable accountsA = new JCTable("lib/accounts");
                double addnxt = 0.0;
                String allnxt;
                //JCRow[] acclist = accountsA.select("Flag","=", "1");
                Vector acc = readAccounts("1");
                for (int i=0; i<acc.size(); i++) {
                    //allnxt = acclist1.getString("Balance");
                    //addnxt += Double.parseDouble(allnxt);
                    String accountId = acc.get(i).toString();//acclist1.getString("Account_Number");
                    Vector dat = readRecord(accountId);
                    Vector ballc = (Vector) dat.get(0);
                    allnxt = (String) ballc.get(2);
                    addnxt += Double.parseDouble(allnxt);
                    //String accountLabel = acclist1.getString("Label");
                    String currentAssetAPI = "nxt?requestType=getAccount&account=";
                    try {
                        currentAssetAPI += URLEncoder.encode(accountId, "UTF-8");
                    } catch (UnsupportedEncodingException ex) {
                        Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    String str = null;
                    try {
                        str = urlExec(globalURL,currentAssetAPI);
                    } catch (IOException ex) {
                        LogGui("WARNING! IOException "+ex.getMessage());
                    }
                    JSONParser parser = new JSONParser();
                    Object obj = null;
                    if (str != null){
                    try {
                        obj = parser.parse(str);
                    } catch (ParseException ex) {
                        LogGui("WARNING! ParseException "+ex.getMessage());
                    }
                    JSONObject jsonObject = (JSONObject) obj;
                    jProgressBar1.setValue(0);
                    jProgressBar1.setStringPainted(true);
                    int length=0;
                    int total=0;
                    if (jsonObject != null){
                        JSONArray newjsonObject = (JSONArray) jsonObject.get("assetBalances");
                        if (newjsonObject != null){
                            Iterator<String> iterator = newjsonObject.iterator();
                            length = newjsonObject.size();
                            while (iterator.hasNext()) {
                                Object inarr = iterator.next();
                                JSONObject injsonObject = (JSONObject) inarr;
                                Long balance = (Long) injsonObject.get("balance");
                                String asset = (String) injsonObject.get("asset");
                                String[] data = getAsset(asset);
                                tableoverviewModel.addRow(new Object[]{accountId, data[1], balance});
                                publish(tableoverviewModel);
                                total=total+1;
                                float Percent=(total*100)/length;
                                jProgressBar1.setValue((int)Percent);
                            }
                        }
                    }
                }
                }
                totalBalanceLabelNXT.setText(String.valueOf(addnxt));
                String nxtbtc = "http://www.cryptocoincharts.info/v2/api/tradingPair/";
                String latestN = null;
                try {
                    latestN = urlExec(nxtbtc,"nxt_btc");
                } catch (IOException ex) {
                    LogGui("WARNING! IOException "+ex.getMessage());
                }
                JSONParser parser2 = new JSONParser();
                Object obj2 = null;
                if (latestN != null){
                try {
                    obj2 = parser2.parse(latestN);
                } catch (ParseException ex) {
                    LogGui("WARNING! ParseException "+ex.getMessage());
                }
                JSONObject jsonObject2 = (JSONObject) obj2;
                if (jsonObject2 != null){
                    String price = (String) jsonObject2.get("price");
                    NxtBtcLabel.setText(price);
                }
                String btcusd = "http://www.cryptocoincharts.info/v2/api/tradingPair/";
                String latestB = null;
                try {
                    latestB = urlExec(btcusd, "btc_usd");
                } catch (IOException ex) {
                    LogGui("WARNING! IOException "+ex.getMessage());
                }
                JSONParser parser3 = new JSONParser();
                Object obj3 = null;
                try {
                    obj3 = parser3.parse(latestB);
                } catch (ParseException ex) {
                    LogGui("WARNING! ParseException "+ex.getMessage());
                }
                JSONObject jsonObject3 = (JSONObject) obj3;
                if (jsonObject3 != null){
                    String price2 = (String) jsonObject3.get("price");
                    double p = Double.parseDouble(price2)*Double.parseDouble(NxtBtcLabel.getText());
                    NxtUsdLabel.setText(String.valueOf(p));
                }
                if (NxtBtcLabel.getText().isEmpty())
                    NxtBtcLabel.setText("0");
                if (NxtUsdLabel.getText().isEmpty())
                    NxtUsdLabel.setText("0");
                totalBalanceLabelBTC.setText(String.valueOf(addnxt*Double.parseDouble(NxtBtcLabel.getText())));
                totalBalanceLabelUSD.setText(String.valueOf(addnxt*Double.parseDouble(NxtUsdLabel.getText())));
                }
                return tableoverviewModel;
            }
            @Override
            protected void done() {
                TableModel model = null;
                try {
                    model = get();
                } catch (InterruptedException ex) {
                    LogGui("WARNING! InterruptedException "+ex.getMessage());
                } catch (ExecutionException ex) {
                    LogGui("WARNING! ExecutionException "+ex.getMessage());
                }
                if (model != null)
                    overviewAssetTable.setModel(model);
            }
            @Override
            protected void process(List<TableModel> chunks) {
                overviewAssetTable.setModel(chunks.get(chunks.size()-1));
            }
        };
        worker.execute(); 
    }//GEN-LAST:event_overviewButtonActionPerformed

    private void senderAddressComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_senderAddressComboBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_senderAddressComboBoxActionPerformed

    private void consoleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_consoleButtonActionPerformed
        ConsolePanel.setVisible(true);
        VotingPanel.setVisible(false);
        AssetExPanel.setVisible(false);
        SendPanel.setVisible(false);
        TransactionsPanel.setVisible(false);
        ReceivePanel.setVisible(false);
        AliasesPanel.setVisible(false);
        OverviewPanel.setVisible(false);
        MessagesPanel.setVisible(false);
        BlocksPanel.setVisible(false);
          
    }//GEN-LAST:event_consoleButtonActionPerformed

    private void transactionsMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_transactionsMenuActionPerformed
        transactionsButton.setSelected(true);
        TransactionsPanel.setVisible(true);
        ConsolePanel.setVisible(false);
        SendPanel.setVisible(false);
        ReceivePanel.setVisible(false);
        AliasesPanel.setVisible(false);
        MessagesPanel.setVisible(false);
        BlocksPanel.setVisible(false);
        OverviewPanel.setVisible(false);
        AssetExPanel.setVisible(false);
        VotingPanel.setVisible(false);
    }//GEN-LAST:event_transactionsMenuActionPerformed

    private void aliasMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aliasMenuActionPerformed
        TransactionsPanel.setVisible(false);
        ConsolePanel.setVisible(false);
        SendPanel.setVisible(false);
        ReceivePanel.setVisible(false);
        AliasesPanel.setVisible(true);
        aliasButton.setSelected(true);
        MessagesPanel.setVisible(false);
        BlocksPanel.setVisible(false);
        OverviewPanel.setVisible(false);
        AssetExPanel.setVisible(false);
        VotingPanel.setVisible(false);
    }//GEN-LAST:event_aliasMenuActionPerformed

    private void defaultDeadlineActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_defaultDeadlineActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_defaultDeadlineActionPerformed

    private void unlockButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_unlockButtonActionPerformed
        ReceivePanel.setVisible(true);
        VotingPanel.setVisible(false);
        AssetExPanel.setVisible(false);
        MessagesPanel.setVisible(false);
        BlocksPanel.setVisible(false);
        ConsolePanel.setVisible(false);
        TransactionsPanel.setVisible(false);
        SendPanel.setVisible(false);
        AliasesPanel.setVisible(false);
        OverviewPanel.setVisible(false);
        if (prefs.get(PREF_masterKey, null) == null){
            deleteDbRecords();
        }
        if (AccountsTable.getRowCount()<1){
            readTable();
        }
        if (AccountsTable.getRowCount()==0){
           if (prefs.get(PREF_masterKey, null) == null){
               JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("MASTER PASSWORD IS NOT SET, PLEASE SET"));
               masterFlag = -1;
               masterPasswordDialog.setSize(310, 75);
               masterPasswordDialog.setLocationRelativeTo(null);
               masterPasswordDialog.setTitle(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("MASTER PASSWORD IS NOT SET, PLEASE SET!"));
               masterPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
               masterPasswordDialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
               masterPasswordDialog.setVisible(true);
           }else{
               JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("MASTER PASSWORD IS SET, BUT COULD NOT READ THE TABLE!"));
           }
        }
    }//GEN-LAST:event_unlockButtonActionPerformed
  
    private String getSHA256(String text){
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        md.update(text.getBytes());
 
        byte byteData[] = md.digest();
 
        //convert the byte to hex format method 1
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
         sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }
    private void ALaccountListComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ALaccountListComboBoxActionPerformed
        try {
            if (ALaccountListComboBox.getItemCount() > 1)
                getAlias(ALaccountListComboBox.getSelectedItem().toString());
        } catch (IOException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_ALaccountListComboBoxActionPerformed

    private void assignAliasButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_assignAliasButtonActionPerformed
        outputL.setText("");
        try {
            String aliasAPI = "nxt?requestType=assignAlias&secretPhrase=";
            aliasAPI += URLEncoder.encode(aliasAccountPassphrase.getText(), "UTF-8");
            aliasAPI += "&alias=";
            aliasAPI += URLEncoder.encode(aliasField.getText(), "UTF-8");
            aliasAPI += "&uri=";
            aliasAPI += URLEncoder.encode(uriField.getText(), "UTF-8");
            aliasAPI += "&fee=";
            aliasAPI += URLEncoder.encode(defaultFee.getText(), "UTF-8");
            aliasAPI += "&deadline=";
            aliasAPI += URLEncoder.encode(defaultDeadline.getText(), "UTF-8");

            String strAl = null;
            try {
                strAl = urlExec(globalURL,aliasAPI);
            } catch (IOException ex) {
                Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
            }
            LogGui(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("ALIAS ASSIGNED: ")+strAl);
            statusbarLabel.setText(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("ALIAS ASSIGNED"));
        } catch (UnsupportedEncodingException ex) {
            LogGui("UnsupportedEncodingException, task is failed!");
        }
    }//GEN-LAST:event_assignAliasButtonActionPerformed

    private void aliasTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_aliasTableMouseClicked
        int indx = aliasTable.getSelectedRowCount();
        if (indx>0){
            aliasField.setText(aliasTable.getValueAt(aliasTable.getSelectedRow(), 0).toString());
            uriField.setText(aliasTable.getValueAt(aliasTable.getSelectedRow(), 1).toString());
        }
    }//GEN-LAST:event_aliasTableMouseClicked

    private void AliasesPanelPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_AliasesPanelPropertyChange
        
    }//GEN-LAST:event_AliasesPanelPropertyChange

    private void SendPanelPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_SendPanelPropertyChange
        
    }//GEN-LAST:event_SendPanelPropertyChange

    private void TransactionsPanelPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_TransactionsPanelPropertyChange
        
    }//GEN-LAST:event_TransactionsPanelPropertyChange

    private void updatePrefsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updatePrefsButtonActionPerformed
        prefs.putInt(PREF_fee, Integer.parseInt(defaultFee.getText()));
        
        if (networkCheckBox.isSelected()){
            prefs.put(PREF_network, "1");
            prefs.put(PREF_server, "localhost:6876");
            LogGui(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("NETWORK SET TO TESTNET, CHANGES WILL TAKE EFFECT AFTER RE-START!"));
        }else{
            prefs.put(PREF_network, "0");
            prefs.put(PREF_server, "localhost:7876");
            LogGui(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("NETWORK SET TO MAINNET, CHANGES WILL TAKE EFFECT AFTER RE-START!"));
        }
                
        prefs.putInt(PREF_deadline, Integer.parseInt(defaultDeadline.getText()));
        if (defaultAccount.getItemCount()<1)
            prefs.put(PREF_account, "");
        else
            prefs.put(PREF_account, defaultAccount.getSelectedItem().toString());
        if (masterPasswordField.getText().length()>0){
            
            if (prefs.get(PREF_masterKey, null) != null){
                JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("YOU HAVE ALREADY SET MASTER PASSWORD! \n IN ORDER TO CHANGE MASTER PASSWORD PLEASE FILL CHANGE SECTION!"));    
            }else{
                masterKey = getSHA256(masterPasswordField.getText());
                prefs.put(PREF_masterKey, masterKey);
            }
        }
        if (changeMasterPasswordField.getText().length()>0){
            if (prefs.get(PREF_masterKey, null) != null){
                if (masterKey.equals(getSHA256(changeMasterPasswordField.getText()))){
                    if (!newMasterPasswordField.getText().equals(confirmMasterPasswordField.getText()))
                        JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("CONFIRMATION DOES NOT MATCH!"));
                    else{
                        prefs.put(PREF_masterKey, getSHA256(newMasterPasswordField.getText()));
                    }
                }else
                    JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("CURRENT PASSWORD DOES NOT MATCH!"));
            }else
                JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("MASTER PASSWORD IS NOT SET!"));
        }
        if(proxyNoRbox.isSelected() == true){
            LogGui(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("CONNECTIONS: NO PROXY"));
            prefs.put(PREF_proxyButton, "0");
        }
        if(proxyAutoRBox.isSelected() == true){
            System.setProperty("java.net.useSystemProxies", "true");
            List l = null;
            try {
                l = ProxySelector.getDefault().select(new URI("http://nxtcrypto.org"));
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            if (l != null) {
                for (Iterator iter = l.iterator(); iter.hasNext();) {
                    java.net.Proxy proxy;
                    proxy = (java.net.Proxy) iter.next();
                    InetSocketAddress addr = (InetSocketAddress) proxy.address();
                    if (addr == null) {
                        LogGui(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("NO PROXY DETECTED"));
                    } else {
                        LogGui(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("CONNECTIONS: DETECTING PROXIES"));
                        LogGui("proxy hostname : " + addr.getHostName());
                        System.setProperty("http.proxyHost", addr.getHostName());
                        LogGui("proxy port : " + addr.getPort());
                        System.setProperty("http.proxyPort", Integer.toString(addr.getPort()));
                    }
                }
            }
            prefs.put(PREF_proxyButton, "1");
        }
        if(proxyManRBox.isSelected() == true){
            if (proxyHostField.getText().equals("")) {
                LogGui(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("PLEASE WRITE HOST ADDRESS!"));
            } else {
                LogGui(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("CONNECTIONS: SETTING PROXY..."));
                LogGui("proxy hostname : " + proxyHostField.getText());
                System.setProperty("http.proxyHost", proxyHostField.getText());
                LogGui("proxy port : " + proxyPortField.getText());
                System.setProperty("http.proxyPort", proxyPortField.getText());
                System.setProperty("http.proxyUser", proxyUserField.getText());
                LogGui("proxy User : " + proxyUserField.getText());
                System.setProperty("http.proxyPassword", proxyPasswordField.getText());
                LogGui("proxy Password : xxxx");
            }
            prefs.put(PREF_proxyButton, "2");
            prefs.put(PREF_proxyManualHost, proxyHostField.getText());
            prefs.put(PREF_proxyManualPort, proxyPortField.getText());
        }
        
        
        
        preferencesFrame.dispose();
    }//GEN-LAST:event_updatePrefsButtonActionPerformed

    private void OKbuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OKbuttonActionPerformed
        String sendAPI = null;
        String sendMsgAPI = null;
        String strNxt = null;
        String strMsg = null;
        Long defaccb = null;
        String Tx = null;
        if ((amountNxtField.getText().endsWith(":")) && receiverAddressField.getText().endsWith(":")){
            //bulk address
            if (!attachMessageCheckBox.isSelected()){
                StringTokenizer tokensR = new StringTokenizer(receiverAddressField.getText().substring(0, receiverAddressField.getText().length()-1), ":");
                StringTokenizer tokensA = new StringTokenizer(amountNxtField.getText().substring(0, amountNxtField.getText().length()-1), ":");                       
                //message is not selected
                while (tokensR.hasMoreTokens()){
                    try {
                        sendAPI = "nxt?requestType=sendMoney&recipient=";
                        String accountNR = tokensR.nextToken().toString();
                        sendAPI += URLEncoder.encode(accountNR, "UTF-8");
                        sendAPI += "&amount=";
                        String amountNA = tokensA.nextToken().toString();
                        sendAPI += URLEncoder.encode(amountNA, "UTF-8");
                        sendAPI += "&fee=";
                        sendAPI += URLEncoder.encode(defaultFee.getText(), "UTF-8");
                        sendAPI += "&deadline=";
                        sendAPI += URLEncoder.encode(defaultDeadline.getText(), "UTF-8");
                        sendAPI += "&secretPhrase=";
                        sendAPI += URLEncoder.encode(passphraseField.getText(), "UTF-8");
                        try {
                            defaccb = getBalance(senderAddressComboBox.getSelectedItem().toString());
                        } catch (IOException ex) {
                            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        if (defaccb > 1){
                            if (Float.parseFloat(amountNA) > defaccb){
                                JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("ACCOUNT BALANCE IS LOWER THAN AMOUNT ENTERED!"));
                            }else{
                                try {
                                    strNxt = urlExec(globalURL,sendAPI);
                                } catch (IOException ex) {
                                    Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                Date date = new Date();
                                String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(date);
                                consoleOutput.append("\n["+time+"]"+java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("SENT NXT :")+strNxt);
                                statusbarLabel.setText(java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("SENT NXT :").substring(0, java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("SENT NXT :").length()-1));
                            }
                        }else
                            JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("NOT ENOUGH FUND!"));
                    } catch (UnsupportedEncodingException ex) {
                        Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            else
            {
                StringTokenizer tokensMR = new StringTokenizer(receiverAddressField.getText().substring(0, receiverAddressField.getText().length()-1), ":");
                StringTokenizer tokensMA = new StringTokenizer(amountNxtField.getText().substring(0, amountNxtField.getText().length()-1), ":");                       
                StringTokenizer tokensMM = new StringTokenizer(inputMessageField.getText().substring(0, inputMessageField.getText().length()-1), ":");                       
                String inputMsgs = null;
                //First Send Nxt and then reference tx to Message and send msg as well
                while (tokensMR.hasMoreTokens()){
                    try {
                        String receiverMR = tokensMR.nextToken().toString();
                        String amountMA = tokensMA.nextToken().toString();
                        String inputMsgMM = tokensMM.nextToken().toString();
                        
                        if (!amountMA.equals("0")){
                            sendAPI = "nxt?requestType=sendMoney&recipient=";
                            sendAPI += URLEncoder.encode(receiverMR, "UTF-8");
                            sendAPI += "&amount=";
                            sendAPI += URLEncoder.encode(amountMA, "UTF-8");
                            sendAPI += "&fee=";
                            sendAPI += URLEncoder.encode(defaultFee.getText(), "UTF-8");
                            sendAPI += "&deadline=";
                            sendAPI += URLEncoder.encode(defaultDeadline.getText(), "UTF-8");
                            sendAPI += "&secretPhrase=";
                            sendAPI += URLEncoder.encode(passphraseField.getText(), "UTF-8");
                            try {
                                defaccb = getBalance(senderAddressComboBox.getSelectedItem().toString());
                            } catch (IOException ex) {
                                Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            if (defaccb > 1){
                                if (Float.parseFloat(amountMA) > defaccb){
                                    JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("ACCOUNT BALANCE IS LOWER THAN AMOUNT ENTERED!"));
                                }
                                else
                                {
                                    try {
                                        strNxt = urlExec(globalURL,sendAPI);
                                    } catch (IOException ex) {
                                        Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                    consoleOutput.append("\n["+nowtime()+"]"+java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("1. SENT NXT :")+strNxt);
                                    
                                    JSONParser parser = new JSONParser();
                                    Object obj = null;
                                    try {
                                        obj = parser.parse(strNxt);
                                    } catch (ParseException ex) {
                                        Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                    JSONObject jsonObject = (JSONObject) obj;
                                    if (jsonObject != null){
                                        Tx = (String) jsonObject.get("transaction");
                                    }
                                }
                            }
                            else
                                JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("NOT ENOUGH FUND!"));
                            
                        } //if amount is not zero
                        else{
                            //only message part
                            sendMsgAPI = "nxt?requestType=sendMessage&secretPhrase=";
                            sendMsgAPI += URLEncoder.encode(passphraseField.getText(), "UTF-8");
                            sendMsgAPI += "&recipient=";
                            sendMsgAPI += URLEncoder.encode(receiverMR, "UTF-8");
                            sendMsgAPI += "&fee=";
                            sendMsgAPI += URLEncoder.encode(defaultFee.getText(), "UTF-8");
                            
                            Long accs = nxt.util.Convert.parseUnsignedLong(receiverMR);
                            byte[] pvkey = Crypto.getPrivateKey(passphraseField.getText());
                            byte[] pubkey = nxt.Account.getAccount(accs).getPublicKey();
                            byte[] encryptedtext = Crypto.encryptMessage(inputMsgMM, pvkey, pubkey);
                            //byte[] mm = Crypto.encodeMessage(inputMsgMM, passphraseField.getText(), getPubKey(receiverMR).getBytes("UTF-8"));
                            inputMsgs = nxt.util.Convert.toHexString(encryptedtext);//convertStringToHex(new String(encryptedtext, "UTF-8"));
                            
                            sendMsgAPI += "&message=";
                            sendMsgAPI += URLEncoder.encode(inputMsgs, "UTF-8");
                            sendMsgAPI += "&deadline=";
                            sendMsgAPI += URLEncoder.encode(defaultDeadline.getText(), "UTF-8");
                            try {
                                defaccb = getBalance(senderAddressComboBox.getSelectedItem().toString());
                            } catch (IOException ex) {
                                Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            try {
                                if (defaccb < 1){
                                    JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("ACCOUNT BALANCE MUST BE MORE THAN 1 NXT"));
                                }
                                else{
                                    strMsg = urlExec(globalURL,sendMsgAPI);
                                    consoleOutput.append("\n["+nowtime()+"]"+java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("SENT MSG :")+strMsg);
                                    statusbarLabel.setText(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("MSG SENT..."));
                                }
                            } catch (IOException ex) {
                                Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }//else amount is zero (send only message)
                        
                    }                catch (UnsupportedEncodingException ex) {
                        Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } //else msg included
        }//if it is bulk
        else{
            //non bulk part
            if ((amountNxtField.getText().contains(":"))||receiverAddressField.getText().contains(":"))
                JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("YOU ARE TRYING BULK SEND?\nPLEASE CHECK FORMAT, RECIPIENT AND AMOUNT FIELDS MUST END WITH \":\"."));
            else {
            if (attachMessageCheckBox.isSelected()){
                if (Float.parseFloat(amountNxtField.getText()) == 0){
                try {
                    sendMsgAPI = "nxt?requestType=sendMessage&secretPhrase=";
                    sendMsgAPI += URLEncoder.encode(passphraseField.getText(), "UTF-8");
                    sendMsgAPI += "&recipient=";
                    sendMsgAPI += URLEncoder.encode(receiverAddressField.getText(), "UTF-8");
                    sendMsgAPI += "&fee=";
                    sendMsgAPI += URLEncoder.encode(defaultFee.getText(), "UTF-8");
                    
                    Long accs = nxt.util.Convert.parseUnsignedLong(receiverAddressField.getText());
                    byte[] pvkey = Crypto.getPrivateKey(passphraseField.getText());
                    byte[] pubkey = nxt.Account.getAccount(accs).getPublicKey();
                    byte[] encryptedtext = Crypto.encryptMessage(inputMessageField.getText(), pvkey, pubkey);
                    
                    //byte[] mm = Crypto.encodeMessage(inputMessageField.getText(), passphraseField.getText(), getPubKey(receiverAddressField.getText()).getBytes("UTF-8"));
                    String inputMsgs = nxt.util.Convert.toHexString(encryptedtext);//convertStringToHex(new String(encryptedtext, "UTF-8"));
                    sendMsgAPI += "&message=";
                    sendMsgAPI += URLEncoder.encode(inputMsgs, "UTF-8");
                    sendMsgAPI += "&deadline=";
                    sendMsgAPI += URLEncoder.encode(defaultDeadline.getText(), "UTF-8");
                    try {
                        defaccb = getBalance(senderAddressComboBox.getSelectedItem().toString());
                    } catch (IOException ex) {
                        Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    try {
                        if (defaccb < 1){
                            JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("ACCOUNT BALANCE MUST BE MORE THAN 1 NXT"));
                        }
                        else{
                            strMsg = urlExec(globalURL,sendMsgAPI);
                            consoleOutput.append("\n["+nowtime()+"]"+java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("SENT MSG :")+strMsg);
                            statusbarLabel.setText(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("MSG SENT"));
                        }
                    } catch (IOException ex) {
                        Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } catch (UnsupportedEncodingException ex) {
                    Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                }
                } // if amount is 0, only message
                else {
                    try {
                    //sending NXT first then reference TX to included message.
                    sendAPI = "nxt?requestType=sendMoney&recipient=";
                    sendAPI += URLEncoder.encode(receiverAddressField.getText(), "UTF-8");
                    sendAPI += "&amount=";
                    sendAPI += URLEncoder.encode(amountNxtField.getText(), "UTF-8");
                    sendAPI += "&fee=";
                    sendAPI += URLEncoder.encode(defaultFee.getText(), "UTF-8");
                    sendAPI += "&deadline=";
                    sendAPI += URLEncoder.encode(defaultDeadline.getText(), "UTF-8");
                    sendAPI += "&secretPhrase=";
                    sendAPI += URLEncoder.encode(passphraseField.getText(), "UTF-8");
                    try {
                        defaccb = getBalance(senderAddressComboBox.getSelectedItem().toString());
                    } catch (IOException ex) {
                        Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    if (defaccb > 1){
                        if (Float.parseFloat(amountNxtField.getText()) > defaccb){
                            JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("ACCOUNT BALANCE IS LOWER THAN AMOUNT ENTERED!"));
                        }else{
                            try {
                                strNxt = urlExec(globalURL,sendAPI);
                            } catch (IOException ex) {
                                Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            
                            consoleOutput.append("\n["+nowtime()+"]"+java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("SENT NXT :")+strNxt);
                            statusbarLabel.setText(java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("SENT NXT "));
                            
                            JSONParser parser = new JSONParser();
                            Object obj = null;
                            try {
                                obj = parser.parse(strNxt);
                            } catch (ParseException ex) {
                                Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            JSONObject jsonObject = (JSONObject) obj;
                            if (jsonObject != null){
                                Tx = (String) jsonObject.get("transaction");
                            }
                         }
                    }else
                        JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("NOT ENOUGH FUND!"));
                    } catch (UnsupportedEncodingException ex) {
                        Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                  // above sent NXT get tx
                    //now include msg reference tx
                    try {
                    sendMsgAPI = "nxt?requestType=sendMessage&secretPhrase=";
                    sendMsgAPI += URLEncoder.encode(passphraseField.getText(), "UTF-8");
                    sendMsgAPI += "&recipient=";
                    sendMsgAPI += URLEncoder.encode(receiverAddressField.getText(), "UTF-8");
                    sendMsgAPI += "&fee=";
                    sendMsgAPI += URLEncoder.encode(defaultFee.getText(), "UTF-8");
                    
                    Long accs = nxt.util.Convert.parseUnsignedLong(receiverAddressField.getText());
                    byte[] pvkey = Crypto.getPrivateKey(passphraseField.getText());
                    byte[] pubkey = nxt.Account.getAccount(accs).getPublicKey();
                    byte[] encryptedtext = Crypto.encryptMessage(inputMessageField.getText(), pvkey, pubkey);
            
                    //byte[] mm = Crypto.encodeMessage(inputMessageField.getText(), passphraseField.getText(), getPubKey(receiverAddressField.getText()).getBytes("UTF-8"));
                    String inputMsgs = nxt.util.Convert.toHexString(encryptedtext);//convertStringToHex(new String(encryptedtext, "UTF-8"));
                    sendMsgAPI += "&message=";
                    sendMsgAPI += URLEncoder.encode(inputMsgs, "UTF-8");
                    sendMsgAPI += "&deadline=";
                    sendMsgAPI += URLEncoder.encode(defaultDeadline.getText(), "UTF-8");
                    sendMsgAPI += "&referencedTransaction=";
                    sendMsgAPI += URLEncoder.encode(Tx, "UTF-8");
                    try {
                        if (defaccb < 1){
                            JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("ACCOUNT BALANCE MUST BE MORE THAN 1 NXT"));
                        }
                        else{
                            strMsg = urlExec(globalURL,sendMsgAPI);
                            consoleOutput.append("\n["+nowtime()+"]"+java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("SENT MSG :")+strMsg);
                            statusbarLabel.setText(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("MSG SENT"));
                        }
                    } catch (IOException ex) {
                        Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } catch (UnsupportedEncodingException ex) {
                    Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                }
                } //else amount is > 0, message included
            }
            else {
                try {
                    //single address
                    sendAPI = "nxt?requestType=sendMoney&recipient=";
                    sendAPI += URLEncoder.encode(receiverAddressField.getText(), "UTF-8");
                    sendAPI += "&amount=";
                    sendAPI += URLEncoder.encode(amountNxtField.getText(), "UTF-8");
                    sendAPI += "&fee=";
                    sendAPI += URLEncoder.encode(defaultFee.getText(), "UTF-8");
                    sendAPI += "&deadline=";
                    sendAPI += URLEncoder.encode(defaultDeadline.getText(), "UTF-8");
                    sendAPI += "&secretPhrase=";
                    sendAPI += URLEncoder.encode(passphraseField.getText(), "UTF-8");
                    try {
                        defaccb = getBalance(senderAddressComboBox.getSelectedItem().toString());
                    } catch (IOException ex) {
                        Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    if (defaccb > 1){
                        if (Float.parseFloat(amountNxtField.getText()) > defaccb){
                            JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("ACCOUNT BALANCE IS LOWER THAN AMOUNT ENTERED!"));
                        }else{
                            try {
                                strNxt = urlExec(globalURL,sendAPI);
                            } catch (IOException ex) {
                                Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            Date date = new Date();
                            String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(date);
                            consoleOutput.append("\n["+time+"]"+java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("SENT NXT :")+strNxt);
                            statusbarLabel.setText(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("SENT NXT"));
                        }
                    }else
                        JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("NOT ENOUGH FUND!"));
                } catch (UnsupportedEncodingException ex) {
                    Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                }
            } //else if message is not included   
            } //else correct format
        }
        passphraseField.setText("");
        amountNxtField.setText("");
        inputMessageField.setText("");
        accountCheckTable.removeAll();
        SentDialog.dispose();
    }//GEN-LAST:event_OKbuttonActionPerformed

    private void senderAddressComboBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_senderAddressComboBoxMouseClicked
        //add list of acccounts
    }//GEN-LAST:event_senderAddressComboBoxMouseClicked

    private void updateBalancesButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateBalancesButtonActionPerformed
        //Object[][] rowData = {};
        //Object[] columnNames = {java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("LABEL"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("ACCOUNT_NUMBER"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("BALANCE")};

        //AccountsTable.removeAll();
        //DefaultTableModel AccountsTableModel;
        //AccountsTableModel = new DefaultTableModel(rowData, columnNames);
        //AccountsTable.setModel(AccountsTableModel);
        
        //select * from accounts
        //and display in jtable
        //JCTable accountsA = new JCTable("lib/accounts");
        //JCRow[] resultAll = accountsA.select();
        Vector acc = readAccounts(null);
        for (int i = 0; i < acc.size(); i++) {
            /*Vector dat = readRecord(acc.get(i).toString());
            Vector resultAll = (Vector) dat.get(0);
            String LabelName = (String) resultAll.get(0); //label
            String AccountN = (String) resultAll.get(1); //account
            String dbalance = (String) resultAll.get(2); //balance*/
            Long accb = null;
            try {
                accb = getBalance(acc.get(i).toString());
                accb = accb;
            } catch (IOException ex) {
                Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
            }
            //accb = nxt.Account.getAccount(AccountN.getBytes()).getBalance();
            String nbalance = accb.toString();
            //consoleOutput.append("\nB:"+nbalance);
            //String Balance = resultAll[i].getString("Balance");
            tableModelA.updateAccount(null, null, nbalance, tableModelA.getAccount(i).getDeadline(), i);
            try {
                updateRecordIntoDbAccountsTable(acc.get(i).toString(), nbalance);
            } catch (SQLException ex) {
                Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_updateBalancesButtonActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        SentDialog.dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void englishMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_englishMenuItemActionPerformed
        prefs.put(PREF_language, "en");
    }//GEN-LAST:event_englishMenuItemActionPerformed

    private void russianMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_russianMenuItemActionPerformed
        prefs.put(PREF_language, "ru");
    }//GEN-LAST:event_russianMenuItemActionPerformed

    private void restartCMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_restartCMenuActionPerformed
        if (flag == 1){
            stopServer();
        }
        //if (flag == 0)
            startServer();
        //isTestnetCMenu.setEnabled(false);
        restartCMenu.setEnabled(false);
    }//GEN-LAST:event_restartCMenuActionPerformed

    private void stopCMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stopCMenuActionPerformed
        if (flag != 0){
            stopServer();
            //isTestnetCMenu.setEnabled(true);
        }
        restartCMenu.setEnabled(true);
    }//GEN-LAST:event_stopCMenuActionPerformed

    private void assetexchangeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_assetexchangeButtonActionPerformed
        AliasesPanel.setVisible(false);
        ConsolePanel.setVisible(false);
        SendPanel.setVisible(false);
        TransactionsPanel.setVisible(false);
        ReceivePanel.setVisible(false);
        OverviewPanel.setVisible(false);
        MessagesPanel.setVisible(false);
        BlocksPanel.setVisible(false);
        VotingPanel.setVisible(false);
        AssetExPanel.setVisible(true);
        
        
    }//GEN-LAST:event_assetexchangeButtonActionPerformed

    private void votingButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_votingButtonActionPerformed
        AliasesPanel.setVisible(false);
        ConsolePanel.setVisible(false);
        SendPanel.setVisible(false);
        TransactionsPanel.setVisible(false);
        ReceivePanel.setVisible(false);
        OverviewPanel.setVisible(false);
        MessagesPanel.setVisible(false);
        BlocksPanel.setVisible(false);
        VotingPanel.setVisible(true);
        AssetExPanel.setVisible(false);
        
        
        
    }//GEN-LAST:event_votingButtonActionPerformed
    private String txfilepath;
    private String blfilepath;    
    private void searchAssetFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchAssetFieldActionPerformed
        String filter = searchAssetField.getText().trim();
        DefaultRowSorter rs = (DefaultRowSorter)allAssetsTable.getRowSorter();
        rs.setRowFilter(filter.length() > 0 ?
        RowFilters.regexFilter(".*?"+filter+".*?", 0, 1, 2) : null);
    }//GEN-LAST:event_searchAssetFieldActionPerformed

    private void aliasSearchFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aliasSearchFieldActionPerformed
        String value = aliasSearchField.getText();
        aliasTable.repaint();
        for (int row = 0; row <= aliasTable.getRowCount() - 1; row++) {
            for (int col = 0; col <= aliasTable.getColumnCount() - 1; col++) {
                //Object values = aliasTable.getValueAt(row, col);
                //if (values!=null && values.toString().matches(".*"+Pattern.quote(value)+".*")){
                if (value.equals(aliasTable.getValueAt(row, col))) {
                    // this will automatically set the view of the scroll in the location of the value
                    aliasTable.scrollRectToVisible(aliasTable.getCellRect(row, 0, true));
                    // this will automatically set the focus of the searched/selected row/value
                    aliasTable.setRowSelectionInterval(row, row);
                    for (int i = 0; i <= aliasTable.getColumnCount() - 1; i++) {
                        aliasTable.getColumnModel().getColumn(i).setCellRenderer(new HighlightRenderer());
                    }
                }
            }
        }
    }//GEN-LAST:event_aliasSearchFieldActionPerformed

    private void issueAssetButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_issueAssetButtonActionPerformed
        issueAssetDialog.setLocationRelativeTo(null);
        issueAssetDialog.setTitle(java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("ISSUE ASSET"));
        issueAssetDialog.setSize(560, 250);
        issueAssetDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        issueAssetDialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        issueAssetDialog.setVisible(true);
    }//GEN-LAST:event_issueAssetButtonActionPerformed

    private void transferAssetButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_transferAssetButtonActionPerformed
       
        //JCTable accountsA = new JCTable("lib/accounts");
        assetAccountsComboBox.removeAllItems();
        //JCRow[] acclist = accountsA.select();
        Vector acclist = readAccounts(null);
        for (int i=0; i<acclist.size(); i++) {
            assetAccountsComboBox.addItem(acclist.get(i).toString());
        }
        
        transferAssetDialog.setLocationRelativeTo(null);
        transferAssetDialog.setTitle(java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("TRANSFER ASSET"));
        transferAssetDialog.setSize(580, 290);
        transferAssetDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        transferAssetDialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        transferAssetDialog.setVisible(true);
    }//GEN-LAST:event_transferAssetButtonActionPerformed

    private void ordersAssetButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ordersAssetButtonActionPerformed

        //JCTable accountsA = new JCTable("lib/accounts");
        assetAccountListComboBox.removeAllItems();
        //JCRow[] acclist = accountsA.select();
        Vector acclist = readAccounts(null);
        for (int i=0; i<acclist.size(); i++) {
            assetAccountListComboBox.addItem(acclist.get(i).toString());
        }
        
        
        myOrdersDialog.setLocationRelativeTo(null);
        myOrdersDialog.setTitle(java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("MY ORDERS"));
        myOrdersDialog.setSize(560, 560);
        myOrdersDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        myOrdersDialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        myOrdersDialog.setVisible(true);
    }//GEN-LAST:event_ordersAssetButtonActionPerformed

    private void getAccountCurrentBidOrderIds(String accountId){

        String bidAPI = "nxt?requestType=getAccountCurrentBidOrderIds&account=";
        
        try {
            bidAPI += URLEncoder.encode(accountId, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        String str = null;
        try {
            str = urlExec(globalURL,bidAPI);
        } catch (IOException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        JSONParser parser = new JSONParser();
        Object obj = null;
        try {
            obj = parser.parse(str);
        } catch (ParseException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        JSONObject jsonObject = (JSONObject) obj;
        jProgressBar1.setValue(0);
        jProgressBar1.setStringPainted(true);
        int totalDataRead=0;
        int length;
        if (jsonObject != null){
            JSONArray newjsonObject = (JSONArray) jsonObject.get("bidOrderIds");
            if (newjsonObject != null){
                Iterator<String> iterator = newjsonObject.iterator();
                length = newjsonObject.size();

                while (iterator.hasNext()) {
                    String data[];
                    String name[];
                    String orderId = iterator.next();
                    data = getBidOrder(orderId);
                    name = getAsset(data[1]);
                    
                    totalDataRead=totalDataRead+1;
                    float Percent=(totalDataRead*100)/length;
                    jProgressBar1.setValue((int)Percent);
                    
                    tableCurrentOrders.addRow(new Object[]{"bid", orderId, name[1] ,data[2], data[3]});
                }
            }
        }
    }
    Object[][] rowData7 = {};
    Object[] columnNames7 = {java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("TYPE"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("ORDER ID"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("ASSET NAME"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("QUANTITY"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("PRICE")};
    DefaultTableModel tableCurrentOrders = new DefaultTableModel(rowData7, columnNames7);
    private void getAccountCurrentAskOrderIds(String accountId){
        
        String bidAPI = "nxt?requestType=getAccountCurrentAskOrderIds&account=";
        
        try {
            bidAPI += URLEncoder.encode(accountId, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        String str = null;
        try {
            str = urlExec(globalURL,bidAPI);
        } catch (IOException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        JSONParser parser = new JSONParser();
        Object obj = null;
        try {
            obj = parser.parse(str);
        } catch (ParseException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        JSONObject jsonObject = (JSONObject) obj;
        jProgressBar1.setValue(0);
        jProgressBar1.setStringPainted(true);
        int totalDataRead=0;
        int length;
        if (jsonObject != null){
            JSONArray newjsonObject = (JSONArray) jsonObject.get("askOrderIds");
            if (newjsonObject != null){
                Iterator<String> iterator = newjsonObject.iterator();
                length = newjsonObject.size();
                while (iterator.hasNext()) {
                    String data[];
                    String name[];
                    String orderId = iterator.next();
                    data = getAskOrder(orderId);
                    name = getAsset(data[1]);
                    totalDataRead=totalDataRead+1;
                    float Percent=(totalDataRead*100)/length;
                    jProgressBar1.setValue((int)Percent);
                    
                    tableCurrentOrders.addRow(new Object[]{"ask", orderId, name[1] ,data[2], data[3]});
                }
            }
        }
    }
    private void viewAskBidCMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewAskBidCMenuActionPerformed
        
        int index = allAssetsTable.getSelectedRow();
        
        if (index >= 0){
            String assetId = allAssetsTable.getValueAt(index, 4).toString();
            askOrdersAssetTable.removeAll();
            bidOrdersAssetTable.removeAll();
            
            getAssetAskOrders(assetId);
            getAssetBidOrders(assetId);
            
            assetOrdersDialog.setLocationRelativeTo(null);
            assetOrdersDialog.setTitle(java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("CURRENT ORDERS FOR CHOSEN ASSET"));
            assetOrdersDialog.setSize(560, 560);
            assetOrdersDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            assetOrdersDialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
            assetOrdersDialog.setVisible(true);

        }else
            JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("PLEASE, SELECT THE ASSET"));
    }//GEN-LAST:event_viewAskBidCMenuActionPerformed

    private void assetOrderSubmitButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_assetOrderSubmitButtonActionPerformed
        if (!assetIdOrderLabel.equals("."))
            placeOrder(assetIdOrderLabel.getText());
    }//GEN-LAST:event_assetOrderSubmitButtonActionPerformed

    private void placeAskBidCMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_placeAskBidCMenuActionPerformed
        int index = allAssetsTable.getSelectedRow();
        if (index >= 0){
            assetIdOrderLabel.setText(allAssetsTable.getValueAt(index, 4).toString());
            placeOrderDialog.setLocationRelativeTo(null);
            placeOrderDialog.setTitle(java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("PLACE ORDER"));
            placeOrderDialog.setSize(280, 350);
            placeOrderDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            placeOrderDialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
            placeOrderDialog.setVisible(true);
        }
    }//GEN-LAST:event_placeAskBidCMenuActionPerformed

    private void assetExMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_assetExMenuActionPerformed
        TransactionsPanel.setVisible(false);
        ConsolePanel.setVisible(false);
        SendPanel.setVisible(false);
        ReceivePanel.setVisible(false);
        AliasesPanel.setVisible(false);
        MessagesPanel.setVisible(false);
        BlocksPanel.setVisible(false);
        OverviewPanel.setVisible(false);
        AssetExPanel.setVisible(true);
        assetexchangeButton.setSelected(true);
        VotingPanel.setVisible(false);
    }//GEN-LAST:event_assetExMenuActionPerformed

    private void votingMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_votingMenuActionPerformed
        TransactionsPanel.setVisible(false);
        ConsolePanel.setVisible(false);
        SendPanel.setVisible(false);
        ReceivePanel.setVisible(false);
        AliasesPanel.setVisible(false);
        MessagesPanel.setVisible(false);
        BlocksPanel.setVisible(false);
        OverviewPanel.setVisible(false);
        AssetExPanel.setVisible(false);
        VotingPanel.setVisible(true);
        votingButton.setSelected(true);
    }//GEN-LAST:event_votingMenuActionPerformed

    private void issueAssetButtonInActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_issueAssetButtonInActionPerformed
        String name = assetNameField.getText();
        String description = assetDescriptionField.getText();
        String quantity = assetQtyField.getText();
        String passphrase = assetPassphraseField.getText();
        String fee = String.valueOf(Integer.parseInt(defaultFee.getText())+999);
        String issueAPI = "nxt?requestType=issueAsset&secretPhrase=";
        try {
            issueAPI += URLEncoder.encode(passphrase, "UTF-8");
            issueAPI += "&name=";
            issueAPI += URLEncoder.encode(name, "UTF-8");
            issueAPI += "&description=";
            issueAPI += URLEncoder.encode(description, "UTF-8");
            issueAPI += "&quantity=";
            issueAPI += URLEncoder.encode(quantity, "UTF-8");
            issueAPI += "&fee=";
            issueAPI += URLEncoder.encode(fee, "UTF-8");
            issueAPI += "&deadline=";
            issueAPI += URLEncoder.encode(defaultDeadline.getText(), "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        String str = null;
        try {
            str = urlExec(globalURL, issueAPI);
        } catch (IOException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        consoleOutput.append("\n["+nowtime()+"]"+java.util.ResourceBundle.getBundle("clienxt/Bundle").getString(" ASSET ISSUE TX :")+str);
        statusbarLabel.setText(str);
        
        assetNameField.setText("");
        assetDescriptionField.setText("");
        assetPassphraseField.setText("");
        assetQtyField.setText("");
        issueAssetDialog.dispose();
    }//GEN-LAST:event_issueAssetButtonInActionPerformed

    private void assetAccountListComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_assetAccountListComboBoxActionPerformed
        if (assetAccountListComboBox.getItemCount()>0){
            myOrdersTable.removeAll();
            tableCurrentOrders.setRowCount(0);
            myOrdersTable.setModel(tableCurrentOrders);
            getAccountCurrentBidOrderIds(assetAccountListComboBox.getSelectedItem().toString());
            getAccountCurrentAskOrderIds(assetAccountListComboBox.getSelectedItem().toString());
        }
    }//GEN-LAST:event_assetAccountListComboBoxActionPerformed

    private void assetAccountsComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_assetAccountsComboBoxActionPerformed
        nameAssetComboBox.removeAllItems();
        if (assetAccountsComboBox.getItemCount()>0)
            getAccountCurrentAssets(assetAccountsComboBox.getSelectedItem().toString());
    }//GEN-LAST:event_assetAccountsComboBoxActionPerformed

    private void transferAssetButtonInActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_transferAssetButtonInActionPerformed
        transferAsset();
        passphraseAssetField.setText("");
        recipientAssetField.setText("");
        quantityAssetField.setText("");
        issueAssetDialog.dispose();
    }//GEN-LAST:event_transferAssetButtonInActionPerformed

    private void cancelOrderCMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelOrderCMenuActionPerformed
        int index = myOrdersTable.getSelectedRow();
        if (index >= 0){
            typeOrderLabel.setText("type " + myOrdersTable.getValueAt(index, 0).toString());
            idOrderLabel.setText(myOrdersTable.getValueAt(index, 1).toString());
            myOrderCancelDialog.setLocationRelativeTo(null);
            myOrderCancelDialog.setTitle(java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("CANCEL ORDER"));
            myOrderCancelDialog.setSize(355, 155);
            myOrderCancelDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            myOrderCancelDialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
            myOrderCancelDialog.setVisible(true);
        }
    }//GEN-LAST:event_cancelOrderCMenuActionPerformed

    private void confirmCancelAssetButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confirmCancelAssetButtonActionPerformed
        
        int type = 0; //0 - ask, 1 - bid
        if (typeOrderLabel.getText().endsWith("ask"))
            type = 0;
        if (typeOrderLabel.getText().endsWith("bid"))
            type = 1;
        
        cancelAsset(type);
        
        cancelPassField.setText("");
        myOrderCancelDialog.dispose();
                
    }//GEN-LAST:event_confirmCancelAssetButtonActionPerformed
    private void nameAssetComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nameAssetComboBoxActionPerformed
        String data[];
        int index = nameAssetComboBox.getItemCount();
        if (index > 0)
            if (!nameAssetComboBox.getSelectedItem().toString().endsWith("!")){
                quantityAssetField.setText(quantityAssetList.get(nameAssetComboBox.getSelectedIndex()));
                //data = getAsset(nameAssetComboBox.getSelectedItem().toString());
                //quantityAssetField.setText(data[3]);
            }
    }//GEN-LAST:event_nameAssetComboBoxActionPerformed

    private void viewTradesCMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewTradesCMenuActionPerformed
        int index = allAssetsTable.getSelectedRow();
        if (index >= 0){
            getTrades(allAssetsTable.getValueAt(index, 4).toString());
            assetTradesDialog.setLocationRelativeTo(null);
            assetTradesDialog.setTitle(java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("TRADES HISTORY"));
            assetTradesDialog.setSize(640, 500);
            assetTradesDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            assetTradesDialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
            assetTradesDialog.setVisible(true);
        }
    }//GEN-LAST:event_viewTradesCMenuActionPerformed

    private void updateTableCMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateTableCMenuActionPerformed
        Swingworke mainWorker = new Swingworke(allAssetsTable);
        mainWorker.execute(); 
        
        /*if (assetexchangeButton.isSelected()){
            try {
                getAssetIds();
            } catch (IOException ex) {
                Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
            }
        }*/
    }//GEN-LAST:event_updateTableCMenuActionPerformed
    TableRowSorter<TableModel> sorter;
    /*private void newFilter() {
        RowFilter<TableModel, Object> rf = null;
        //If current expression doesn't parse, don't update.
        try {
            rf = RowFilter.regexFilter(searchAssetField.getText(), 0);
        } catch (java.util.regex.PatternSyntaxException e) {
            return;
        }
        sorter.setRowFilter(rf);
    }*/
    private void searchAssetFieldInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_searchAssetFieldInputMethodTextChanged
        
    }//GEN-LAST:event_searchAssetFieldInputMethodTextChanged
    class Swingworke extends SwingWorker<TableModel, TableModel> {

        private final JTable table;

        public Swingworke(JTable table) {
            this.table = table;
        }    

        @Override
        protected void process(List<TableModel> chunks) {
            table.setModel(chunks.get(chunks.size()-1));
        }
        
        @Override
        protected TableModel doInBackground() throws Exception {
        
            //gui table
            Object[][] rowData = {};
            Object[] columnNames = {java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("ACCOUNT"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("NAME"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("DESCRIPTION"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("QUANTITY"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("ASSETID")};

            DefaultTableModel tableModelAssets = new DefaultTableModel(rowData, columnNames);
                //get all assetIds
                String assetsAPI = "nxt?requestType=getAssetIds";
                String strA = urlExec(globalURL,assetsAPI);

                JSONParser parser3 = new JSONParser();
                Object obj3 = null;
                try {
                    obj3 = parser3.parse(strA);
                } catch (ParseException ex) {
                    Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                }
                JSONObject jsonObject3 = (JSONObject) obj3;
                JSONArray assets = null;
                int totalDataRead = 0;
                jProgressBar1.setValue(0);
                jProgressBar1.setStringPainted(true);

                if (jsonObject3 != null){
                    assets = (JSONArray) jsonObject3.get("assetIds");
                    Iterator<String> iterator = assets.iterator();
                    int length = assets.size();
                    while (iterator.hasNext()) {
                        String data[];
                        data = getAsset(iterator.next());
                        tableModelAssets.addRow(new Object[]{data[0], data[1], data[2], data[3], data[4]});
                        
                        publish(tableModelAssets);

                        totalDataRead=totalDataRead+1;
                        float Percent=(totalDataRead*100)/length;
                        jProgressBar1.setValue((int)Percent);
                    }
            }
            return tableModelAssets;
        }
        @Override
        protected void done() {
            try {
                TableModel model = get();
                table.setModel(model);

            } catch (InterruptedException ex) {
            } catch (ExecutionException ex) {
                Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    class VoteWorker extends SwingWorker<TableModel, TableModel> {

        private final JTable table;

        public VoteWorker(JTable table) {
            this.table = table;
        }    

        @Override
        protected void process(List<TableModel> chunks) {
            table.setModel(chunks.get(chunks.size()-1));
        }
        
        @Override
        protected TableModel doInBackground() throws Exception {
        
            Object[][] rowData = {};
            Object[] columnNames = {java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("NAME"), java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("DESCRIPTION"), java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("OPTIONS"), java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("VOTERS"), java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("POLLID")};

            //DefaultTableModel tableModelAssets = new DefaultTableModel(rowData, columnNames);
            DefaultTableModel tableModelVotes = new DefaultTableModel(rowData, columnNames);

            String assetsAPI = "nxt?requestType=getPollIds";
            String strA = urlExec(globalURL,assetsAPI);

            JSONParser parser3 = new JSONParser();
            Object obj3 = null;
            try {
                obj3 = parser3.parse(strA);
            } catch (ParseException ex) {
                Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
            }
            JSONObject jsonObject3 = (JSONObject) obj3;
            JSONArray polls = null;
            int totalDataRead = 0;
            jProgressBar1.setValue(0);
            jProgressBar1.setStringPainted(true);

            if (jsonObject3 != null){
                polls = (JSONArray) jsonObject3.get("pollIds");
                Iterator<String> iterator = polls.iterator();
                int length = polls.size();
                while (iterator.hasNext()) {
                    String data[];
                    data = getPoll(iterator.next());

                    TableColumn sportColumn = allPollsTable.getColumnModel().getColumn(2);

                    System.out.println(data[2]);

                    tableModelVotes.addRow(new Object[]{data[0], data[1], data[2], data[3], data[4]});

                    publish(tableModelVotes);

                    totalDataRead=totalDataRead+1;
                    float Percent=(totalDataRead*100)/length;
                    jProgressBar1.setValue((int)Percent);
                }
            }
            return tableModelVotes;
        }
        @Override
        protected void done() {
            try {
                TableModel model = get();
                table.setModel(model);

            } catch (InterruptedException ex) {
            } catch (ExecutionException ex) {
                Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    
    
    /*class BackWorker extends SwingWorker<String, String> {
        public String deadline;
        private String secretPass;

        public BackWorker(String secretPassphrase) {
            this.secretPass = secretPassphrase;
        }  
        @Override
        protected String doInBackground() throws Exception {
            //Updating Peers/Blocks
            int delay = 60000; //60 secs
            ActionListener taskPerformer = new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent evt) {
                    String[] data = getCurrentState();
                    if (data[0] != null)
                        deadline = startForging(secretPass);
                    publish(deadline);
                }
            };
            new Timer(delay, taskPerformer).start();
            return deadline;
        }

        @Override
        protected void done() {
            String text = null;
            try {
                text = get();
                if (text != null)
                    timeToForgeLabel.setText(getDurationString(Integer.getInteger(text)));
            } catch (InterruptedException ex) {
                Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ExecutionException ex) {
                Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
            }
            timeToForgeLabel.setText(text);
        }

        @Override
        protected void process(List<String> chunks) {
            final String time = chunks.get(chunks.size()-1);
            
            System.out.println(time);
            if (time != null){
                //System.out.println(getDurationString(Integer.parseInt(time)));
                
                //timeToForgeLabel.setText(getDurationString(Integer.parseInt(time)));
                
                Timer countDown = new Timer(1000, new ActionListener(){
                    double ntime = Double.valueOf(time);
                    SimpleDateFormat setTime = new SimpleDateFormat("hh:mm:ss");
                    public void actionPerformed(ActionEvent e) {
                        if (ntime >= 0) {
                            //timeToForgeLabel.setText(setTime.format(ntime));
                            int daysLeft = (int) (ntime/(60*60*24));
                            int hoursLeft = (int) ((ntime/3600)-(daysLeft*24));
                            int minutesLeft = (int) ((ntime/60)-((hoursLeft*60)+(daysLeft*24*60)));
                            int secondsLeft = (int) ((ntime)-((minutesLeft*60)+(hoursLeft*60*60)+(daysLeft*24*60*60)));
                            timeToForgeLabel.setText(daysLeft + " days, " + hoursLeft + " hours, " + minutesLeft + " minutes, " + secondsLeft + " seconds left.");
                            ntime = ntime-1;
                            if(ntime<=0){
                                timeToForgeLabel.setText("not forging!");
                            }
                        }
                    }   
                });
                if (countDown.isRunning()){
                    countDown.stop();
                    countDown.start();
                }
                else
                    countDown.start();
            }
        }
    }*/
        
    private String getDurationString(int seconds) {
        int hours = seconds / 3600;
        int minutes = (seconds % 3600) / 60;
        seconds = seconds % 60;
        //System.out.println(hours);
        return twoDigitString(hours) + " : " + twoDigitString(minutes) + " : " + twoDigitString(seconds);
    }
    private String twoDigitString(int number) {
        if (number == 0) {
            return "00";
        }
        
        if (number / 10 == 0) {
            return "0" + number;
        }
        return String.valueOf(number);
    }
    private String startForging(String secretPhrase){
        String deadline = null;
        String forgingAPI = "nxt?requestType=startForging&secretPhrase=";
        try {
            forgingAPI += URLEncoder.encode(secretPhrase, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        String str = null;
        try {
            str = urlExec(globalURL,forgingAPI);
        } catch (IOException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        JSONParser parser = new JSONParser();
        Object obj = null;
        try {
            obj = parser.parse(str);
        } catch (ParseException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        JSONObject jsonObject = (JSONObject) obj;
        if (jsonObject != null){
            deadline = String.valueOf(jsonObject.get("deadline"));
        }
        return deadline;
    }
    private void getAssetsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_getAssetsButtonActionPerformed
        Swingworke mainWorker = new Swingworke(allAssetsTable);
        mainWorker.execute(); 
    }//GEN-LAST:event_getAssetsButtonActionPerformed
    //static Log log = LogFactory.getLog(CipherUtils.class);
    public static String encryptAES(byte[] key, String strToEncrypt)
    {
        try
        {
            int ctLength;
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            final SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
            
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            // Encrypt the parameter toEncrypt
            
            byte[] cipherText = new byte[cipher.getOutputSize(strToEncrypt.length())];

            ctLength = cipher.update(strToEncrypt.getBytes(), 0, strToEncrypt.length(), cipherText, 0);

            ctLength += cipher.doFinal(cipherText, ctLength);
            
            //System.out.println("toEncrypt AES: "+ strToEncrypt);
            //String encryptedString = cipher.doFinal(strToEncrypt.getBytes()).toString();
            final String encryptedString = new String(cipherText).getBytes("UTF-8").toString();//new BASE64Encoder().encode(cipher.doFinal(strToEncrypt.getBytes()));
            
            return encryptedString;
        }
        catch (Exception e)
        {
            System.out.println(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("ERROR WHILE ENCRYPTING ")+ e);
        }
        return null;

    }
    private static void removeCryptographyRestrictions() {
        if (!isRestrictedCryptography()) {
            LogGui(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("CRYPTOGRAPHY RESTRICTIONS REMOVAL IS NOT NECESSARY"));
            return;
        }
        try {
            /*
             * Do the following, but with reflection to bypass access checks:
             *
             * JceSecurity.isRestricted = false;
             * JceSecurity.defaultPolicy.perms.clear();
             * JceSecurity.defaultPolicy.add(CryptoAllPermission.INSTANCE);
             */
            final Class<?> jceSecurity = Class.forName("javax.crypto.JceSecurity");
            final Class<?> cryptoPermissions = Class.forName("javax.crypto.CryptoPermissions");
            final Class<?> cryptoAllPermission = Class.forName("javax.crypto.CryptoAllPermission");

            final Field isRestrictedField = jceSecurity.getDeclaredField("isRestricted");
            isRestrictedField.setAccessible(true);
            isRestrictedField.set(null, false);

            final Field defaultPolicyField = jceSecurity.getDeclaredField("defaultPolicy");
            defaultPolicyField.setAccessible(true);
            final PermissionCollection defaultPolicy = (PermissionCollection) defaultPolicyField.get(null);

            final Field perms = cryptoPermissions.getDeclaredField("perms");
            perms.setAccessible(true);
            ((Map<?, ?>) perms.get(defaultPolicy)).clear();

            final Field instance = cryptoAllPermission.getDeclaredField("INSTANCE");
            instance.setAccessible(true);
            defaultPolicy.add((Permission) instance.get(null));

            LogGui(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("SUCCESSFULLY REMOVED CRYPTOGRAPHY RESTRICTIONS"));
        } catch (final Exception e) {
            LogGui(Level.WARNING+ java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("FAILED TO REMOVE CRYPTOGRAPHY RESTRICTIONS")+ e.getMessage());
        }
    }

    private static boolean isRestrictedCryptography() {
        // This simply matches the Oracle JRE, but not OpenJDK.
        return "Java(TM) SE Runtime Environment".equals(System.getProperty("java.runtime.name"));
    }
    private int ctLength0=0;
    public static String decryptAES(byte[] key, String strToDecrypt)
    {
        int maxKeyLen = 0;
        int ctLength = 0;
        try {
            //Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
            maxKeyLen = Cipher.getMaxAllowedKeyLength("AES");
            System.out.println(maxKeyLen);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        try
        {
            key = Arrays.copyOf(key, maxKeyLen);
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
            final SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);

            byte[] plainText = new byte[cipher.getOutputSize(ctLength)];

            int ptLength = cipher.update(strToDecrypt.getBytes(), 0, ctLength, plainText, 0);

            ptLength += cipher.doFinal(plainText, ptLength);

            System.out.println(new String(plainText));
            
            
            /*KeyGenerator kgen = KeyGenerator.getInstance("AES");
            kgen.init(128);
            final SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);*/
            final String decryptedString = new String(plainText);//new String(cipher.doFinal(new BASE64Decoder().decodeBuffer(strToDecrypt)));
            return decryptedString;
        }
        catch (Exception e)
        {
             System.out.println(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("ERROR WHILE DECRYPTING ")+ e);
        }
        return null;
    }
    public Long id1;
    public Long id2;
    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        
    }//GEN-LAST:event_jMenuItem2ActionPerformed

//calculate the deadline with the totalEffectiveBalance from getState:
//chance_to_forge = AccountEffectiveBalance / totalEffectiveBalance.Value * 100

    private void germanMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_germanMenuItemActionPerformed
        prefs.put(PREF_language, "de");
    }//GEN-LAST:event_germanMenuItemActionPerformed

    private void checkNxtUpdateMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkNxtUpdateMenuActionPerformed
        //checklatest();
        //LogGui(prefs.get(PREF_version, "0.0.0"));
        //LogGui(nxt.Nxt.VERSION);
        prefs.put(PREF_version, nxt.Nxt.VERSION);
        SwingWorker<String,String> checking = new SwingWorker<String, String>() {

            @Override
            protected String doInBackground() throws Exception {
                String stat = "";
                org.jsoup.nodes.Document doc = null;
                String URL = "http://download.nxtcrypto.org/";
                try {
                    LogGui(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("CONNECTING..."));
                    doc = Jsoup.connect(URL).timeout(0).get();
                } catch (IOException ex) {
                    LogGui(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("DISCONNECTED... PLEASE MAKE SURE YOU ARE CONNECTED TO THE INTERNET!"));
                    //LogGui(ex.getMessage());
                }

                List<Version> versions = new ArrayList<Version>();
                String name = null;
                String linker = null;
                String latestVersion;
                // get all links
                Elements links = doc.select("a[href]");
                int length = links.size();
                int totalDataRead = 0;
                for (Element link : links) {
                        // get the value from href attribute
                    if (link.attr("href").endsWith("zip")){
                        linker = link.attr("href");
                        name = link.attr("href").substring(11, linker.length()-4);
                        if (!name.endsWith("e"))
                            versions.add(new Version(name));
                        stat = java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("CHECKING VERSION...");
                        publish(stat);
                        totalDataRead=totalDataRead+1;
                        float Percent=(totalDataRead*100)/length;
                        jProgressBar1.setValue((int)Percent);
                    }
                }
                stat = java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("VERSION CHECK DONE!");
                publish(stat);
                jProgressBar1.setValue((int)100);
                latestVersion = Collections.max(versions).get();
                String myVersion = prefs.get(PREF_version, "0.0.0");

                if (myVersion.equals(latestVersion)){
                    JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("YOU HAVE THE LATEST VERSION ")+latestVersion+java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString(" OF NXT!"));
                }else{
                      int result = JOptionPane.showConfirmDialog(
                                        null, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("YOUR VERSION: ")+myVersion+java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString(" LATEST VERSION: ")+latestVersion+"\n"+java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("DO YOU WISH TO DOWNLOAD LATEST VERSION?"), 
                                        java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("CHECKING LATEST VERSION OF NXT..."), JOptionPane.YES_NO_OPTION);
                        if (result == JOptionPane.YES_OPTION){
                            File file = new File(LIBFolder+"nxt");
                            if (file.isDirectory()) {
                                try {
                                    stat = java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("DELETING FOLDER...");
                                    publish(stat);
                                    deleteFile(file.getPath());
                                    //deletefiles(file);
                                } catch (IOException ex) {
                                    Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                            try {
                                stat = java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("DOWNLOADING...");
                                publish(stat);
                                download(URL+linker, LIBFolder+linker);
                                prefs.put(PREF_version, latestVersion);
                            } catch (IOException ex) {
                                Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            LogGui(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("NXT CORE BEING UPDATED"));
                            stat = java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("EXTRACTING ARCHIVE...");
                            publish(stat);
                            unZipIt(LIBFolder+linker,LIBFolder);
                            
                            renamefiles(NXTFolder+"nxt.jar", LIBFolder+"nxt-updated.jar");
                            /*nxt.util.Logger.logMessage("Delete old nxt.jar");
                            deleteFile(LIBFolder+"nxt.jar");
                            nxt.util.Logger.logMessage("Move new nxt.jar");
                            copyFile(NXTFolder+"nxt.jar", LIBFolder+"nxt.jar");
                            nxt.util.Logger.logMessage("Delete unneccesary files from new Nxt");
                            deleteFile(LIBFolder+"nxt");*/
                            
                            stat = java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("UPDATED NXT CORE!");
                            LogGui(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("UPDATED TO ")+nxt.Nxt.VERSION+java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString(". CHANGES WILL TAKE EFFECT AFTER RE-START!"));
                            publish(stat);
                        }
                    }
                return stat;
            }

            @Override
            protected void done() {
                try {
                    String text = get();
                    if (text != null)
                        statusbarLabel.setText(text);
                } catch (InterruptedException ex) {
                    LogGui("Interrupted exception, task was interrupted!");
                } catch (ExecutionException ex) {
                    LogGui("Execution exception, task can't be finished!");
                }
                
            }

            @Override
            protected void process(List<String> chunks) {
                statusbarLabel.setText(chunks.get(chunks.size()-1));
                LogGui(chunks.get(chunks.size()-1));
            }
            
        };
        checking.execute();
        
    }//GEN-LAST:event_checkNxtUpdateMenuActionPerformed
    private void copyFile(String oldFilePath, String newFilePath){
        try {
            // Get a file channel for the file
            File file = new File(oldFilePath);
            FileChannel channel = new RandomAccessFile(file, "rw").getChannel();

            // Use the file channel to create a lock on the file.
            // This method blocks until it can retrieve the lock.
            FileLock lock = channel.lock();

            // Try acquiring the lock without blocking. This method returns
            // null or throws an exception if the file is already locked.
            try {
                lock = channel.tryLock();
            } catch (OverlappingFileLockException e) {
                // File is already locked in this thread or virtual machine
            }
            File fil = new File(oldFilePath);
            if (fil.renameTo(new File(newFilePath)))
                System.out.println(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("FILE IS MOVED!"));
            else
                System.out.println(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("FILE IS FAILED TO MOVE!"));
            // Release the lock
            lock.release();

            // Close the file
            channel.close();
        } catch (Exception e) {
        }
    }
    private void deleteFile(String filePath) throws IOException {
        File f = new File(filePath);
        if (f.isDirectory()) {
          for (File c : f.listFiles())
            deleteFile(c.getPath());
        }
        if (!f.delete())
          throw new FileNotFoundException("Failed to delete file: " + f);
    }
    private class Version implements Comparable<Version> {

        private String version;

        public final String get() {
            return this.version;
        }

        public Version(String version) {
            if(version == null)
                throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("VERSION CAN NOT BE NULL"));
            if(!version.matches("[0-9]+(\\.[0-9]+)*"))
                throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("INVALID VERSION FORMAT"));
            this.version = version;
        }

        @Override public int compareTo(Version that) {
            if(that == null)
                return 1;
            String[] thisParts = this.get().split("\\.");
            String[] thatParts = that.get().split("\\.");
            int length = Math.max(thisParts.length, thatParts.length);
            for(int i = 0; i < length; i++) {
                int thisPart = i < thisParts.length ?
                    Integer.parseInt(thisParts[i]) : 0;
                int thatPart = i < thatParts.length ?
                    Integer.parseInt(thatParts[i]) : 0;
                if(thisPart < thatPart)
                    return -1;
                if(thisPart > thatPart)
                    return 1;
            }
            return 0;
        }

        @Override public boolean equals(Object that) {
            if(this == that)
                return true;
            if(that == null)
                return false;
            if(this.getClass() != that.getClass())
                return false;
            return this.compareTo((Version) that) == 0;
        }

    }
    static int getEpochTime(long time) {
        Calendar calendar = Calendar.getInstance();
        long epochBeginning = calendar.getTimeInMillis();
        return (int)((time - epochBeginning + 500) / 1000);
    }
    private String getAccountId(String secretPhrase){
        String accountId = null;
        String accAPI = "nxt?requestType=getAccountId&secretPhrase=";
        try {
            accAPI += URLEncoder.encode(secretPhrase, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        String str = null;
        try {
            str = urlExec(globalURL,accAPI);
        } catch (IOException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        JSONParser parser = new JSONParser();
        Object obj = null;
        try {
            obj = parser.parse(str);
        } catch (ParseException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        JSONObject jsonObject = (JSONObject) obj;
        if (jsonObject != null){
            accountId = (String) jsonObject.get("accountId");
        }
        return accountId;
    }
    private String generateToken(String websites, String secretPhrase){
        byte[] website = null;
        try {
            website = websites.getBytes("UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        byte[] data = new byte[website.length + 32 + 4];
        System.arraycopy(website, 0, data, 0, website.length);
        /*String pubkey = null;
        try {
            pubkey = getPublicKey(secretPhrase);//getPubKey(getAccountId(secretPhrase));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        System.arraycopy(Crypto.getPublicKey(secretPhrase), 0, data, website.length, 32);
        int timestamp = getEpochTime(System.currentTimeMillis());
        data[website.length + 32] = (byte)timestamp;
        data[website.length + 32 + 1] = (byte)(timestamp >> 8);
        data[website.length + 32 + 2] = (byte)(timestamp >> 16);
        data[website.length + 32 + 3] = (byte)(timestamp >> 24);
        byte[] token = new byte[100];
        System.arraycopy(data, website.length, token, 0, 32 + 4);
        System.arraycopy(Crypto.sign(data, secretPhrase), 0, token, 32 + 4, 64);
        String tokenString = "";
        for (int ptr = 0; ptr < 100; ptr += 5) {
            long number = ((long)(token[ptr] & 0xFF)) | (((long)(token[ptr + 1] & 0xFF)) << 8) | (((long)(token[ptr + 2] & 0xFF)) << 16) | (((long)(token[ptr + 3] & 0xFF)) << 24) | (((long)(token[ptr + 4] & 0xFF)) << 32);
            if (number < 32) {
                    tokenString += "0000000";
            } else if (number < 1024) {
                    tokenString += "000000";
            } else if (number < 32768) {
                    tokenString += "00000";
            } else if (number < 1048576) {
                    tokenString += "0000";
            } else if (number < 33554432) {
                    tokenString += "000";
            } else if (number < 1073741824) {
                    tokenString += "00";
            } else if (number < 34359738368L) {
                    tokenString += "0";
            }
            tokenString += Long.toString(number, 32);
        }
        return tokenString;
    }
    private void generateTokenButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generateTokenButtonActionPerformed
        generateTokenDialog.setSize(360, 220);
        generateTokenDialog.setLocationRelativeTo(null);
        generateTokenDialog.setTitle(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("GENERATING TOKEN"));
        generateTokenDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        generateTokenDialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        
        //tokenTextPane.getDocument().addDocumentListener(new MyDocumentListener());
        
        generateTokenDialog.setVisible(true);
    }//GEN-LAST:event_generateTokenButtonActionPerformed

    private void tokenPassFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tokenPassFieldActionPerformed
        tokenTextPane.setText(generateToken(websiteField.getText(), tokenPassField.getText()));
        
    }//GEN-LAST:event_tokenPassFieldActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
         tokenTextPane.setText(generateToken(websiteField.getText(), tokenPassField.getText()));
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        String[] data = decodeToken(websiteField.getText(), tokenTextPane.getText());
            if (data.length>0)
                tokenAccountIdLabel.setText(data[0]+" is "+data[2]);
            else
                tokenAccountIdLabel.setText(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("INVALID ACCOUNT"));
    }//GEN-LAST:event_jButton3ActionPerformed

    private void qrCodeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_qrCodeButtonActionPerformed
        
        if (AccountsTable.getSelectedRowCount() == 0)
            JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("PLEASE SELECT THE ACOUNT!"));
        else{
        String qrCodeData = (String) AccountsTable.getValueAt(AccountsTable.getSelectedRow(), 1);
        //String qrCodeData = "Hello World!";
        
        String filePath = "QRCode.png";
        String charset = "UTF-8"; // or "ISO-8859-1"
        Map<EncodeHintType, ErrorCorrectionLevel> hintMap = new HashMap<EncodeHintType, ErrorCorrectionLevel>();
        hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
        try {
            createQRCode(qrCodeData, filePath, charset, hintMap, 200, 200);
        } catch (WriterException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        //System.out.println("QR Code image created successfully!");
        /*try {
            System.out.println("Data read from QR Code: " + readQRCode(filePath, charset, hintMap));
        } catch (IOException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NotFoundException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        
        BufferedImage image = null;
        try {
            image = ImageIO.read(new FileInputStream(filePath));
        } catch (IOException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        JFrame frame = new JFrame();
        frame.setSize(200, 250);
        JLabel labelText = new JLabel();
        //labelText.setVerticalAlignment(JLabel.BOTTOM);
        try {
            labelText.setText(readQRCode(filePath, charset, hintMap));
        } catch (IOException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NotFoundException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        frame.getContentPane().setLayout(new CardLayout());
        frame.getContentPane().add(new JLabel(new ImageIcon(image)));
        frame.getContentPane().add(labelText);
        frame.setTitle(qrCodeData);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        }
    }//GEN-LAST:event_qrCodeButtonActionPerformed

    private void createPollsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createPollsButtonActionPerformed
        createPollDialog.setSize(460, 460);
        createPollDialog.setLocationRelativeTo(null);
        createPollDialog.setTitle(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("CREATE POLL"));
        createPollDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        createPollDialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        createPollDialog.setVisible(true);
    }//GEN-LAST:event_createPollsButtonActionPerformed

    private void voteMaxFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_voteMaxFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_voteMaxFieldActionPerformed

    private void creatPollButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_creatPollButtonActionPerformed
        String pollAPI = "nxt?requestType=createPoll&secretPhrase=";
        try {   
            pollAPI += URLEncoder.encode(voteSecretPassField.getText(), "UTF-8");
            pollAPI += "&name=";
            pollAPI += URLEncoder.encode(voteNameField.getText(), "UTF-8");
            pollAPI += "&description=";
            pollAPI += URLEncoder.encode(voteDescriptionField.getText(), "UTF-8");
            pollAPI += "&minNumberOfOptions=";
            pollAPI += URLEncoder.encode(voteMinField.getText(), "UTF-8");
            pollAPI += "&maxNumberOfOptions=";
            pollAPI += URLEncoder.encode(voteMaxField.getText(), "UTF-8");
            pollAPI += "&optionsAreBinary=";
            pollAPI += URLEncoder.encode(voteOptionsAreBinaryComboBox.getSelectedItem().toString(), "UTF-8");
            String options = "";
            String[] lines = voteOptionsField.getText().split("\n");
            if (lines.length<2)
                JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("AT LEAST 2 NUMBER OF OPTIONS ARE REQUIRED!"));
            else{
                for(int i=0; i < lines.length; i++){
                    options += "&option"+i+"="+lines[i];
                }
            }
            pollAPI += options;
            pollAPI += "&fee=";
            pollAPI += URLEncoder.encode(defaultFee.getText(), "UTF-8");
            pollAPI += "&deadline=";
            pollAPI += URLEncoder.encode(defaultDeadline.getText(), "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        String str = null;
        try {
            str = urlExec(globalURL,pollAPI);
        } catch (IOException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        statusbarLabel.setText(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("CREATED POLL"));
        consoleOutput.append("["+nowtime()+"] "+java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("CREATE POLL: ")+str);
        voteOptionsField.setText("");
        voteNameField.setText("");
        voteDescriptionField.setText("");
        voteSecretPassField.setText("");
        createPollDialog.dispose();
    }//GEN-LAST:event_creatPollButtonActionPerformed

    private void castVoteSubmitButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_castVoteSubmitButtonActionPerformed
        String voteAPI = "nxt?requestType=castVote&secretPhrase=";
        try {
            voteAPI += URLEncoder.encode(castVotePassField.getText(), "UTF-8");
            voteAPI += "&poll=";
            voteAPI += URLEncoder.encode(castVoteIdLabel.getText(), "UTF-8");
            voteAPI += "&fee=";
            voteAPI += URLEncoder.encode(defaultFee.getText(), "UTF-8");
            voteAPI += "&deadline=";
            voteAPI += URLEncoder.encode(defaultDeadline.getText(), "UTF-8");
            voteAPI += "&vote"+castVoteOptionComboBox.getSelectedIndex()+"=1";
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        String str = null;
        try {
            str = urlExec(globalURL,voteAPI);
        } catch (IOException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        statusbarLabel.setText(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("VOTED"));
        consoleOutput.append("["+nowtime()+"] "+java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("CAST VOTE: ")+str);
        
        castVotePassField.setText("");
        castVoteDialog.dispose();
    }//GEN-LAST:event_castVoteSubmitButtonActionPerformed

    private void castVoteMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_castVoteMenuItemActionPerformed
        int index = allPollsTable.getSelectedRow();
        castVoteOptionComboBox.removeAllItems();
        if (index >= 0){
            castVoteIdLabel.setText(allPollsTable.getValueAt(index, 4).toString());
            String Options = (String) allPollsTable.getValueAt(index, 2);
            //String Voters = (String) allPollsTable.getValueAt(index, 3);
            //xsxs
            if (Options.endsWith(":")){
                StringTokenizer token = new StringTokenizer(Options.substring(0, Options.length()-1), ":");
                while (token.hasMoreTokens()) {
                    castVoteOptionComboBox.addItem(token.nextToken());
                //op(iterator.next());
                }            
            }
                
            castVoteDialog.setSize(360, 220);
            castVoteDialog.setLocationRelativeTo(null);
            castVoteDialog.setTitle(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("CAST YOUR VOTE"));
            castVoteDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            castVoteDialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
            castVoteDialog.setVisible(true);
        }else
            JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("PLEASE SELECT A POLL!"));
    }//GEN-LAST:event_castVoteMenuItemActionPerformed

    private void portugueseMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_portugueseMenuItemActionPerformed
        prefs.put(PREF_language, "pt");
    }//GEN-LAST:event_portugueseMenuItemActionPerformed

    private void getPollsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_getPollsButtonActionPerformed
        allPollsTable.removeAll();
        VoteWorker mainWorker = new VoteWorker(allPollsTable);
        mainWorker.execute(); 
    }//GEN-LAST:event_getPollsButtonActionPerformed

    private void spanishMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_spanishMenuItemActionPerformed
        prefs.put(PREF_language, "es");
    }//GEN-LAST:event_spanishMenuItemActionPerformed

    private void forgePassFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_forgePassFieldActionPerformed
   
        if (secretFlag == 0){
            final nxt.Generator check;
            int ded;

            if (AccountsTable.getValueAt(AccountsTable.getSelectedRow(), 1).equals(getAccountId(forgePassField.getText()))){
                check = nxt.Generator.startForging(forgePassField.getText());
                Generators.set(AccountsTable.getSelectedRow(), check);

                if (check != null){
                    ded = (int)check.getDeadline();
                }else
                    ded = 0;
                tableModelA.updateAccount(null,null,null,(int) ded, AccountsTable.getSelectedRow());
            }            
            else
                JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("SECRETPHRASE AND ACCOUNTID ARE NOT MATCH!"));
        }
        
        if (secretFlag == 1){
            int index = messagesTable.getSelectedRow();
            try {
                byte[] pvkey = Crypto.getPrivateKey(forgePassField.getText());
                Long num = nxt.util.Convert.parseUnsignedLong(messagesTable.getValueAt(index, 0).toString());
                if (getAccountId(forgePassField.getText()).equals(messagesTable.getValueAt(index, 0).toString()))
                    num = nxt.util.Convert.parseUnsignedLong(messagesTable.getValueAt(index, 1).toString());
                byte[] pubkey = nxt.Account.getAccount(num).getPublicKey();
                String pltxt = Crypto.decryptMessage(nxt.util.Convert.parseHexString(messagesTable.getValueAt(index, 2).toString()), pvkey, pubkey);
                messageContentField.setText(pltxt);
                //messageContentField.setText(Crypto.decodeMessage(convertHexToString(messagesTable.getValueAt(index, 2).toString()).getBytes("UTF-8"), forgePassField.getText(), getPubKey(messagesTable.getValueAt(index, 0).toString()).getBytes("UTF-8")));
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
            }
            secretFlag = 0;
        }
        forgePassField.setText("");
        forgeDialog.dispose();
    }//GEN-LAST:event_forgePassFieldActionPerformed

    private void forgeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_forgeButtonActionPerformed

        if (AccountsTable.getSelectedRowCount()>0){
            forgeDialog.setSize(310, 75);
            forgeDialog.setLocationRelativeTo(null);
            forgeDialog.setTitle(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("ENTER SECRETPHRASE!"));
            forgeDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            forgeDialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
            forgeDialog.setVisible(true);
        }else
            JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("SELECT THE ACCOUNT YOU WANT TO FORGE WITH!"));
    }//GEN-LAST:event_forgeButtonActionPerformed

    private void masterPasswordFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_masterPasswordFieldActionPerformed
        //xcxc
            
    }//GEN-LAST:event_masterPasswordFieldActionPerformed

    private void changeMasterPasswordFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_changeMasterPasswordFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_changeMasterPasswordFieldActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        if (AccountsTable.getSelectedRowCount()>0){
            masterFlag = 0;        
            masterPasswordDialog.setSize(310, 75);
            masterPasswordDialog.setLocationRelativeTo(null);
            masterPasswordDialog.setTitle(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("ENTER MASTERPASSWORD!"));
            masterPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            masterPasswordDialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
            masterPasswordDialog.setVisible(true);
        }else
            JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("SELECT THE ACCOUNT YOU WANT TO GET SECRETPHRASE OF!"));
    }//GEN-LAST:event_jButton8ActionPerformed
    private int masterFlag = 0;
    private int secretFlag = 0;
    private void masterPassowordFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_masterPassowordFieldActionPerformed
        
        switch (masterFlag){
            case -1: //set masterPass and generate account
                if (masterPassowordField.getText().length()>0){
                    deleteDbRecords();
                    prefs.put(PREF_masterKey, getSHA256(masterPassowordField.getText()));
                    generateNewAccount(masterPassowordField.getText());
                }else
                    JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("MASTER PASSWORD CANNOT BE EMPTY!"));
                masterFlag = 99;
                break;
            case 0: //decrypt secretPhrase button
                if (masterPassowordField.getText().length()>0){
                //JCTable accountsA = new JCTable("lib/accounts");
                String accountii = AccountsTable.getValueAt(AccountsTable.getSelectedRow(), 1).toString();
                Vector data = readRecord(accountii);
                
                if (data.size() > 0){
                    Vector row = (Vector) data.get(0);
                    String encodedPhrase = row.get(3).toString();
                    String masterTry = null;
                    if (encodedPhrase != null){
                        try {
                            masterTry = getSHA256(masterPassowordField.getText());//getMD5(masterPassowordField.getText());//getSHA256(masterPassowordField.getText());////PBKDF2.getSaltedHash(masterPassowordField.getText()); //
                        } catch (Exception ex) {
                            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        try {
                            if (!masterTry.equals(prefs.get(PREF_masterKey, null))) //PBKDF2.check(masterKey, prefs.get(PREF_masterKey, ""))
                                JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("INCORRECT MASTER PASSWORD!"));
                            else {
                                String sp = aes.decrypt(encodedPhrase, masterPassowordField.getText());//decryptAES(masterPassowordField.getText().getBytes(), encodedPhrase);////decryptAES(getSHA256(masterPassowordField.getText()).getBytes(), encodedPhrase,ctLength0);//decryptAES(masterTry.getBytes(), encodedPhrase);
                                //String sp = DeEncrypter.getInstance(masterPassowordField.getText().getBytes()).decrypt(encodedPhrase);
                                JLabel lp = new JLabel(sp);
                                JOptionPane.showMessageDialog(null, lp, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("SECRETPHRASE OF SELECTED ACCOUNT!"), JOptionPane.INFORMATION_MESSAGE);
                                //System.out.println(sp);
                            }
                        } catch (Exception ex) {
                            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }else
                        JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("SECRETPHRASE IS NOT SAVED!"));
                }else
                    JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("ACCOUNT DOESN'T HAVE SECRETPHRASE SAVED IN DATABASE!"));                
                }else
                    JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("MASTER PASSWORD CANNOT BE EMPTY!"));
                masterFlag = 99;
                break;
            case 1: //
                if (masterPassowordField.getText().length()>0){
                    if (getSHA256(masterPassowordField.getText()).equals(prefs.get(PREF_masterKey, null)))
                        generateNewAccount(masterPassowordField.getText());
                    else
                        JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("MASTER PASSWORD DOESN'T MATCH!"));
                }
                else
                    JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("MASTER PASSWORD CANNOT BE EMPTY!"));
                masterFlag = 99;
                break;
            case 2:
                if (masterPassowordField.getText().length()>0){
                    if (getSHA256(masterPassowordField.getText()).equals(prefs.get(PREF_masterKey, null)))
                        mpass = masterPassowordField.getText();
                    else
                        JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("MASTER PASSWORD DOESN'T MATCH!"));
                }
                else
                    JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("MASTER PASSWORD CANNOT BE EMPTY!"));
                masterFlag = 99;
                break;
            case 3:
                if (masterPassowordField.getText().length()>0){
                    if (getSHA256(masterPassowordField.getText()).equals(prefs.get(PREF_masterKey, null))){
                        String enc = null;
                        try {
                            enc = aes.encrypt(accountPassField.getText(), masterPassowordField.getText());
                        } catch (NoSuchAlgorithmException ex) {
                            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (NoSuchProviderException ex) {
                            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (NoSuchPaddingException ex) {
                            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (InvalidKeyException ex) {
                            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IllegalBlockSizeException ex) {
                            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (BadPaddingException ex) {
                            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (UnsupportedEncodingException ex) {
                            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        try {
                            insertRecordIntoDbAccountsTable(labelofAccount.getText(), sapId, sapBl, enc, "1");
                        } catch (SQLException ex) {
                            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }else
                        JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("MASTER PASSWORD DOESN'T MATCH!"));
                }
                else
                    JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("MASTER PASSWORD CANNOT BE EMPTY!"));
                masterFlag = 99;
                break;
        }
        masterPassowordField.setText("");
        masterPasswordDialog.dispose();
    }//GEN-LAST:event_masterPassowordFieldActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        masterFlag = 1;
        masterPasswordDialog.setSize(310, 75);
        masterPasswordDialog.setLocationRelativeTo(null);
        masterPasswordDialog.setTitle(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("ENTER MASTERPASSWORD!"));
        masterPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        masterPasswordDialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        masterPasswordDialog.setVisible(true);
    }//GEN-LAST:event_jButton9ActionPerformed

    private void watchAccountNumberFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_watchAccountNumberFieldActionPerformed
        if (watchAccountNumberField.getText().length()>0){
            Long accb = null;
            try {
                accb = getBalance(watchAccountNumberField.getText());
            } catch (IOException ex) {
                Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
            }

            try {
                insertRecordIntoDbAccountsTable(labelWatchAccountField.getText(), watchAccountNumberField.getText(), accb.toString(), null, "0");
            } catch (SQLException ex) {
                Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
            }

            nxt.Generator ww;
            ww = nxt.Generator.startForging("");
            Generators.add(ww);
            tableModelA.addAccount(new AccountGUI(labelofAccount.getText(), watchAccountNumberField.getText(), accb.toString(), 0));
        }
        watchAccountNumberField.setText("");
        labelWatchAccountField.setText("");
        watchAccountDialog.dispose();
    }//GEN-LAST:event_watchAccountNumberFieldActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        watchAccountDialog.setSize(380, 140);
        watchAccountDialog.setLocationRelativeTo(null);
        watchAccountDialog.setVisible(true);
        watchAccountDialog.setTitle(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("ADD ACCOUNT NUMBER TO WATCH!"));
        watchAccountDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        watchAccountDialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    }//GEN-LAST:event_jButton10ActionPerformed

    private void proxyManRBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_proxyManRBoxItemStateChanged
        if (proxyManRBox.isSelected()){
            proxyHostField.setEnabled(true);
            proxyPortField.setEnabled(true);
        }else{
            proxyHostField.setEnabled(false);
            proxyPortField.setEnabled(false);
        }
            
    }//GEN-LAST:event_proxyManRBoxItemStateChanged

    private void attachMessageCheckBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_attachMessageCheckBoxItemStateChanged
        if (attachMessageCheckBox.isSelected()){
            jScrollPane3.setEnabled(true);
            inputMessageField.setEnabled(true);
            //inputMessageField.setSize(164, 94);
        }
        else {
            jScrollPane3.setEnabled(false);
            inputMessageField.setEnabled(false);
            //inputMessageField.setSize(164, 94);
        }
    }//GEN-LAST:event_attachMessageCheckBoxItemStateChanged

    private void messagesButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_messagesButtonActionPerformed
                // snd
        SendPanel.setVisible(false);
        VotingPanel.setVisible(false);
        AssetExPanel.setVisible(false);
        jScrollPane3.setEnabled(false);
        inputMessageField.setEnabled(false);
        
        ConsolePanel.setVisible(false);
        TransactionsPanel.setVisible(false);
        ReceivePanel.setVisible(false);
        AliasesPanel.setVisible(false);
        MessagesPanel.setVisible(true);
        BlocksPanel.setVisible(false);
        OverviewPanel.setVisible(false);
        if (MessagesPanel.isVisible()){
            //JCTable accountsA = new JCTable("lib/accounts");
            messageSenderComboBox.removeAllItems();
            //JCRow[] acclist = accountsA.select("Flag","=", "1");
            Vector acc = readAccounts("1");
            for (int i=0; i<acc.size(); i++){
                messageSenderComboBox.addItem(acc.get(i).toString());
            }
        }
    }//GEN-LAST:event_messagesButtonActionPerformed

    private void messageSenderComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_messageSenderComboBoxActionPerformed
        if (MessagesPanel.isVisible()){
            if (consoleOutput.getText().trim().length() != 0){
                if (messageSenderComboBox.getItemCount() > 0){
                    getMessages(messageSenderComboBox.getSelectedItem().toString());
                }
            }
        }
    }//GEN-LAST:event_messageSenderComboBoxActionPerformed

    private void messageSendButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_messageSendButtonActionPerformed
        //xsxsxs
        Long defaccb = null;
        String sendMsgAPI;
        sendMsgAPI = "nxt?requestType=sendMessage&secretPhrase=";
        try {
            sendMsgAPI += URLEncoder.encode(messageSecretPhraseField.getText(), "UTF-8");
            sendMsgAPI += "&recipient=";
            sendMsgAPI += URLEncoder.encode(messageRecipientField.getText(), "UTF-8");
            sendMsgAPI += "&fee=";
            sendMsgAPI += URLEncoder.encode(defaultFee.getText(), "UTF-8");
            
            Long accs = nxt.util.Convert.parseUnsignedLong(messageRecipientField.getText());
            byte[] pvkey = Crypto.getPrivateKey(messageSecretPhraseField.getText());
            byte[] pubkey = nxt.Account.getAccount(accs).getPublicKey();
            byte[] encryptedtext = Crypto.encryptMessage(messageContentField.getText(), pvkey, pubkey);
            
            //byte[] mm = Crypto.encodeMessage(messageContentField.getText(), messageSecretPhraseField.getText(), getPubKey(messageRecipientField.getText()).getBytes("UTF-8"));
            sendMsgAPI += "&message=";
            
            sendMsgAPI += URLEncoder.encode(nxt.util.Convert.toHexString(encryptedtext), "UTF-8");
            //System.out.println(stringToHex(new String(mm)) + " size "+ stringToHex(new String(mm)).length());
            sendMsgAPI += "&deadline=";
            sendMsgAPI += URLEncoder.encode(defaultDeadline.getText(), "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            defaccb = getBalance(messageSenderComboBox.getSelectedItem().toString());
        } catch (IOException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        String strMsg;
        try {
            if (defaccb < 1){
                JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("ACCOUNT BALANCE MUST BE MORE THAN 1 NXT"));
            }
            else{
                strMsg = urlExec(globalURL,sendMsgAPI);
                LogGui(java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("SENT MSG :")+strMsg);
                statusbarLabel.setText(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("MSG SENT..."));
                messageContentField.setText("");
                messageSecretPhraseField.setText("");
            }
        } catch (IOException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_messageSendButtonActionPerformed

    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton11ActionPerformed
        int index = messagesTable.getSelectedRowCount();
        if (index > 0){
            secretFlag = 1;
            forgeDialog.setSize(310, 75);
            forgeDialog.setLocationRelativeTo(null);
            forgeDialog.setTitle(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("ENTER SECRETPHRASE!"));
            forgeDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            forgeDialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
            forgeDialog.setVisible(true);
        }else
            JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("PLEASE SELECT A MESSAGE ROW!"));
    }//GEN-LAST:event_jButton11ActionPerformed

    private void messagesTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_messagesTableMouseClicked
        int index = messagesTable.getSelectedRowCount();
        if (index > 0){
            try {
                //messageContentField.setText(convertHexToString(messagesTable.getValueAt(messagesTable.getSelectedRow(), 2).toString()));
                messageContentField.setText(new String(nxt.util.Convert.parseHexString(messagesTable.getValueAt(messagesTable.getSelectedRow(), 2).toString()), "UTF-8"));
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_messagesTableMouseClicked
    private void copySenderAddress(JTable table){
        if (table.getSelectedRowCount()==1){
            StringSelection selection = new StringSelection(table.getValueAt(table.getSelectedRow(), 0).toString());
            Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
            clipboard.setContents(selection, selection);
        }else
            JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("PLEASE SELECT THE DESIRED ROW!"));
    }
    private void copyRecipientAddress(JTable table){
        if (table.getSelectedRowCount()==1){
            StringSelection selection = new StringSelection(table.getValueAt(table.getSelectedRow(), 1).toString());
            Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
            clipboard.setContents(selection, selection);
        }else
            JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("PLEASE SELECT THE DESIRED ROW!"));
    }
    private void copySenderAddressCMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_copySenderAddressCMenuActionPerformed
        if (transactionsButton.isSelected())
            copySenderAddress(transactionsTable);
        if (messagesButton.isSelected())
            copySenderAddress(messagesTable);
    }//GEN-LAST:event_copySenderAddressCMenuActionPerformed

    private void copyRecipientAddressCMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_copyRecipientAddressCMenuActionPerformed
        if (transactionsButton.isSelected())
            copyRecipientAddress(transactionsTable);
        if (messagesButton.isSelected())
            copyRecipientAddress(messagesTable);
    }//GEN-LAST:event_copyRecipientAddressCMenuActionPerformed
    private void sendNxtToSender(JTable table){
        if (table.getSelectedRowCount()==1){
            receiverAddressField.setText(table.getValueAt(table.getSelectedRow(), 0).toString());
            sendButton.setSelected(true);
            SendPanel.setVisible(true);
        }else
            JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("PLEASE SELECT THE DESIRED ROW!"));
    }
    private void sendNxtToSenderAddressCMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendNxtToSenderAddressCMenuActionPerformed
        if (transactionsButton.isSelected()){
            sendNxtToSender(transactionsTable);
            TransactionsPanel.setVisible(false);
        }
        if (messagesButton.isSelected()){
            sendNxtToSender(messagesTable);
            MessagesPanel.setVisible(false);
        }
    }//GEN-LAST:event_sendNxtToSenderAddressCMenuActionPerformed
    private void sendNxtToRecipient(JTable table){
        if (table.getSelectedRowCount()==1){
            receiverAddressField.setText(table.getValueAt(table.getSelectedRow(), 1).toString());
            sendButton.setSelected(true);
            SendPanel.setVisible(true);
        }else
            JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("PLEASE SELECT THE DESIRED ROW!"));
    }
    private void sendNxtToRecipientAddressCMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendNxtToRecipientAddressCMenuActionPerformed
        if (transactionsButton.isSelected()){
            sendNxtToRecipient(transactionsTable);
            TransactionsPanel.setVisible(false);
        }
        if (messagesButton.isSelected()){
            sendNxtToRecipient(messagesTable);
            MessagesPanel.setVisible(false);
        }
    }//GEN-LAST:event_sendNxtToRecipientAddressCMenuActionPerformed
    private void sendMsgToSender(JTable table){
        if (table.getSelectedRowCount()==1){
            messageRecipientField.setText(table.getValueAt(table.getSelectedRow(), 0).toString());
            messagesButton.setSelected(true);
            MessagesPanel.setVisible(true);
        }else
            JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("PLEASE SELECT THE DESIRED ROW!"));
    }
    private void sendMsgToSenderAddressCMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendMsgToSenderAddressCMenuActionPerformed
        if (transactionsButton.isSelected()){
            sendMsgToSender(transactionsTable);
            TransactionsPanel.setVisible(false);
        }
        if (messagesButton.isSelected()){
            sendMsgToSender(messagesTable);
        }
    }//GEN-LAST:event_sendMsgToSenderAddressCMenuActionPerformed
    private void sendMsgToRecipient(JTable table){
        if (table.getSelectedRowCount()==1){
            messageRecipientField.setText(table.getValueAt(table.getSelectedRow(), 1).toString());
            messagesButton.setSelected(true);
            MessagesPanel.setVisible(true);
        }else
            JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("PLEASE SELECT THE DESIRED ROW!"));
    }
    private void sendMsgToRecipientAddressCMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendMsgToRecipientAddressCMenuActionPerformed
        if (transactionsButton.isSelected()){
            sendMsgToRecipient(transactionsTable);
            TransactionsPanel.setVisible(false);
        }
        if (messagesButton.isSelected()){
            sendMsgToRecipient(messagesTable);
        }
    }//GEN-LAST:event_sendMsgToRecipientAddressCMenuActionPerformed
    private DefaultTableModel tableModelm;
    private void getMessages(final String accountId){
        SwingWorker<TableModel, TableModel> getMessagesWork;
        getMessagesWork = new SwingWorker<TableModel, TableModel>() {
            @Override
            protected TableModel doInBackground() throws Exception {
                Object[][] rowData = {};
                Object[] columnNames = {java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("SENDER"), java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("RECIPIENT"),  java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("MESSAGE"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("TIME")};
                tableModelm = new DefaultTableModel(rowData, columnNames);

                //get transactionIds
                String transactionIdsAPI = "nxt?requestType=getAccountTransactionIds&timestamp=0&account=";
                transactionIdsAPI += URLEncoder.encode(accountId, "UTF-8");
                String str = urlExec(globalURL,transactionIdsAPI);
                final List<String> transactions = new ArrayList<String>();
                JSONParser parser3 = new JSONParser();
                Object obj3 = null;
                try {
                    obj3 = parser3.parse(str);
                } catch (ParseException ex) {
                    Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                }
                JSONObject jsonObject3 = (JSONObject) obj3;
                JSONArray transactionIdss = null;
                int length1 = 0;
                int totalDataRead1 = 0;
                jProgressBar1.setValue(0);
                jProgressBar1.setStringPainted(true);
                if (jsonObject3 != null){
                    transactionIdss = (JSONArray) jsonObject3.get("transactionIds");
                    if (transactionIdss != null){
                        Iterator<String> iterator = transactionIdss.iterator();
                        length1 = transactionIdss.size();
                        while (iterator.hasNext()) {
                            transactions.add(iterator.next());
                            totalDataRead1=totalDataRead1+1;
                            float Percent=(totalDataRead1*100)/length1;
                            jProgressBar1.setValue((int)Percent);
                        }
                        //getTransactions(transactions);
                        Timestamp later = Timestamp.valueOf("2013-11-24 13:00:00");
                        try {
                            String message = "";
                            int length = transactions.size();
                            int totalDataRead = 0;
                            jProgressBar1.setValue(0);
                            jProgressBar1.setStringPainted(true);
                            for (int j=0; j<length; j++){
                                String transactionsAPI = "nxt?requestType=getTransaction&transaction=";
                                transactionsAPI += URLEncoder.encode(transactions.get(j).toString(), "UTF-8");
                                String str2 = null;
                                try {
                                    str2 = urlExec(globalURL, transactionsAPI);
                                } catch (IOException ex) {
                                    Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                JSONParser parser = new JSONParser();
                                Object obj = null;
                                try {
                                    obj = parser.parse(str2);
                                } catch (ParseException ex) {
                                    Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                JSONObject jsonObject = (JSONObject) obj;
                                if (jsonObject != null){
                                    String senderId = (String) jsonObject.get("sender");
                                    Long timestamp = (Long) jsonObject.get("timestamp");
                                    TYPE = (Long) jsonObject.get("type");
                                    SUBTYPE = (Long) jsonObject.get("subtype");
                                    String receiver = (String) jsonObject.get("recipient");
                                    
                                    Date sdf = null;
                                    try {
                                        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2013-11-24 13:00:00");
                                    } catch (java.text.ParseException ex) {
                                        Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                    later = new Timestamp(timestamp * 1000 + sdf.getTime());
                                    if (TYPE == 1){
                                        if (SUBTYPE == 0){
                                            JSONObject newJsonObject = (JSONObject) jsonObject.get("attachment");
                                            if (newJsonObject != null){
                                                message = (String) newJsonObject.get("message");
                                                if (message != null)
                                                    message = message;
                                                
                                                totalDataRead=totalDataRead+1;
                                                float Percent=(totalDataRead*100)/length;
                                                jProgressBar1.setValue((int)Percent);
                                                tableModelm.addRow(new Object[]{senderId, receiver, message, later.toString()});    
                                                publish(tableModelm);
                                            }
                                        }     
                                    }
                                }
                            }
                        }catch (UnsupportedEncodingException ex) {
                            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
                //------
                return tableModelm;
            }
            @Override
            protected void done() {
                try {
                    TableModel model = get();
                    if (model != null)
                        messagesTable.setModel(model);
                    jProgressBar1.setValue(100);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                } catch (ExecutionException ex) {
                    Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            @Override
            protected void process(List<TableModel> chunks) {
                messagesTable.setModel(chunks.get(chunks.size()-1));
            }
        };
    getMessagesWork.execute();
    }
    private String getTimestamp(int timestamp){
        Date sdf = null;
        Timestamp later = Timestamp.valueOf("2013-11-24 13:00:00");
        try {
            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2013-11-24 13:00:00");
        } catch (java.text.ParseException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        later = new Timestamp(timestamp * 1000 + sdf.getTime());
        return later.toString();
    }
    public static void createQRCode(String qrCodeData, String filePath,
      String charset, Map hintMap, int qrCodeheight, int qrCodewidth)
      throws WriterException, IOException {
    BitMatrix matrix = new MultiFormatWriter().encode(
        new String(qrCodeData.getBytes(charset), charset),
        BarcodeFormat.QR_CODE, qrCodewidth, qrCodeheight, hintMap);
    MatrixToImageWriter.writeToFile(matrix, filePath.substring(filePath
        .lastIndexOf('.') + 1), new File(filePath));
    
    }
 
  public static String readQRCode(String filePath, String charset, Map hintMap)
      throws FileNotFoundException, IOException, NotFoundException {
    BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(
        new BufferedImageLuminanceSource(
            ImageIO.read(new FileInputStream(filePath)))));
    Result qrCodeResult = new MultiFormatReader().decode(binaryBitmap,
        hintMap);
    return qrCodeResult.getText();
  }
    private String[] decodeToken(String website, String token){
        String decodeAPI = "nxt?requestType=decodeToken&website=";
        String str = null;
        String account = null;
        String timestamp = null;
        String valid = null;
        if (website != null && token != null){
            try {
                decodeAPI += URLEncoder.encode(website, "UTF-8");
                decodeAPI += "&token=";
                decodeAPI += URLEncoder.encode(token, "UTF-8");
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                str = urlExec(globalURL,decodeAPI);
            } catch (IOException ex) {
                Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
            }
            JSONParser parser = new JSONParser();
            Object obj = null;
            try {
                obj = parser.parse(str);
            } catch (ParseException ex) {
                Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
            }
            JSONObject jsonObject = (JSONObject) obj;
            if (jsonObject != null){
                account = (String) jsonObject.get("account");
                timestamp = String.valueOf(jsonObject.get("timestamp"));
                valid = String.valueOf(jsonObject.get("valid"));
            }
        }
        else{
            account = "query";
            timestamp = "is";
            valid = "invalid";
        }
            
        return new String[] {account, timestamp, valid};
    }
    private class MyDocumentListener implements DocumentListener {
        String newline = "\n";
        public void insertUpdate(DocumentEvent e) {
            //updateLog(e, "inserted into");
            String[] data = decodeToken(websiteField.getText(), tokenTextPane.getText());
            if (data.length>0)
                tokenAccountIdLabel.setText(data[0]+"is"+data[2]);
            else
                tokenAccountIdLabel.setText(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("INVALID TOKEN"));
        }
        public void removeUpdate(DocumentEvent e) {
            //updateLog(e, "removed from");
            String[] data = decodeToken(websiteField.getText(), tokenTextPane.getText());
            if (data.length>0)
                tokenAccountIdLabel.setText(data[0]+"is"+data[2]);
            else
                tokenAccountIdLabel.setText(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("INVALID ACCOUNT"));
        }
        public void changedUpdate(DocumentEvent e) {
            //Plain text components do not fire these events
        }

        /*public void updateLog(DocumentEvent e, String action) {
            Document doc = (Document)e.getDocument();
            int changeLength = e.getLength();
            
            
            tokenAccountIdLabel.setText(lang);
            
                //changeLength + " character" +
                //((changeLength == 1) ? " " : "s ") +
                //action + doc.getProperty("name") + "." + newline +
                //"  Text length = " + doc.getLength() + newline);
        }*/
    }
    
    private void getTrades(String assetId){
        
        Object[][] rowData = {};
        Object[] columnNames = {java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("TIME"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("QUANTITY"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("PRICE"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("ASKORDERID"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("BIDORDERID")};
        
        DefaultTableModel tableTradesAssetOrders = new DefaultTableModel(rowData, columnNames);
        
        assetTradesTable.setModel(tableTradesAssetOrders);
        Timestamp newtime = Timestamp.valueOf("2013-11-24 13:00:00");
        
        String tradesAPI = "nxt?requestType=getTrades&asset=";
        try {
            tradesAPI += URLEncoder.encode(assetId, "UTF-8");
            tradesAPI += "&firstIndex=0&lastIndex=1000";
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        String str = null;
        try {
            str = urlExec(globalURL,tradesAPI);
        } catch (IOException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        JSONParser parser = new JSONParser();
        Object obj = null;
        try {
            obj = parser.parse(str);
        } catch (ParseException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        JSONObject jsonObject = (JSONObject) obj;
        jProgressBar1.setValue(0);
        jProgressBar1.setStringPainted(true);
        int totalDataRead = 0;
        int length;
        if (jsonObject != null){
            JSONArray newjsonObject = (JSONArray) jsonObject.get("trades");
            if (newjsonObject != null){
                Iterator<String> iterator = newjsonObject.iterator();
                length = newjsonObject.size();
                while (iterator.hasNext()) {
                    Object inarr = iterator.next();
                    JSONObject injsonObject = (JSONObject) inarr;
                    Long timestamp = (Long) injsonObject.get("timestamp");
                    Long price = (Long) injsonObject.get("price");
                    float prc = price;
                    prc = prc/100;
                    Long quantity = (Long) injsonObject.get("quantity");
                    String bidOrderId = (String) injsonObject.get("bidOrderId");
                    String askOrderId = (String) injsonObject.get("askOrderId");
                    
                    totalDataRead=totalDataRead+1;
                    float Percent=(totalDataRead*100)/length;
                    jProgressBar1.setValue((int)Percent);
                    
                    Date sdf = null;
                    try {
                        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2013-11-24 13:00:00");
                    } catch (java.text.ParseException ex) {
                        Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    newtime = new Timestamp(timestamp * 1000 + sdf.getTime());

                    tableTradesAssetOrders.addRow(new Object[]{newtime, quantity, prc, askOrderId, bidOrderId});
                }
            }
        }
        
    }
    private void cancelAsset(int type){
        String cancelAPI = "nxt?requestType=";
        try {
            if (type == 0)
                cancelAPI += URLEncoder.encode("cancelAskOrder", "UTF-8");
            if (type == 1)
                cancelAPI += URLEncoder.encode("cancelBidOrder", "UTF-8");

            cancelAPI += "&secretPhrase=";
            String pass = cancelPassField.getText();
            cancelAPI += URLEncoder.encode(pass, "UTF-8");
            cancelAPI += "&order=";
            cancelAPI += URLEncoder.encode(idOrderLabel.getText(), "UTF-8");
            cancelAPI += "&fee=";
            cancelAPI += URLEncoder.encode(defaultFee.getText(), "UTF-8");
            cancelAPI += "&deadline=";
            cancelAPI += URLEncoder.encode(defaultDeadline.getText(), "UTF-8");
            //System.out.println(cancelAPI);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        String str = null;
        try {
            str = urlExec(globalURL,cancelAPI);
        } catch (IOException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        consoleOutput.append("\n["+nowtime()+"]"+java.util.ResourceBundle.getBundle("clienxt/Bundle").getString(" ORDER CANCELED TX :")+str);
        statusbarLabel.setText(str);
    }
    
    private void transferAsset(){
        String transerAPI = "nxt?requestType=transferAsset&secretPhrase=";
        try {
            transerAPI += URLEncoder.encode(passphraseAssetField.getText(), "UTF-8");
            transerAPI += "&recipient=";
            transerAPI += URLEncoder.encode(recipientAssetField.getText(), "UTF-8");
            transerAPI += "&asset=";
            transerAPI += URLEncoder.encode(nameAssetComboBox.getSelectedItem().toString(), "UTF-8");
            transerAPI += "&quantity=";
            transerAPI += URLEncoder.encode(quantityAssetField.getText(), "UTF-8");
            transerAPI += "&fee=";
            transerAPI += URLEncoder.encode(defaultFee.getText(), "UTF-8");
            transerAPI += "&deadline=";
            transerAPI += URLEncoder.encode(defaultDeadline.getText(), "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        String str = null;
        try {
            str = urlExec(globalURL,transerAPI);
        } catch (IOException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        consoleOutput.append("\n["+nowtime()+"]"+java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("ASSET TRANSFERED TX :")+str);
        statusbarLabel.setText(str);
    }
    private void getAccountCurrentAssets(String accountId){
        
        Object[][] rowData = {};
        Object[] columnNames = {java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("ACCOUNT LABEL"), java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("ASSET NAME"), java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("QUANTITY")};
        DefaultTableModel tableoverviewModel = new DefaultTableModel(rowData, columnNames);
        overviewAssetTable.setModel(tableoverviewModel);
        
        String currentAssetAPI = "nxt?requestType=getAccount&account=";
        try {
            currentAssetAPI += URLEncoder.encode(accountId, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        String str = null;
        try {
            str = urlExec(globalURL,currentAssetAPI);
        } catch (IOException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        JSONParser parser = new JSONParser();
        Object obj = null;
        try {
            obj = parser.parse(str);
        } catch (ParseException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        JSONObject jsonObject = (JSONObject) obj;
        quantityAssetList.clear();
        if (jsonObject != null){
            JSONArray newjsonObject = (JSONArray) jsonObject.get("assetBalances");
            if (newjsonObject != null){
                Iterator<String> iterator = newjsonObject.iterator();
                while (iterator.hasNext()) {
                    Object inarr = iterator.next();
                    JSONObject injsonObject = (JSONObject) inarr;
                    Long balance = (Long) injsonObject.get("balance");
                    String asset = (String) injsonObject.get("asset");
                    quantityAssetList.add(balance.toString());
                    nameAssetComboBox.addItem(asset);
                    tableoverviewModel.addRow(new Object[]{accountId, asset, balance});
                }
            }else
                nameAssetComboBox.addItem(java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("NO ASSETS AVAILABLE!"));
        }
        
    }
    List<String> quantityAssetList = new ArrayList<String>();
    private void placeOrder(String assetId){
        String placeOrderAPI = "nxt?requestType=";
        String order;
        if (orderTypeComboBox.getSelectedIndex() == 0)
            order = "placeAskOrder";
        else
            order = "placeBidOrder";
        try {
            placeOrderAPI += URLEncoder.encode(order, "UTF-8");
            placeOrderAPI += "&asset=";         
            placeOrderAPI += URLEncoder.encode(assetId, "UTF-8");
            placeOrderAPI += "&quantity=";
            placeOrderAPI += URLEncoder.encode(assetQuantityField.getText(), "UTF-8");
            placeOrderAPI += "&price=";
            placeOrderAPI += URLEncoder.encode(assetPriceField.getText(), "UTF-8");
            placeOrderAPI += "&secretPhrase=";
            placeOrderAPI += URLEncoder.encode(assetPassField.getText(), "UTF-8");
            placeOrderAPI += "&fee=";
            placeOrderAPI += URLEncoder.encode(defaultFee.getText(), "UTF-8");
            placeOrderAPI += "&deadline=";
            placeOrderAPI += URLEncoder.encode(defaultDeadline.getText(), "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        String str = null;
        try {
            str = urlExec(globalURL,placeOrderAPI);
        } catch (IOException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        consoleOutput.append("\n["+nowtime()+"]"+java.util.ResourceBundle.getBundle("clienxt/Bundle").getString(" ASSET ORDER TX :")+str);
        statusbarLabel.setText(str);
        
        assetQuantityField.setText("");
        assetPriceField.setText("");
        assetPassField.setText("");
        placeOrderDialog.dispose();
    }
    
    private void getAssetAskOrders(String assetsId){
        Object[][] rowData = {};
        Object[] columnNames = {java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("ACCOUNT"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("ASSET ID"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("QUANTITY"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("PRICE")};
        
        DefaultTableModel tableAskAssetOrders = new DefaultTableModel(rowData, columnNames);
        
        askOrdersAssetTable.setModel(tableAskAssetOrders);
        
        String askOrdersAPI = "nxt?requestType=getAskOrderIds&timestamp=0&asset=";
        try {
            askOrdersAPI += URLEncoder.encode(assetsId, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        String str = null;
        try {
            str = urlExec(globalURL,askOrdersAPI);
        } catch (IOException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        JSONParser parser = new JSONParser();
        Object obj = null;
        try {
            obj = parser.parse(str);
        } catch (ParseException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        JSONObject jsonObject = (JSONObject) obj;
        jProgressBar1.setValue(0);
        jProgressBar1.setStringPainted(true);
        int totalDataRead=0;
        int length;
        if (jsonObject != null){
            JSONArray newjsonObject = (JSONArray) jsonObject.get("askOrderIds");
            if (newjsonObject != null){
                Iterator<String> iterator = newjsonObject.iterator();
                length = newjsonObject.size();
                while (iterator.hasNext()) {
                    String data[];
                    data = getAskOrder(iterator.next());
                    
                    totalDataRead=totalDataRead+1;
                    float Percent=(totalDataRead*100)/length;
                    jProgressBar1.setValue((int)Percent);
                    
                    tableAskAssetOrders.addRow(new Object[]{data[0], data[1], data[2], data[3]});
                }
                //askOrdersAssetTable.repaint();
            }
        }
    }
    
    private String[] getAskOrder(String askId){
        
        String askOrderAPI = "nxt?requestType=getAskOrder&order=";
        
        try {
            askOrderAPI += URLEncoder.encode(askId, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        String str = null;
        try {
            str = urlExec(globalURL,askOrderAPI);
        } catch (IOException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        JSONParser parser = new JSONParser();
        Object obj = null;
        try {
            obj = parser.parse(str);
        } catch (ParseException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        JSONObject jsonObject = (JSONObject) obj;
        
        String account = null ;
        String asset = null;  
        Long quantity = null;
        Long price = null;
        float prc = 0;
        if (jsonObject != null){
          account = (String)jsonObject.get("account");
          asset = (String)jsonObject.get("asset");  
          quantity = (Long)jsonObject.get("quantity");
          price = (Long)jsonObject.get("price");
          prc = price;
          prc = prc/100;
        }
        return new String[] {account, asset, quantity.toString(), String.valueOf(prc)};
    }
    private void getAssetBidOrders(String assetsId){
        Object[][] rowData = {};
        Object[] columnNames = {java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("ACCOUNT"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("ASSET ID"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("QUANTITY"), java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("PRICE")};
        
        DefaultTableModel tableBidAssetOrders = new DefaultTableModel(rowData, columnNames);
   
        bidOrdersAssetTable.setModel(tableBidAssetOrders);
        
        String bidOrdersAPI = "nxt?requestType=getBidOrderIds&timestamp=0&asset=";
        try {
            bidOrdersAPI += URLEncoder.encode(assetsId, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        String str = null;
        try {
            str = urlExec(globalURL,bidOrdersAPI);
        } catch (IOException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        JSONParser parser = new JSONParser();
        Object obj = null;
        try {
            obj = parser.parse(str);
        } catch (ParseException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        JSONObject jsonObject = (JSONObject) obj;
        jProgressBar1.setValue(0);
        jProgressBar1.setStringPainted(true);
        int totalDataRead=0;
        int length;
        if (jsonObject != null){
            JSONArray newjsonObject = (JSONArray) jsonObject.get("bidOrderIds");
            if (newjsonObject != null){
                Iterator<String> iterator = newjsonObject.iterator();
                length = newjsonObject.size();
                while (iterator.hasNext()) {
                    String data[];
                    data = getBidOrder(iterator.next());
                    
                    totalDataRead=totalDataRead+1;
                    float Percent=(totalDataRead*100)/length;
                    jProgressBar1.setValue((int)Percent);
        
                    tableBidAssetOrders.addRow(new Object[]{data[0], data[1], data[2], data[3]});
                }
                //bidOrdersAssetTable.repaint();
            }
        }
    }
    
    private String[] getBidOrder(String askId){
        /* 
         * input askOrderId
         * return account, asset id, quantity, price
         */
        String bidOrderAPI = "nxt?requestType=getBidOrder&order=";
        
        try {
            bidOrderAPI += URLEncoder.encode(askId, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        String str = null;
        try {
            str = urlExec(globalURL,bidOrderAPI);
        } catch (IOException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        JSONParser parser = new JSONParser();
        Object obj = null;
        try {
            obj = parser.parse(str);
        } catch (ParseException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        JSONObject jsonObject = (JSONObject) obj;
        
        String account = null ;
        String asset = null;  
        Long quantity = null;
        Long price = null;
        float prc = 0;
        if (jsonObject != null){
          account = (String)jsonObject.get("account");
          asset = (String)jsonObject.get("asset");  
          quantity = (Long)jsonObject.get("quantity");
          price = (Long)jsonObject.get("price"); 
          prc = price;
          prc = prc/100;
        }
        return new String[] {account, asset, quantity.toString(), String.valueOf(prc)};
    }
    private class HighlightRenderer extends DefaultTableCellRenderer {
        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            // everything as usual
            super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            // added behavior
            if(row == table.getSelectedRow()) {
                // this will customize that kind of border that will be use to highlight a row
                setBorder(BorderFactory.createMatteBorder(2, 1, 2, 1, Color.BLACK));
            }   
            /*//test this behavior 
            // added behavior
            if (value!=null && value.toString().matches(".*"+Pattern.quote(text)+".*")) {
                // this will customize that kind of border that will be use to highlight a row
                setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK));
            } else
                setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1);
            */
            return this;
        }
    }
    
    private static String OSType = null;
    
    public static String getOsName()
    {
        if (OSType == null) {OSType = System.getProperty("os.name");}
        return OSType;
    }
    public static boolean isWindows()
    {
        return getOsName().startsWith("Windows");
    }
    public static boolean isLinux()
    {
        return getOsName().startsWith("Linux");
    }
    public static boolean isMac()
    {
        return getOsName().startsWith("Mac");
    }
        /**
     * Unzip it
     * @param zipFile input zip file
     * @param output zip file output folder
     */
    public void unZipIt(String zipFiler, String outputFolder){
      try {
         ZipFile zipFile = new ZipFile(zipFiler);
         zipFile.extractAll(outputFolder);
    } catch (ZipException e) {
        e.printStackTrace();
    }
   }
    private void download(String fileURL, String destinationDirectory) throws IOException {
        // File name that is being downloaded
        String downloadedFileName = fileURL.substring(fileURL.lastIndexOf("/")+1);
         
        // Open connection to the file
        URL url = new URL(fileURL);
        InputStream is = url.openStream();
        // Stream to the destionation file
        FileOutputStream fos = new FileOutputStream(destinationDirectory);
  
        URLConnection connection = url.openConnection();
        connection.connect();

        int fileLenth = connection.getContentLength();
        
        // Read bytes from URL to the local file
        byte[] buffer = new byte[4096];
        int bytesRead = 0;
        jProgressBar1.setValue(0);
        jProgressBar1.setStringPainted(true);
                   
        int totalDataRead = 0;
        consoleOutput.append("\n["+nowtime()+"] Downloading: "+downloadedFileName);
        statusbarLabel.setText(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("DOWNLOADING"));

        //consoleOutput.append("["+nowtime()+"]");  // Progress bar :)
        while ((bytesRead = is.read(buffer)) != -1) {
            totalDataRead=totalDataRead+bytesRead;
            float Percent=(totalDataRead*100)/fileLenth;
            jProgressBar1.setValue((int)Percent);
            fos.write(buffer,0,bytesRead);
        }
        LogGui(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("DOWNLOAD COMPLETED!"));
        statusbarLabel.setText(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("DOWNLOAD COMPLETED"));
  
        // Close destination stream
        fos.close();
        // Close URL stream
        is.close();
    }  

    String OUTPUTUSRFOLDER = System.getProperty("user.dir");
    Properties propMain;
    Properties propTest;
    Properties props = new Properties();
    String isTestnet = prefs.get(PREF_network, null);
    InputStream input = null;
    private void startServer(){ 
        isTestnet = prefs.get(PREF_network, null);
        
/*      //*** update nxt.jar ***
        File f = new File(LIBFolder+"nxt-updated.jar");
        if(f.exists()){
            deletefiles(LIBFolder+"nxt.jar");
            renamefiles(LIBFolder+"nxt-updated.jar", LIBFolder+"nxt.jar");
            //copyFile(LIBFolder+"nxt-updated.jar", LIBFolder+"nxt.jar");
            LogGui("Nxt update file is found!");
        }else{
            LogGui("No Nxt update is found!");
        }
*/
/*      //*** Add file in classpath ***
        ClassLoader currentThreadClassLoader = Thread.currentThread().getContextClassLoader();
        // Add the conf dir to the classpath
        // Chain the current thread classloader
        URLClassLoader urlClassLoader = null;
        try {
            urlClassLoader = new URLClassLoader(new URL[]{new File(LIBFolder+"nxt.jar").toURL()}, currentThreadClassLoader);
            LogGui("MOST LIKELY ADDED the file!");
        } catch (MalformedURLException ex) {
            LogGui("Classpath cannot be updated!");
        }
        // Replace the thread classloader - assumes
        // you have permissions to do so
        Thread.currentThread().setContextClassLoader(urlClassLoader);
*/       
        // Get current classloader
        ClassLoader cl = this.getClass().getClassLoader();
        propMain = new Properties();
        propTest = new Properties();
        try {
            propMain.load(cl.getResourceAsStream("nxt-default.properties"));
            //ur = cl.getResource("nxt-default.properties");
            //System.out.println(ur.getPath());
            //System.setProperty("nxt-default.properties","nxt-default.properties");
            propTest.load(cl.getResourceAsStream("nxt-testnet.properties"));
        } catch (IOException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (isTestnet != null){
            if (isTestnet.equals("1")){
                nxt.Nxt.init(propTest);
            }
            else{
                //System.setProperties(propMain);
                nxt.Nxt.init(propMain);
            }
        }else
            JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("CANNOT START NXT SERVER, CONFUSED TO CHOOSE NETWORK (MAIN OR TEST)?!"));
            
        flag = 1;
        
        nxt.Generator.addListener(new nxt.util.Listener<nxt.Generator>() {
                @Override
                public void notify(nxt.Generator t) {
                    for (int i=0; i<Generators.size(); i++){
                            if (Generators.get(i) == t){
                                tableModelA.updateAccount(null,null,null,(int) t.getDeadline(), i);
                            }
                    }
                    latestBlockLabel.setText(nxt.util.Convert.fromEpochTime(nxt.Nxt.getBlockchain().getLastBlock().getTimestamp()).toString());
                    latestBlockLabel.setToolTipText(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("LATEST BLOCK TIME: ")+nxt.util.Convert.fromEpochTime(nxt.Nxt.getBlockchain().getLastBlock().getTimestamp()).toString()+"\n"+java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("LATEST BLOCK ID: ")+nxt.Nxt.getBlockchain().getLastBlock().getId());
                    peersLabel.setText(nxt.peer.Peers.getAllPeers().size()+java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString(" PEERS"));
                }
            }, nxt.Generator.Event.GENERATION_DEADLINE);
/*      //*** number of peers ***
        nxt.peer.Peers.addListener(new Listener<Peer>() {

            @Override
            public void notify(Peer t) {
                peersLabel.setText(t.);
            }
        }, Peers.Event.WEIGHT)
*/
        partialComponents(true);
    }
    
    private void partialComponents(boolean check){
        overviewButton.setEnabled(check);
        sendButton.setEnabled(check);
        transactionsButton.setEnabled(check);
        assetexchangeButton.setEnabled(check);
        votingButton.setEnabled(check);
        unlockButton.setEnabled(check);
        messagesButton.setEnabled(check);
        aliasButton.setEnabled(check);
        MenuBar.setEnabled(check);
    }
    
    volatile int flag;
    Process p;
    private void stopServer(){ 
        nxt.Nxt.shutdown();
        //Db.shutdown();
        flag = 0;
    }
    
    private String LIBFolder;
    private String NXTFolder;
    private final List<String> accountNumberList = new ArrayList<String>();
    private Long accountBalances;
    private Long unconfirmedBalances;
    private Long effectiveBalances;
    private final int counter = 0;
    
    private Long getBalance(String accountnumbero) throws IOException{
        try {
            String balanceAPI = "nxt?requestType=getBalance&account=";
            balanceAPI += URLEncoder.encode(accountnumbero, "UTF-8");
            
            String bal = urlExec(globalURL,balanceAPI);
            
            JSONParser parser = new JSONParser();
            Object obj = null;
            try {
                obj = parser.parse(bal);
            } catch (ParseException ex) {
                Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
            }
            JSONObject jsonObject = (JSONObject) obj;
            if (jsonObject != null){
                accountBalances = (Long) jsonObject.get("balance");
                effectiveBalances = (Long) jsonObject.get("effectiveBalance");
                unconfirmedBalances = (Long) jsonObject.get("unconfirmedBalance");

                //take last two decimal digits
                if (accountBalances > 100){
                    accountBalances = accountBalances/100;
                }
                if (effectiveBalances > 100){
                    effectiveBalances = effectiveBalances/100;
                }
                if (unconfirmedBalances > 100){
                    unconfirmedBalances = unconfirmedBalances/100;
                }
            }
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        return accountBalances;
    }
    public String convertStringToHex(String str){
 
	  char[] chars = str.toCharArray();
 
	  StringBuffer hex = new StringBuffer();
	  for(int i = 0; i < chars.length; i++){
	    hex.append(Integer.toHexString((int)chars[i]));
	  }
 
	  return hex.toString();
  }
 
  public String convertHexToString(String hex){
 
	  StringBuilder sb = new StringBuilder();
	  StringBuilder temp = new StringBuilder();
 
	  //49204c6f7665204a617661 split into two characters 49, 20, 4c...
	  for( int i=0; i<hex.length()-1; i+=2 ){
 
	      //grab the hex in pairs
	      String output = hex.substring(i, (i + 2));
	      //convert hex to decimal
	      int decimal = Integer.parseInt(output, 16);
	      //convert the decimal to character
	      sb.append((char)decimal);
 
	      temp.append(decimal);
	  } 
	  return sb.toString();
  }
    private String getPubKey(String accountN) throws UnsupportedEncodingException {
        
            String pubkeyAPI = "nxt?requestType=getAccountPublicKey&account=";
            pubkeyAPI += URLEncoder.encode(accountN, "UTF-8");
            
            String pkey = null;
        try {
            pkey = urlExec(globalURL,pubkeyAPI);
        } catch (IOException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
            JSONParser parser = new JSONParser();
            Object obj = null;
            if (pkey != null){
                try {
                    obj = parser.parse(pkey);
                } catch (ParseException ex) {
                    Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
                }
                JSONObject jsonObject = (JSONObject) obj;
                return (String) jsonObject.get("publicKey");
            }else
                return (String) "";
    }
    
    ArrayList<String> accountsDatabase = new ArrayList<String>();
   
    private void generateNewAccount(String masterPassword){
        removeCryptographyRestrictions();
        Long balls = null;

        //AccountTableModel tableModelz = (AccountTableModel) AccountsTable.getModel();//new AccountTableModel();
        //AccountsTable.setModel(tableModelz);
        //JCTable accountsA = new JCTable(LIBFolder+"accounts");
       
        
        PasswordUtil newp = new PasswordUtil();
        String secretPhrase = newp.generatePassword();
        //API call and extract data
        String unlockAPI = "nxt?requestType=getAccountId&secretPhrase="; 
        try {
            unlockAPI += URLEncoder.encode(secretPhrase, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        String str = null;
        try {
            str = urlExec(globalURL,unlockAPI);
        } catch (IOException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }

        String accountID;
        JSONParser parser = new JSONParser();
        Object obj = null;
        try {
            obj = parser.parse(str);
        } catch (ParseException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        JSONObject jsonObject = (JSONObject) obj;
        accountID = (String) jsonObject.get("accountId");

        //if result exist message account is already listed, otherwise insert new data
        //JCRow[] result = accountsA.select("Account_Number", "=", accountID);
        //if (result.length == 1){
          //  JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("ACCOUNT IS ALREADY LISTED!"));        
        //}
        //else {
            try {
                balls = getBalance(accountID);
            } catch (IOException ex) {
                Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
            }
            //JCRow newRow = new JCRow(accountsA);
            //newRow.setString("Label", "(no label)");
            //newRow.setString("Account_Number", accountID);
            //newRow.setString("Balance", balls.toString());
            //masterKey = "12345678910111213";
            String kkey = null;
            try {
                kkey = masterPassword;//getSHA256(masterKey);//SCryptUtil.scrypt(masterKey, 8, 8, 8);//PBKDF2.getSaltedHash(masterKey); //
                //kkey = Arrays.copyOf(kkey.getBytes(), 256).toString();
            } catch (Exception ex) {
                Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
            }
            //nxt.util.Logger.logMessage(getSHA256(masterKey));
            //nxt.util.Logger.logMessage(kkey);
            prefs.put(PREF_masterKey, getSHA256(kkey));
            String enc = null;
            try {
                enc = aes.encrypt(secretPhrase, masterPassword);//encryptAES(masterPassword.getBytes(), secretPhrase);//
                //enc = DeEncrypter.getInstance(masterPassword.getBytes()).encrypt(secretPhrase);
            } catch (Exception ex) {
                Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
            }
            //newRow.setString("Flag", "1");

            //accountsA.insert(newRow);
            try {
                insertRecordIntoDbAccountsTable(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("(NO LABEL)"), accountID, balls.toString(), enc, "1");
            } catch (SQLException ex) {
                Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
            }

            final nxt.Generator check;
            check = nxt.Generator.startForging(secretPhrase);
            Generators.add(check);
            int ded;
            if (check != null){
                ded = (int)check.getDeadline();
            }else
                ded = 0;
            tableModelA.addAccount(new AccountGUI(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("(NO LABEL)"), accountID, balls.toString(), ded));
        //}
    }
    private void deleteDbRecords(){
        try (Connection dbConnection = Db.getDBConnection()){
            Statement st = dbConnection.createStatement();
            String sql = "drop table accounts";
            st.executeUpdate(sql);

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } 
        //after dropping table, init database to create empty table.
        Db.init();
    }
    private void insertRecordIntoDbAccountsTable(String label, String accountId, String balance, String phrase, String flag) throws SQLException {
        try (Connection dbConnection = Db.getDBConnection()){
            Statement st = dbConnection.createStatement();
            String sql = "insert into accounts (label, accountId, balance, phrase, flag) values ('"+label+"','"+accountId+"','"+balance+"','"+phrase+"','"+flag+"')";
            st.executeUpdate(sql);
        } catch (SQLException e) {
            LogGui(e.getMessage());
        } 
    }
    private void updateRecordIntoDbAccountsTable(String accountId, String balance) throws SQLException {
        try (Connection dbConnection = Db.getDBConnection()){
            Statement st = dbConnection.createStatement();
            String sql = "update accounts set balance = '"+balance+"' where accountId ='"+accountId+"'";
            st.executeUpdate(sql);
        } catch (SQLException e) {
            LogGui(e.getMessage());
        } 
    }
    private Vector readRecord(String accountId){

        Vector data = new Vector();
        Vector row = new Vector();
        try (Connection dbConnection = Db.getDBConnection()){
            Statement st = dbConnection.createStatement();
            String sql = "select * from accounts where accountId = '"+accountId+"'"; 
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()){
                row.add(rs.getString("label"));
                row.add(rs.getString("accountId"));
                row.add(rs.getString("balance"));
                row.add(rs.getString("phrase"));
                row.add(rs.getString("flag"));
                data.add(row);
            }
        } catch (SQLException e) {
            LogGui(e.getMessage());
        } 
        return data;
    }
    private Vector readAccounts(String flag){
        Vector acc = new Vector();
        if (flag != null){
            try (Connection dbConnection = Db.getDBConnection()){
                Statement st = dbConnection.createStatement();
                String sql = "select accountId from accounts where flag='"+flag+"'"; 
                ResultSet rs = st.executeQuery(sql);
                while (rs.next()){
                    acc.add(rs.getString("accountId"));
                }
            } catch (SQLException e) {
                LogGui(e.getMessage());
            } 
        }else{
            try (Connection dbConnection = Db.getDBConnection()){
                Statement st = dbConnection.createStatement();
                String sql = "select accountId from accounts"; 
                ResultSet rs = st.executeQuery(sql);
                while (rs.next()){
                    acc.add(rs.getString("accountId"));
                }
            } catch (SQLException e) {
                LogGui(e.getMessage());
            }
        }
        return acc;
    }
    private void deleteAccountsFromDb(String accountid){
        Vector acc = new Vector();
        try (Connection dbConnection = Db.getDBConnection()){
            Statement st = dbConnection.createStatement();
            String sql = "delete from accounts where accountId='"+accountid+"'"; 
            st.executeUpdate(sql);
        } catch (SQLException e) {
            LogGui(e.getMessage());
        } 
    }
    
    private void readTable() {
        
        accountsDatabase.clear();
        AccountsTable.removeAll(); 
        tableModelA = (AccountTableModel) AccountsTable.getModel();//new AccountTableModel();
        AccountsTable.setModel(tableModelA);
        AES aes = new AES();
        String mpassword = "";
        try (Connection cc= Db.getConnection(); Statement st = cc.createStatement()){
             // st.executeQuery("SELECT * FROM accounts");
            ResultSet rs = st.executeQuery("select * from accounts");
            while(rs.next())
            {
                String label = rs.getString("label");
                String accountid = rs.getString("accountId");
                String balance = rs.getString("balance");
                String phrase = rs.getString("phrase");
                String flagg = rs.getString("flag");
                String mkey = prefs.get(PREF_masterKey, null);
                String sphrase = "";
                nxt.Generator ww;
                if (mkey != null){
                    if (mpass.equals("")){
                        masterFlag = 2;
                        masterPasswordDialog.setSize(310, 75);
                        masterPasswordDialog.setLocationRelativeTo(null);
                        masterPasswordDialog.setTitle(java.util.ResourceBundle.getBundle("src/clienxt/Bundle").getString("CONFIRM MASTER PASSWORD FOR FORGING!"));
                        masterPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
                        masterPasswordDialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
                        masterPasswordDialog.setVisible(true);
                    }
                    mpassword = getMpass();
                    //xfxfxf
                    sphrase = aes.decrypt(phrase, mpass);
                    ww = nxt.Generator.startForging(sphrase);//getGenerator(lang);//("", "".getBytes(), wa);
                    Generators.add(ww);
                }
                tableModelA.addAccount(new AccountGUI(label, accountid, balance, Integer.parseInt(flagg)));
                accountsDatabase.add(accountid.toString());
            }
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                    st.close();
            }
            if (cc != null) {
                    cc.close();
            }
        } catch (SQLException ex) {
            LogGui("WARNING! SQLException "+ex.getMessage());
        } catch (NoSuchAlgorithmException ex) {
            LogGui("WARNING! NoSuchAlgorithmException "+ex.getMessage());
        } catch (NoSuchProviderException ex) {
            LogGui("WARNING! NoSuchProviderException "+ex.getMessage());
        } catch (NoSuchPaddingException ex) {
            LogGui("WARNING! NoSuchPaddingException "+ex.getMessage());
        } catch (InvalidKeyException ex) {
            LogGui("WARNING! InvalidKeyException "+ex.getMessage());
        } catch (IllegalBlockSizeException ex) {
            LogGui("WARNING! IllegalBlockSizeException "+ex.getMessage());
        } catch (BadPaddingException ex) {
            LogGui("WARNING! BadPaddingException "+ex.getMessage());
        } catch (IOException ex) {
            LogGui("WARNING! IOException "+ex.getMessage());
        } 
    }
    private volatile String mpass = "";
    private String getMpass(){
        return mpass;
    } 
    private String getMD5(String password){
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(ClienxtForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        md.update(password.getBytes());
 
        byte byteData[] = md.digest();
        //convert the byte to hex format method 1
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
         sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

    private String urlExec(String str, String param) throws IOException{
        URL url = new URL(str+param);
        try{
            URLConnection urlc = url.openConnection();
            InputStream is = urlc.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader in = new BufferedReader(isr);
            str="";
            /*Scanner scan = new Scanner(url.openStream()); //urlc.getInputStream()

            while (scan.hasNext())
                str += scan.nextLine();
            scan.close();*/
            String inputLine;
            while ((inputLine = in.readLine()) != null) 
                str += inputLine;
            in.close();
            
        } catch (java.net.ConnectException ex) {
             LogGui("WARNING! ConnectException "+ex.getMessage());
        }
        return str;
    }
    private static String nowtime(){
        Date date = new Date();
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(date);
    }
    public static void LogGui(String message){
        System.out.println("["+nowtime()+"] "+message);
    }
    private void renamefiles(String oldFileName, String newFileName){
        File oldfile =new File(oldFileName);
        File newfile =new File(newFileName);
        if(oldfile.renameTo(newfile)){
            System.out.println("["+nowtime()+java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("] RENAME SUCCESFUL"));
        }else{
            System.out.println("["+nowtime()+java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("] RENAME FAILED"));
        }
    }
    private void deletefiles(String filename){
        try{
            File file = new File(filename);
            if(file.delete()){
                System.out.println("\n["+nowtime()+"] "+file.getName() + java.util.ResourceBundle.getBundle("clienxt/Bundle").getString(" IS DELETED!"));
            }else{
                System.out.println("\n["+nowtime()+java.util.ResourceBundle.getBundle("clienxt/Bundle").getString("] DELETE OPERATION IS FAILED."));
            }
     	}catch(Exception e){
            e.printStackTrace();
     	}
    }

    //read commandline output    
    private class LogsStreamReader implements Runnable{

       private final BufferedReader reader;

    public LogsStreamReader(InputStream is) {
        this.reader = new BufferedReader(new InputStreamReader(is));
    }

       @Override
    public void run() {
        try {
            String line = reader.readLine();
            while (line != null) {
                consoleOutput.append(line+"\n");
                consoleOutput.setCaretPosition(consoleOutput.getDocument().getLength());
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    }
    class CustomRenderer implements TableCellRenderer  
    {  
        JLabel label;  
        int targetRow, targetCol;  

        public CustomRenderer()  
        {  
            label = new JLabel();  
            label.setHorizontalAlignment(JLabel.CENTER);  
            label.setOpaque(true);  
            targetRow = -1;  
            targetCol = -1;  
        }  

        public Component getTableCellRendererComponent(JTable table,  
                                                       Object value,  
                                                       boolean isSelected,  
                                                       boolean hasFocus,  
                                                       int row, int column)  
        {  
            if(isSelected)  
            {  
                label.setBackground(table.getSelectionBackground());  
                label.setForeground(table.getSelectionForeground());  
            }  
            else  
            {  
                label.setBackground(table.getBackground());  
                label.setForeground(table.getForeground());  
            }  
            if(row == targetRow && column == targetCol)  
            {  
                label.setBorder(BorderFactory.createLineBorder(Color.red));  
                label.setFont(table.getFont().deriveFont(Font.BOLD));  
            }  
            else  
            {  
                label.setBorder(null);  
                label.setFont(table.getFont());  
            }  
            label.setText((String)value);  
            return label;  
        }  

        public void setTargetCell(int row, int col)  
        {  
            targetRow = row;  
            targetCol = col;  
        }  
    } 
    class PopupListener extends MouseAdapter {
        JPopupMenu popup;
 
        PopupListener(JPopupMenu popupMenu) {
            popup = popupMenu;
        }
 
        @Override
        public void mousePressed(MouseEvent e) {
            maybeShowPopup(e);
        }
 
        @Override
        public void mouseReleased(MouseEvent e) {
            maybeShowPopup(e);
        }
 
        private void maybeShowPopup(MouseEvent e) {
            if (e.isPopupTrigger()) {
                popup.show(e.getComponent(),
                           e.getX(), e.getY());
            }
        }
    }
    /*
    private void actionUpdateAccount(String nlabel, String naccount, String nbalance, int ndeadline) {
        tableModelA.updateAccount(nlabel,naccount,nbalance,ndeadline,AccountsTable.getSelectedRow());
        //table.repaint();
        //updateButtons();
    }

    // Clear the selected account.
    private void actionClearAccount() {
        clearing = true;
        tableModelA.clearAccount(AccountsTable.getSelectedRow());
        clearing = false;
        selectedAccount = null;
        //updateButtons();
    }*/
/* Update each button's state based off of the
currently selected download's status. */
private void updateButtons() {
    if (selectedAccount != null) {
        int status = selectedAccount.getStatus();
        switch (status) {
            case AccountGUI.FORGING:
            //pauseButton.setEnabled(true);
            //resumeButton.setEnabled(false);
                forgeButton.setEnabled(false);
            //cancelButton.setEnabled(true);
                RemoveAccountButton.setEnabled(false);
            break;
            case AccountGUI.WATCHING:
            //pauseButton.setEnabled(false);
            //resumeButton.setEnabled(true);
                forgeButton.setEnabled(true);
            //cancelButton.setEnabled(true);
                RemoveAccountButton.setEnabled(true);
            break;
            case AccountGUI.ERROR:
            //pauseButton.setEnabled(false);
            //resumeButton.setEnabled(true);
            //cancelButton.setEnabled(false);
                RemoveAccountButton.setEnabled(true);
                forgeButton.setEnabled(true);
            break;
            default: // COMPLETE or CANCELLED
            //pauseButton.setEnabled(false);
            //resumeButton.setEnabled(false);
            //cancelButton.setEnabled(false);
                forgeButton.setEnabled(true);
                RemoveAccountButton.setEnabled(true);
        }
    } else {
        // No download is selected in table.
        forgeButton.setEnabled(false);
        //pauseButton.setEnabled(false);
        //resumeButton.setEnabled(false);
        //cancelButton.setEnabled(false);
        RemoveAccountButton.setEnabled(false);
    }
}
    public void update(Observable o, Object arg) {
        // Update buttons if the selected download has changed.
        if (selectedAccount != null && selectedAccount.equals(o))
            updateButtons();
    }
    static BasicService basicService = null;
    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ClienxtForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ClienxtForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ClienxtForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ClienxtForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        /*SwingUtilities.invokeLater(new Runnable() {
        public void run() {
            ClienxtForm manager = new ClienxtForm();
            manager.setVisible(true);
        }
        });*/
        
        ClienxtForm frame = new ClienxtForm();
        /*
        try {
            basicService = (BasicService)
              ServiceManager.lookup("javax.jnlp.BasicService");
        } catch (UnavailableServiceException e) {
            System.err.println("Lookup failed: " + e);
        }
       */ 
        frame.show();
        //frame.pack();
        
    }
    private final DefaultListModel listModel = new DefaultListModel();
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox ALaccountListComboBox;
    private javax.swing.JTable AccountsTable;
    private javax.swing.JScrollPane AccountsTableScrollPane;
    private javax.swing.JButton AddAccountButton;
    private javax.swing.JPanel AliasesPanel;
    private javax.swing.JPanel AssetExPanel;
    private javax.swing.JPanel BlocksPanel;
    private javax.swing.JTable BlocksTable;
    private javax.swing.JPanel ConsolePanel;
    private javax.swing.JMenu FileMenu;
    private javax.swing.JPanel MainPanel;
    private javax.swing.JToolBar MainToolbar;
    private javax.swing.JMenuBar MenuBar;
    private javax.swing.JPanel MessagesPanel;
    private javax.swing.JLabel NxtBtcLabel;
    private javax.swing.JLabel NxtUsdLabel;
    private javax.swing.JButton OKbutton;
    private javax.swing.JPanel OverviewPanel;
    private javax.swing.JPanel ReceivePanel;
    private javax.swing.JButton RemoveAccountButton;
    private javax.swing.JPanel SendPanel;
    private javax.swing.JDialog SentDialog;
    private javax.swing.JComboBox TXaccountListCombobox;
    private javax.swing.JPopupMenu TablePopupMenu;
    private javax.swing.ButtonGroup ToolbarButtonGroup;
    private javax.swing.JPanel TransactionsPanel;
    private javax.swing.JScrollPane TransactionsScrollPane;
    private javax.swing.JDialog UnlockDialog;
    private javax.swing.JPanel VotingPanel;
    private javax.swing.JDialog aboutClieNXTdialog;
    private javax.swing.JMenuItem aboutClienxtMenuItem;
    private javax.swing.JMenu aboutMenu;
    private javax.swing.JMenuItem aboutNxtMenuItem;
    private javax.swing.JTable accountCheckTable;
    private javax.swing.JPasswordField accountPassField;
    private javax.swing.JSeparator accountsSeparator;
    private javax.swing.JPasswordField aliasAccountPassphrase;
    private javax.swing.JToggleButton aliasButton;
    private javax.swing.JTextField aliasField;
    private javax.swing.JMenuItem aliasMenu;
    private javax.swing.JTextField aliasSearchField;
    private javax.swing.JTable aliasTable;
    private javax.swing.JScrollPane allAssetsScrollPane;
    private javax.swing.JTable allAssetsTable;
    private javax.swing.JTable allPollsTable;
    private javax.swing.JLabel amountLabel;
    private javax.swing.JTextField amountNxtField;
    private javax.swing.JTable askOrdersAssetTable;
    private javax.swing.JComboBox assetAccountListComboBox;
    private javax.swing.JComboBox assetAccountsComboBox;
    private javax.swing.JTextArea assetDescriptionField;
    private javax.swing.JMenuItem assetExMenu;
    private javax.swing.JLabel assetIdOrderLabel;
    private javax.swing.JTextField assetNameField;
    private javax.swing.JButton assetOrderSubmitButton;
    private javax.swing.JDialog assetOrdersDialog;
    private javax.swing.JPasswordField assetPassField;
    private javax.swing.JPasswordField assetPassphraseField;
    private javax.swing.JTextField assetPriceField;
    private javax.swing.JTextField assetQtyField;
    private javax.swing.JTextField assetQuantityField;
    private javax.swing.JDialog assetTradesDialog;
    private javax.swing.JTable assetTradesTable;
    private javax.swing.JToggleButton assetexchangeButton;
    private javax.swing.JButton assignAliasButton;
    private javax.swing.JCheckBox attachMessageCheckBox;
    private javax.swing.JTable bidOrdersAssetTable;
    private javax.swing.JButton cancelButton;
    private javax.swing.JMenuItem cancelOrderCMenu;
    private javax.swing.JPasswordField cancelPassField;
    private javax.swing.JPopupMenu castVoteContextMenu;
    private javax.swing.JDialog castVoteDialog;
    private javax.swing.JLabel castVoteIdLabel;
    private javax.swing.JMenuItem castVoteMenuItem;
    private javax.swing.JComboBox castVoteOptionComboBox;
    private javax.swing.JPasswordField castVotePassField;
    private javax.swing.JButton castVoteSubmitButton;
    private javax.swing.JPasswordField changeMasterPasswordField;
    private javax.swing.JMenuItem checkNxtUpdateMenu;
    private javax.swing.JButton confirmCancelAssetButton;
    private javax.swing.JPasswordField confirmMasterPasswordField;
    private javax.swing.ButtonGroup connectionOptionsCheckBox;
    private javax.swing.JToggleButton consoleButton;
    private javax.swing.JMenuItem consoleMenu;
    private javax.swing.JTextArea consoleOutput;
    private javax.swing.JScrollPane consoleScrollPane;
    private javax.swing.JPopupMenu contextMenu;
    private javax.swing.JPopupMenu contextMenuAssetOrders;
    private javax.swing.JPopupMenu contextMenuAssetTable;
    private javax.swing.JMenuItem copyRecipientAddressCMenu;
    private javax.swing.JMenuItem copySenderAddressCMenu;
    private javax.swing.JButton creatPollButton;
    private javax.swing.JDialog createPollDialog;
    private javax.swing.JButton createPollsButton;
    private javax.swing.JComboBox defaultAccount;
    private javax.swing.JTextField defaultDeadline;
    private javax.swing.JTextField defaultFee;
    private javax.swing.JCheckBox encryptMyAccountCheckBox;
    private javax.swing.JCheckBoxMenuItem englishMenuItem;
    private javax.swing.JMenuItem exitMenuItem;
    private javax.swing.Box.Filler filler1;
    private javax.swing.Box.Filler filler2;
    private javax.swing.JButton forgeButton;
    private javax.swing.JDialog forgeDialog;
    private javax.swing.JPasswordField forgePassField;
    private javax.swing.JButton generateTokenButton;
    private javax.swing.JDialog generateTokenDialog;
    private javax.swing.JCheckBoxMenuItem germanMenuItem;
    private javax.swing.JButton getAssetsButton;
    private javax.swing.JButton getPollsButton;
    private javax.swing.JLabel idOrderLabel;
    private javax.swing.JTextArea inputMessageField;
    private javax.swing.JButton issueAssetButton;
    private javax.swing.JButton issueAssetButtonIn;
    private javax.swing.JDialog issueAssetDialog;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel67;
    private javax.swing.JLabel jLabel68;
    private javax.swing.JLabel jLabel69;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel70;
    private javax.swing.JLabel jLabel71;
    private javax.swing.JLabel jLabel72;
    private javax.swing.JLabel jLabel73;
    private javax.swing.JLabel jLabel74;
    private javax.swing.JLabel jLabel75;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane12;
    private javax.swing.JScrollPane jScrollPane13;
    private javax.swing.JScrollPane jScrollPane14;
    private javax.swing.JScrollPane jScrollPane15;
    private javax.swing.JScrollPane jScrollPane16;
    private javax.swing.JScrollPane jScrollPane17;
    private javax.swing.JScrollPane jScrollPane18;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField labelWatchAccountField;
    private javax.swing.JTextField labelofAccount;
    private javax.swing.JMenu languagesMenuItem;
    private javax.swing.JLabel latestBlockLabel;
    private javax.swing.JPasswordField masterPassowordField;
    private javax.swing.JDialog masterPasswordDialog;
    private javax.swing.JPasswordField masterPasswordField;
    private javax.swing.ButtonGroup menuLanguageButtonGroup;
    private javax.swing.JTextArea messageContentField;
    private javax.swing.JTextField messageRecipientField;
    private javax.swing.JPasswordField messageSecretPhraseField;
    private javax.swing.JButton messageSendButton;
    private javax.swing.JComboBox messageSenderComboBox;
    private javax.swing.JToggleButton messagesButton;
    private javax.swing.JTable messagesTable;
    private javax.swing.JDialog myOrderCancelDialog;
    private javax.swing.JDialog myOrdersDialog;
    private javax.swing.JTable myOrdersTable;
    private javax.swing.JComboBox nameAssetComboBox;
    private javax.swing.JCheckBox networkCheckBox;
    private javax.swing.JPasswordField newMasterPasswordField;
    private javax.swing.JComboBox orderTypeComboBox;
    private javax.swing.JButton ordersAssetButton;
    private javax.swing.JMenuItem othersMenuItemItem;
    private javax.swing.JLabel outputL;
    private javax.swing.JTable overviewAssetTable;
    private javax.swing.JToggleButton overviewButton;
    private javax.swing.JMenuItem overviewMenu;
    private javax.swing.JPasswordField passphraseAssetField;
    private javax.swing.JPasswordField passphraseField;
    private javax.swing.JLabel peersLabel;
    private javax.swing.JMenuItem placeAskBidCMenu;
    private javax.swing.JDialog placeOrderDialog;
    private javax.swing.JCheckBoxMenuItem portugueseMenuItem;
    private javax.swing.JFrame preferencesFrame;
    private javax.swing.JMenuItem preferencesMenuItem;
    private javax.swing.JRadioButton proxyAutoRBox;
    private javax.swing.JTextField proxyHostField;
    private javax.swing.JRadioButton proxyManRBox;
    private javax.swing.JRadioButton proxyNoRbox;
    private javax.swing.JPasswordField proxyPasswordField;
    private javax.swing.JTextField proxyPortField;
    private javax.swing.JTextField proxyUserField;
    private javax.swing.JButton qrCodeButton;
    private javax.swing.JTextField quantityAssetField;
    private javax.swing.JMenuItem receiveMenu;
    private javax.swing.JTextField receiverAddressField;
    private javax.swing.JLabel receiverLabel;
    private javax.swing.JTextField recipientAssetField;
    private javax.swing.JMenuItem restartCMenu;
    private javax.swing.JCheckBoxMenuItem russianMenuItem;
    private javax.swing.JTextField searchAssetField;
    private javax.swing.JToggleButton sendButton;
    private javax.swing.JMenuItem sendMenu;
    private javax.swing.JMenuItem sendMsgToRecipientAddressCMenu;
    private javax.swing.JMenuItem sendMsgToSenderAddressCMenu;
    private javax.swing.JButton sendNxtButton;
    private javax.swing.JMenuItem sendNxtToRecipientAddressCMenu;
    private javax.swing.JMenuItem sendNxtToSenderAddressCMenu;
    private javax.swing.JComboBox senderAddressComboBox;
    private javax.swing.JLabel senderLabel;
    private javax.swing.JMenu settingsMenu;
    private javax.swing.JCheckBoxMenuItem spanishMenuItem;
    private javax.swing.JPanel statusBarPanel;
    private javax.swing.JLabel statusbarLabel;
    private javax.swing.JMenuItem stopCMenu;
    private javax.swing.JLabel timeToForgeLabel;
    private javax.swing.JLabel tokenAccountIdLabel;
    private javax.swing.JPasswordField tokenPassField;
    private javax.swing.JTextPane tokenTextPane;
    private javax.swing.JLabel totalBalanceLabelBTC;
    private javax.swing.JLabel totalBalanceLabelNXT;
    private javax.swing.JLabel totalBalanceLabelUSD;
    private javax.swing.JToggleButton transactionsButton;
    private javax.swing.JMenuItem transactionsMenu;
    private javax.swing.JTable transactionsTable;
    private javax.swing.JButton transferAssetButton;
    private javax.swing.JButton transferAssetButtonIn;
    private javax.swing.JDialog transferAssetDialog;
    private javax.swing.JPanel transferAssetPanel;
    private javax.swing.JLabel typeOrderLabel;
    private javax.swing.JToggleButton unlockButton;
    private javax.swing.JButton updateBalancesButton;
    private javax.swing.JButton updatePrefsButton;
    private javax.swing.JMenuItem updateTableCMenu;
    private javax.swing.JTextField uriField;
    private javax.swing.JMenuItem viewAskBidCMenu;
    private javax.swing.JMenuItem viewTradesCMenu;
    private javax.swing.JTextArea voteDescriptionField;
    private javax.swing.JTextField voteMaxField;
    private javax.swing.JTextField voteMinField;
    private javax.swing.JTextField voteNameField;
    private javax.swing.JComboBox voteOptionsAreBinaryComboBox;
    private javax.swing.JTextArea voteOptionsField;
    private javax.swing.JPasswordField voteSecretPassField;
    private javax.swing.JToggleButton votingButton;
    private javax.swing.JMenuItem votingMenu;
    private javax.swing.JDialog watchAccountDialog;
    private javax.swing.JTextField watchAccountNumberField;
    private javax.swing.JTextField websiteField;
    // End of variables declaration//GEN-END:variables
   
}
